# 掌中俱舍客户端

掌中俱舍是一个俱舍论背诵软件，客户端使用 flutter 跨平台框架开发。

## TODO

- [x] 本地缓存
- [x] 使用`GetxService`作为全局状态，优化当前`CurrentUserController`
- [x] 离线内容同步——本地存 isar
  - [x] 缓存书籍信息、背诵进度
  - [x] 不缓存背诵计划
  - [ ] 缓存注释图片
  - [ ] 离线使用同步机制
- [x] 请求无权限重试机制
- [x] 请求失败重试
- [x] 全局提示样式
- [x] 集成 Sentry 崩溃监控
- [ ] 集成 Posthog 埋点监控
- [ ] 网络及系统关键日志记录及上报
- [ ] 请求防抖

## 环境搭建

Flutter 安装参考[官方文档](https://flutter.cn/docs/get-started/install)

国内需配置环境变量

```
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
```

对应安卓和 iOS 分别需要安装 Android Studio 和 Xcode。

运行`flutter doctor`查看环境是否搭建成功，并根据提示完成剩余部分。

### 运行本项目

首先将仓库克隆到本地，切换到 dev 分支，并获取仓库所需第三方库

```sh
git clone git@gitee.com:zhangzhongjushe/client.git -b dev zzjs-client
cd zzjs-client
flutter pub get
```

确保电脑已安装 vscode，并用 vscode 打开本项目

```
code .
```

### 接口文档

https://yapi.mojerro.wang/project/31/interface/api

需要权限找高景行申请

## 开发规范

### 完成需求、代码提交

每个新功能或修复需要新建`feat`/`fix`分支编写，例如：`feat/user-manage`，完成后合并到`dev`分支。`dev`分支构建出测试包提交给测试。测试通过后合并到`master`分支，并在`master`分支打上版本标签，完成发布。

合并分支到`dev`分支时，需要由至少一位其他开发人员进行审查。

分支 PR 以及审查等操作，可以在 Gitee 上通过 UI 操作。

代码编写完成后推荐手动运行`dart analyze`查看是否存在问题。

### 语法规范

1. 需使用显示类型指定如`int a = 1`，不可用`var a = 1`
2. 不使用`late`声明变量
3. 静态调用需加`const`标识，这个编辑器会有提示
4. 其他规范尽可能遵循官方编辑器提示
5. 推荐：新建页面输入`getpagevoid`快速生成代码框架
6. 推荐：新的 Controller 输入`getcontrollervoid`快速生成代码框架

### 页面结构

#### 框架结构

Flutter 页面开发与 Html 类似，可以理解为通过构建各种矩形来画出界面，Flutter 页面构建是通过`Widget`类来实现，每个`Widget`可以理解为一个灵活的图形，设置样式和展示内容。
建议完整阅读[用户界面](https://flutter.cn/docs/development/ui)内所有文章。

Flutter 官方布局使用`flex`，语法同 css 类似，flex 一些补充参考可查看[阮一峰的博客](https://ruanyifeng.com/blog/2015/07/flex-grammar.html)

[核心 Widget 目录](https://flutter.cn/docs/development/ui/widgets)——分类展示常用`Widget`
[Flutter Widget 目录](https://flutter.cn/docs/reference/widgets)——所有官方提供的`Widget`目录

本项目部分组件引用了[GetWidget](https://www.getwidget.dev/getstarted-with-flutter-design)，其余使用官方`Widget`开发

### 状态管理

本项目使用[GetX](https://github.com/jonataslaw/getx)进行状态管理、路由管理和依赖管理。全局样式、语言变更，也通过`GetX`实现。

#### GetX 常用方法

```dart
Get.dialog(widget);
Get.showOverlay(asyncFunction: asyncFunction);
Get.showSnackbar(GetSnackBar());
Get.rawSnackbar();
Get.defaultDialog();
Get.generalDialog(pageBuilder: pageBuilder);
Get.snackbar(title, message);
Get.bottomSheet(bottomsheet);
Get.forceAppUpdate();
Get.changeTheme(theme);
Get.changeThemeMode(themeMode);
Get.updateLocale(l);
```

#### 编写状态类

TODO doc

#### 初始化状态

TODO doc

#### 获取状态

TODO doc

### 持久化存储

本项目使用[isar](https://isar.dev/tutorials/quickstart.html#_1-add-dependencies)作为本地 NoSQL 数据库进行持久化数据存储。

持久化存储与状态管理类相关联。在状态管理初始化时，尝试从`isar`数据库取数据，并在每个更新操作同步更新本地数据库。

#### 使用方式

1. 创建模型声明类
2. 自动生成静态代码：`flutter pub run build_runner build`
3. 调用 Isar 实例：`final isar = await Isar.open([EmailSchema]);`

### 样式编写

所有样式部分，涉及到颜色及字体大小，均需从当前激活的全局`Themeta`中获取，布局信息可根据 UI 直接编写。

TODO 示例

样式编写可参考[在线编辑器](https://appainter.dev/)
[Material3 在线编辑器](https://m3.material.io/theme-builder#/custom)

#### 样式主题

主题类均定义在 theme 文件夹内，通过以下代码可快捷切换样式主题。

```dart
Get.changeTheme(ThemeData.light());
```

日常编写样式与 flutter 默认方式一致，需注意颜色和字体大小需要使用当前主题样式。

#### 弹窗组件


GetX 框架

```dart
Get.snackbar()
Get.showSnackbar(GetSnackBar(message: "请至少选择一个章节".tr));
```

Toast：fluttertoast: ^8.1.1

### 路由和导航

```dart
Get.toNamed(Routs.LOGIN);
```

### 文本翻译（多语言）

所有需要展示的固定文本，直接在字符串后加`.tr`即可。

如果文本含有参数，需要在字符串后面加`.trParams([Map<String, String>])`。
同时，翻译内容需要用占位符`@key`替代

例如

```dart
// home.dart
'今日需背x偈'.trParams({
                "times": restRecites.toString(),
              })

// zh_CN.dart
{'今日需背x偈': '今日需背 @times偈'}
```

### 网络请求

#### 封装结构

TODO doc

#### 新接口编写规范

TODO doc

#### 接口约束

本项目使用`Dio`管理网络请求，使用`JWT`进行鉴权，并将其共同封装为全局单例来统一管理网络及 JWT 状态。

前后端使用 json 进行通信，接口状态使用 HTTP 状态码，2xx 为成功，4xx、5xx 会触发错误，上层`catchError`会被调用。

##### Http 响应结构

后台错误通过 Http 状态码反应，使用`4xx`及`5xx`表示，响应体中需有`message`字段说明错误类型或原因。

接口正常则直接在`json`根字段返回数据

##### Http 状态码规范

401 代表用户没有权限
409 代表用户被其他客户端登录踢出

### 日志及埋点上报

#### Sentry错误监控

Then create an intentional error, so you can test that everything is working:

```dart
import 'package:sentry/sentry.dart';

try {
  aMethodThatMightFail();
} catch (exception, stackTrace) {
  await Sentry.captureException(
    exception,
    stackTrace: stackTrace,
  );
}
```

#### Sentry性能监控


```dart
import 'package:sentry/sentry.dart';

final transaction = Sentry.startTransaction('processOrderBatch()', 'task');

try {
  await processOrderBatch(transaction);
} catch (exception) {
  transaction.throwable = exception;
  transaction.status = SpanStatus.internalError();
} finally {
  await transaction.finish();
}

Future<void> processOrderBatch(ISentrySpan span) async {
  // span operation: task, span description: operation
  final innerSpan = span.startChild('task', description: 'operation');

  try {
    // omitted code
  } catch (exception) {
    innerSpan.throwable = exception;
    innerSpan.status = SpanStatus.notFound();
  } finally {
    await innerSpan.finish();
  }
}
```

#### Posthog用户操作埋点上报

TODO

### 离线同步——初期只考虑网络可用与否，不考虑弱网

当前规划，使用 Isar 数据库作为后端，通过时间戳排序将其抽象为消息队列（FIFO）后端。每次请求发送首先将内容结构化存储 Isar 数据库（待定，也可请求无响应后存入数据库，弱网环境判断&提醒），请求获得响应（不论 HTTP 状态码为多少）则消费掉。联网后触发 hook，此时如果数据库内含有数据，则推送至后端，沟通结构待定，也可批量重发请求。如果为弱网环境，则定时尝试重发请求，并限制请求体大小。

使用`connectivity_plus`插件检测网络状态

## 构建发布说明

### 构建性能模式用于测试

flutter build apk --profile --build-name 1.1.0 --build-number 1 --split-per-abi

flutter build ipa --build-name 1.1.0 --build-number 2

flutter build apk  --build-name 1.3.4

### 构建发布正式版

参考[官方文档](https://flutter.cn/docs/deployment/obfuscate)

```sh
flutter build apk --obfuscate --split-debug-info=./android/1.3.3 --build-name 1.3.3 --build-number 1 --split-per-abi

flutter build ipa --obfuscate --split-debug-info=./ios/1.3.3 --build-name 1.3.3 --build-number 10 # --export-method ad-hoc
```

## 调试方法

1. [Flutter inspector](https://flutter.cn/docs/development/tools/devtools/inspector)
   vscode 会默认打开 Flutter inspector 的 Widget inspector，可自行在右下角点击`Dart DevTools`使用其他工具页。
1. vscode 按`F5`进入调试状态，支持断点调试

## 开发工具

图标制作：https://icon.wuruihong.com/
官方仓库：https://pub.flutter-io.cn/
Flutter 实战：https://book.flutterchina.club/
在线编辑器：https://appainter.dev/
