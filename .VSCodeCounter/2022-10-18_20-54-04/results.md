# Summary

Date : 2022-10-18 20:54:04

Directory e:\\Project\\zzjs-client

Total : 65 files,  3263 codes, 335 comments, 576 blanks, all 4174 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| Dart | 21 | 1,710 | 76 | 243 | 2,029 |
| C++ | 16 | 506 | 107 | 170 | 783 |
| XML | 10 | 467 | 48 | 12 | 527 |
| JSON | 4 | 248 | 0 | 2 | 250 |
| Markdown | 2 | 116 | 0 | 86 | 202 |
| Groovy | 3 | 102 | 4 | 23 | 129 |
| Swift | 4 | 38 | 3 | 12 | 53 |
| HTML | 1 | 37 | 16 | 6 | 59 |
| YAML | 2 | 31 | 81 | 20 | 132 |
| Properties | 2 | 8 | 0 | 2 | 10 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 65 | 3,263 | 335 | 576 | 4,174 |
| android | 12 | 173 | 50 | 34 | 257 |
| android\\app | 8 | 130 | 50 | 23 | 203 |
| android\\app\\src | 7 | 63 | 46 | 9 | 118 |
| android\\app\\src\\debug | 1 | 4 | 4 | 1 | 9 |
| android\\app\\src\\main | 5 | 55 | 38 | 7 | 100 |
| android\\app\\src\\main\\res | 4 | 26 | 32 | 6 | 64 |
| android\\app\\src\\main\\res\\drawable | 1 | 4 | 7 | 2 | 13 |
| android\\app\\src\\main\\res\\drawable-v21 | 1 | 4 | 7 | 2 | 13 |
| android\\app\\src\\main\\res\\values | 1 | 9 | 9 | 1 | 19 |
| android\\app\\src\\main\\res\\values-night | 1 | 9 | 9 | 1 | 19 |
| android\\app\\src\\profile | 1 | 4 | 4 | 1 | 9 |
| android\\gradle | 1 | 5 | 0 | 1 | 6 |
| android\\gradle\\wrapper | 1 | 5 | 0 | 1 | 6 |
| ios | 7 | 222 | 2 | 8 | 232 |
| ios\\Runner | 7 | 222 | 2 | 8 | 232 |
| ios\\Runner\\Assets.xcassets | 3 | 148 | 0 | 3 | 151 |
| ios\\Runner\\Assets.xcassets\\AppIcon.appiconset | 1 | 122 | 0 | 0 | 122 |
| ios\\Runner\\Assets.xcassets\\LaunchImage.imageset | 2 | 26 | 0 | 3 | 29 |
| ios\\Runner\\Base.lproj | 2 | 61 | 2 | 2 | 65 |
| lib | 20 | 1,696 | 66 | 236 | 1,998 |
| lib\\api | 2 | 78 | 25 | 27 | 130 |
| lib\\components | 2 | 0 | 0 | 2 | 2 |
| lib\\controllers | 1 | 76 | 1 | 17 | 94 |
| lib\\pages | 6 | 271 | 10 | 26 | 307 |
| lib\\routes | 2 | 24 | 6 | 10 | 40 |
| lib\\schemas | 2 | 1,176 | 6 | 139 | 1,321 |
| lib\\translations | 3 | 31 | 2 | 7 | 40 |
| linux | 5 | 98 | 27 | 38 | 163 |
| linux\\flutter | 2 | 12 | 9 | 11 | 32 |
| macos | 5 | 437 | 3 | 11 | 451 |
| macos\\Flutter | 1 | 6 | 3 | 4 | 13 |
| macos\\Runner | 4 | 431 | 0 | 7 | 438 |
| macos\\Runner\\Assets.xcassets | 1 | 68 | 0 | 0 | 68 |
| macos\\Runner\\Assets.xcassets\\AppIcon.appiconset | 1 | 68 | 0 | 0 | 68 |
| macos\\Runner\\Base.lproj | 1 | 343 | 0 | 1 | 344 |
| test | 1 | 14 | 10 | 7 | 31 |
| web | 2 | 72 | 16 | 7 | 95 |
| windows | 10 | 407 | 80 | 131 | 618 |
| windows\\flutter | 2 | 11 | 9 | 11 | 31 |
| windows\\runner | 8 | 396 | 71 | 120 | 587 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)