# Details

Date : 2022-10-18 20:54:04

Directory e:\\Project\\zzjs-client

Total : 65 files,  3263 codes, 335 comments, 576 blanks, all 4174 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [README.md](/README.md) | Markdown | 113 | 0 | 84 | 197 |
| [analysis_options.yaml](/analysis_options.yaml) | YAML | 3 | 23 | 4 | 30 |
| [android/app/build.gradle](/android/app/build.gradle) | Groovy | 67 | 4 | 14 | 85 |
| [android/app/src/debug/AndroidManifest.xml](/android/app/src/debug/AndroidManifest.xml) | XML | 4 | 4 | 1 | 9 |
| [android/app/src/main/AndroidManifest.xml](/android/app/src/main/AndroidManifest.xml) | XML | 29 | 6 | 1 | 36 |
| [android/app/src/main/res/drawable-v21/launch_background.xml](/android/app/src/main/res/drawable-v21/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android/app/src/main/res/drawable/launch_background.xml](/android/app/src/main/res/drawable/launch_background.xml) | XML | 4 | 7 | 2 | 13 |
| [android/app/src/main/res/values-night/styles.xml](/android/app/src/main/res/values-night/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [android/app/src/main/res/values/styles.xml](/android/app/src/main/res/values/styles.xml) | XML | 9 | 9 | 1 | 19 |
| [android/app/src/profile/AndroidManifest.xml](/android/app/src/profile/AndroidManifest.xml) | XML | 4 | 4 | 1 | 9 |
| [android/build.gradle](/android/build.gradle) | Groovy | 27 | 0 | 5 | 32 |
| [android/gradle.properties](/android/gradle.properties) | Properties | 3 | 0 | 1 | 4 |
| [android/gradle/wrapper/gradle-wrapper.properties](/android/gradle/wrapper/gradle-wrapper.properties) | Properties | 5 | 0 | 1 | 6 |
| [android/settings.gradle](/android/settings.gradle) | Groovy | 8 | 0 | 4 | 12 |
| [ios/Runner/AppDelegate.swift](/ios/Runner/AppDelegate.swift) | Swift | 12 | 0 | 2 | 14 |
| [ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json](/ios/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json) | JSON | 122 | 0 | 0 | 122 |
| [ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json](/ios/Runner/Assets.xcassets/LaunchImage.imageset/Contents.json) | JSON | 23 | 0 | 1 | 24 |
| [ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md](/ios/Runner/Assets.xcassets/LaunchImage.imageset/README.md) | Markdown | 3 | 0 | 2 | 5 |
| [ios/Runner/Base.lproj/LaunchScreen.storyboard](/ios/Runner/Base.lproj/LaunchScreen.storyboard) | XML | 36 | 1 | 1 | 38 |
| [ios/Runner/Base.lproj/Main.storyboard](/ios/Runner/Base.lproj/Main.storyboard) | XML | 25 | 1 | 1 | 27 |
| [ios/Runner/Runner-Bridging-Header.h](/ios/Runner/Runner-Bridging-Header.h) | C++ | 1 | 0 | 1 | 2 |
| [lib/api/api.dart](/lib/api/api.dart) | Dart | 59 | 23 | 20 | 102 |
| [lib/api/user.dart](/lib/api/user.dart) | Dart | 19 | 2 | 7 | 28 |
| [lib/components/gatha_card.dart](/lib/components/gatha_card.dart) | Dart | 0 | 0 | 1 | 1 |
| [lib/components/recite_range.dart](/lib/components/recite_range.dart) | Dart | 0 | 0 | 1 | 1 |
| [lib/config.dart](/lib/config.dart) | Dart | 4 | 2 | 2 | 8 |
| [lib/controllers/user.dart](/lib/controllers/user.dart) | Dart | 76 | 1 | 17 | 94 |
| [lib/main.dart](/lib/main.dart) | Dart | 36 | 14 | 6 | 56 |
| [lib/pages/demo.dart](/lib/pages/demo.dart) | Dart | 32 | 5 | 9 | 46 |
| [lib/pages/gatha_detail.dart](/lib/pages/gatha_detail.dart) | Dart | 0 | 0 | 1 | 1 |
| [lib/pages/gatha_list.dart](/lib/pages/gatha_list.dart) | Dart | 14 | 1 | 3 | 18 |
| [lib/pages/home.dart](/lib/pages/home.dart) | Dart | 136 | 3 | 5 | 144 |
| [lib/pages/login.dart](/lib/pages/login.dart) | Dart | 77 | 1 | 5 | 83 |
| [lib/pages/plan.dart](/lib/pages/plan.dart) | Dart | 12 | 0 | 3 | 15 |
| [lib/routes/pages.dart](/lib/routes/pages.dart) | Dart | 16 | 0 | 3 | 19 |
| [lib/routes/routes.dart](/lib/routes/routes.dart) | Dart | 8 | 6 | 7 | 21 |
| [lib/schemas/user.dart](/lib/schemas/user.dart) | Dart | 14 | 0 | 10 | 24 |
| [lib/schemas/user.g.dart](/lib/schemas/user.g.dart) | Dart | 1,162 | 6 | 129 | 1,297 |
| [lib/translations/en_US.dart](/lib/translations/en_US.dart) | Dart | 3 | 1 | 2 | 6 |
| [lib/translations/translations.dart](/lib/translations/translations.dart) | Dart | 10 | 0 | 3 | 13 |
| [lib/translations/zh_CN.dart](/lib/translations/zh_CN.dart) | Dart | 18 | 1 | 2 | 21 |
| [linux/flutter/generated_plugin_registrant.cc](/linux/flutter/generated_plugin_registrant.cc) | C++ | 7 | 4 | 5 | 16 |
| [linux/flutter/generated_plugin_registrant.h](/linux/flutter/generated_plugin_registrant.h) | C++ | 5 | 5 | 6 | 16 |
| [linux/main.cc](/linux/main.cc) | C++ | 5 | 0 | 2 | 7 |
| [linux/my_application.cc](/linux/my_application.cc) | C++ | 74 | 11 | 20 | 105 |
| [linux/my_application.h](/linux/my_application.h) | C++ | 7 | 7 | 5 | 19 |
| [macos/Flutter/GeneratedPluginRegistrant.swift](/macos/Flutter/GeneratedPluginRegistrant.swift) | Swift | 6 | 3 | 4 | 13 |
| [macos/Runner/AppDelegate.swift](/macos/Runner/AppDelegate.swift) | Swift | 8 | 0 | 2 | 10 |
| [macos/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json](/macos/Runner/Assets.xcassets/AppIcon.appiconset/Contents.json) | JSON | 68 | 0 | 0 | 68 |
| [macos/Runner/Base.lproj/MainMenu.xib](/macos/Runner/Base.lproj/MainMenu.xib) | XML | 343 | 0 | 1 | 344 |
| [macos/Runner/MainFlutterWindow.swift](/macos/Runner/MainFlutterWindow.swift) | Swift | 12 | 0 | 4 | 16 |
| [pubspec.yaml](/pubspec.yaml) | YAML | 28 | 58 | 16 | 102 |
| [test/widget_test.dart](/test/widget_test.dart) | Dart | 14 | 10 | 7 | 31 |
| [web/index.html](/web/index.html) | HTML | 37 | 16 | 6 | 59 |
| [web/manifest.json](/web/manifest.json) | JSON | 35 | 0 | 1 | 36 |
| [windows/flutter/generated_plugin_registrant.cc](/windows/flutter/generated_plugin_registrant.cc) | C++ | 6 | 4 | 5 | 15 |
| [windows/flutter/generated_plugin_registrant.h](/windows/flutter/generated_plugin_registrant.h) | C++ | 5 | 5 | 6 | 16 |
| [windows/runner/flutter_window.cpp](/windows/runner/flutter_window.cpp) | C++ | 45 | 4 | 13 | 62 |
| [windows/runner/flutter_window.h](/windows/runner/flutter_window.h) | C++ | 20 | 5 | 9 | 34 |
| [windows/runner/main.cpp](/windows/runner/main.cpp) | C++ | 30 | 4 | 10 | 44 |
| [windows/runner/resource.h](/windows/runner/resource.h) | C++ | 9 | 6 | 2 | 17 |
| [windows/runner/utils.cpp](/windows/runner/utils.cpp) | C++ | 53 | 2 | 10 | 65 |
| [windows/runner/utils.h](/windows/runner/utils.h) | C++ | 8 | 6 | 6 | 20 |
| [windows/runner/win32_window.cpp](/windows/runner/win32_window.cpp) | C++ | 183 | 15 | 48 | 246 |
| [windows/runner/win32_window.h](/windows/runner/win32_window.h) | C++ | 48 | 29 | 22 | 99 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)