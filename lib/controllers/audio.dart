import 'dart:async';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';

import '../config.dart';
import '../entities/audio.dart';
import '../utils/download.dart';
import 'global.dart';

class AudioDetailController extends GetxController with StateMixin {
  static AudioDetailController get to => Get.find();

  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache = AudioCache();

  Duration? totalDuration ;
  Duration? currentPosition ;

  AudioModel? entity;

  String fileName = '';

  PlayerState? playerState;

  ///判断是否播放
  Rx<bool> isPlaying = false.obs;

  ///是否在下载,0 未下载，1已下载，2正在下载
  Rx<int> audioLoading = 0.obs;

  int showAudio = -1;

  StreamSubscription? durationSubscription;

  StreamSubscription? positionSubscription;

  StreamSubscription? playerCompleteSubscription;

  StreamSubscription? playerStateChangeSubscription;


  String?  durationText ;

  String?  positionText ;

  String?  remainingTime ;

  Rx<String?>  playingTime = ''.obs;
  Rx<double>  sliderProgress = (0.0).obs ;
  // Rx<num>  sliderProgress = 0.obs ;
  // var sliderProgress = Rx<Double>(0 as Double).obs;

  @override
  Future<void> onInit() async {
    super.onInit();
    Map<String, dynamic> arguments = Get.arguments;
    entity = arguments['entity'];


    playerState == PlayerState.playing;
    playerState = audioPlayer.state;
    updatePlayingStatus();

    audioPlayer.getDuration().then(
            (value) {
              totalDuration = value;
            },
        );
    audioPlayer.getCurrentPosition().then(
          (value)  {
            currentPosition = value;
          },
    );
    _initStreams();
  }

  Future<void> getAudioAndPlay() async {
    audioLoading.value = 2;

    ///这要加个版本判断
    ///存数据库
    // if (entity != null) {
    //   await GlobalService.to.isar.writeTxn(() async {
    //     await GlobalService.to.isar.audioModels.put(entity!);
    //   });
    // }

    if (entity!.audioUrl.isNotEmpty && entity!.audioUrl != '') {
      int lastIndex = entity?.audioUrl.lastIndexOf('/') ?? 0;

      if (lastIndex > 0) {
        fileName = entity?.audioUrl.substring(lastIndex + 1, entity?.audioUrl.length) ?? '';

        if (fileName != '') {
          String? urlStr = '';
          print('why:fileName:${Config.audioPath}/$fileName');
          if (!await isFileExist('${Config.audioPath}/$fileName')) {
             urlStr = await downloadFile(entity!.audioUrl, fileName);

            ///http://zzjs-dev.bj-kyhx.com:1081/audio/C1_JIE_PIN_FULL.mp3
            print('why：下载音频文件成功！');
          }else{
            urlStr = '${Config.audioPath}/$fileName';
          }

            if(urlStr!=null){
              audioLoading.value = 1;
            }
            await audioPlayer.stop();
            await audioPlayer.setSource(DeviceFileSource(urlStr!));
            await audioPlayer.play(DeviceFileSource(urlStr));
            audioPlayer.onPlayerComplete.listen((event) {
              if (kDebugMode) {
                print('player is complete');
              }
            });

            playerState = PlayerState.playing;
            updatePlayingStatus();

        }
      }
    }
  }

  void _initStreams() {
    durationSubscription = audioPlayer.onDurationChanged.listen((duration) {
      totalDuration = duration;
      updatePlayingTime();
    });

    positionSubscription = audioPlayer.onPositionChanged.listen((duration) {
      currentPosition = duration;
      updatePlayingTime();
    }
    );

    // _playerCompleteSubscription = player.onPlayerComplete.listen((event) {
    //   setState(() {
    //     _playerState = PlayerState.stopped;
    //     _currentPosition = Duration.zero;
    //   });
    // });

    playerStateChangeSubscription =
        audioPlayer.onPlayerStateChanged.listen((state) {
      playerState = state;
      updatePlayingStatus();
      updatePlayingTime();
    });
  }

  Future<void> playFromStorage() async {
    String filePath = '${Config.audioPath}/$fileName'; // 替换为您的音频文件路径

    print('why：$filePath');
    // await audioPlayer.play(DeviceFileSource(filePath));
    audioPlayer.play(UrlSource(
        'http://zzjs-dev.bj-kyhx.com:1081/audio/C1_JIE_PIN_FULL.mp3'));
    // audioPlayer.play(UrlSource('https://example.com/audio.mp3'));
  }

  Future backMusicPlay() async {
    // await audioPlayer.play(AssetSource('audio/jp1.mp3'));
    await audioPlayer.resume();
  }

  Future<void> play() async {
    if(audioLoading.value==0){
      getAudioAndPlay();
    }
    audioPlayer.resume();
    playerState = PlayerState.playing;
    updatePlayingStatus();
  }

  Future<void> pause() async {
    audioPlayer.pause();
    playerState = PlayerState.paused;
    updatePlayingStatus();
  }

  Future<void> stop() async {
    audioPlayer.stop();
    playerState = PlayerState.stopped;
    updatePlayingStatus();
  }

  String _formatDuration(Duration duration) {
    int minutes = duration.inMinutes.remainder(60);
    int seconds = duration.inSeconds.remainder(60);
    return '$minutes:${seconds.toString().padLeft(2, '0')}';
  }

  void updatePlayingStatus() {
    isPlaying.value = playerState == PlayerState.playing;
  }

  void updatePlayingTime(){

    durationText = totalDuration?.toString().split('.').first ?? '';
    positionText = currentPosition?.toString().split('.').first ?? '';
    remainingTime = currentPosition != null && totalDuration != null
        ? _formatDuration(totalDuration! - currentPosition!)
        : '00:00';

    playingTime.value = currentPosition != null
    // ? '$_positionText / $_durationText'
        ? remainingTime
        : totalDuration != null
        ? durationText
        : '';

    sliderProgress.value = (currentPosition != null &&
        totalDuration != null &&
        currentPosition!.inMilliseconds > 0 &&
        currentPosition!.inMilliseconds < totalDuration!.inMilliseconds)
        ? currentPosition!.inMilliseconds / totalDuration!.inMilliseconds
        : 0.0;
  }

  // @override
  // void dispose() {
  //   audioPlayer.dispose();
  //   durationSubscription?.cancel();
  //   positionSubscription?.cancel();
  //   playerCompleteSubscription?.cancel();
  //   playerStateChangeSubscription?.cancel();
  //   stop();
  //   super.dispose();
  // }

  @override
  void onClose() {
    audioPlayer.dispose();
    durationSubscription?.cancel();
    positionSubscription?.cancel();
    playerCompleteSubscription?.cancel();
    playerStateChangeSubscription?.cancel();
    stop();
    super.onClose();
  }
}
