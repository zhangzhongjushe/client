import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/api/plan.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/entities/recite_plan.dart';
import 'package:zzjs_client/schemas/book.dart';

/// 背诵计划状态管理
///
/// 负责管理背诵计划的展示、调整，不可离线使用，无需同步本地缓存
class RecitePlanController extends GetxController with StateMixin {
  static RecitePlanController get to => Get.find();

  int minSpeed = 1;
  int maxSpeed = 100;

  /// 可选择修改的背诵速度
  RxMap<int, String> speedChoices = <int, String>{}.obs;

  /// 全部未进行的计划，不包含当前正在进行，优先展示未完成计划
  RxList<RecitePlanItem> allPlans = <RecitePlanItem>[].obs;

  /// 当前计划
  // RecitePlanItem get onGoingPlan => _onGoingPlan.value;
  final Rx<RecitePlanItem> onGoingPlan = RecitePlanItem.zero().obs;

  /// 初始化状态
  ///
  /// 获取背诵计划信息，无网络则报错，并返回首页
  @override
  onReady() async {
    try {
      await fetchPlans();
    } catch (e, trace) {
      Sentry.captureException(e, stackTrace: trace);
    }

    // TODO: monitor
    // Get.snackbar("finished", "message");
    super.onReady();
    change(null, status: RxStatus.success());
  }

  /// 创建背诵计划
  Future<void> createRecitePlan(int bookId, List<int> gathas) async {
    // 发送请求创建计划
    // 创建计划默认寄送速度是1
    await createPlan(bookId, 1, gathas);

    // 创建成功后重新获取计划列表
    var plan = await fetchPlans();

    await changeCurrentPlan(plan.id, bookId);
  }

  /// 刷新列表，重新获取内容
  Future<RecitePlanItem> fetchPlans() async {
    List<RecitePlanItem> res = await getAllPlan();

    allPlans.clear();
    RecitePlanItem? current;
    for (var e in res) {
      if (e.isCurrent) {
        onGoingPlan(e);
        current = e;
      } else {
        allPlans.add(e);
      }
    }

    // 计划列表排序
    sortPlanList();

    return current!;
  }

  /// 生成当前计划背诵速度修改选项
  List<Widget> generateSpeedPlanList() {
    int total = onGoingPlan.value.total;
    int restTotal = total - onGoingPlan.value.finishedCount;
    maxSpeed = min(100, total);
    speedChoices.clear();
    for (var i = 0; i < total; i++) {
      speedChoices[i] =
          "${"x个".trArgs([(i + 1).toString()])}        ${"x日".trArgs([
            ((restTotal) / (i + 1)).ceil().toString()
          ])}";
    }

    return List<Widget>.generate(speedChoices.length, (int index) {
      return Center(
        child: Text(
          RecitePlanController.to.speedChoices[index]!,
        ),
      );
    });
  }

  /// 修改当前计划背诵速度
  Future<bool> changeReciteSpeed(int speed) async {
    await updatePlan(onGoingPlan.value.id.toString(), speed);
    onGoingPlan.update((val) {
      val!.speed = speed;
    });
    return true;
  }

  /// 计划列表排序
  ///
  /// 1、列表分两部分，第一部分展示未完成的计划，第二部分展示已完成的计划
  /// 2、未完成的计划，按创建时间降序排序
  /// 3、已完成的计划，按完成时间降序排序
  void sortPlanList() {
    allPlans.sort(
      // 更换a、b位置，使升序变为降序排序
      (RecitePlanItem a, RecitePlanItem b) {
        if (a.isFinished && !b.isFinished) {
          return 1;
        } else if (!a.isFinished && b.isFinished) {
          return -1;
        }

        // 相比较的两个计划都处于同一状态时候，按创建时间、完成时间比较
        if (a.isFinished) {
          // 处于已完成状态，按完成时间降序排序
          return b.lastFinishedAt.compareTo(a.lastFinishedAt);
        } else {
          // 处于未完成状态，按创建时间降序排序
          return b.createdAt.compareTo(a.createdAt);
        }
      },
    );
  }

  /// 删除背诵计划
  deleteRecitePlan(int index) async {
    // 删除二次确认
    Get.dialog(
      CupertinoAlertDialog(
        title: Text("删除内容".tr),
        content: Text("您已背诵了该计划的x个偈颂，删除后将无法恢复，确定要删除？".trArgs([
          RecitePlanController.to.allPlans[index].finishedCount.toString()
        ])),
        actions: <CupertinoDialogAction>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Get.back();
            },
            child: Text("取消".tr),
          ),
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () async {
              await _deleteRecitePlan(index);
            },
            child: Text("确定".tr),
          ),
        ],
      ),
    );
  }

  _deleteRecitePlan(int index) async {
    Posthog().capture(
      eventName: '确认删除计划',
      properties: {'id': allPlans[index].id},
    );
    await deletePlan(allPlans[index].id.toString());
    allPlans.removeAt(index);
    Get.back();
  }

  /// 切换学习计划
  changeRecitePlan(int index) async {
    // 切换计划二次确认
    Get.dialog(
      CupertinoAlertDialog(
        title: Text("切换学习内容".tr),
        content: Text("确定要切换学习范围？".tr),
        actions: <CupertinoDialogAction>[
          CupertinoDialogAction(
            isDestructiveAction: true,
            onPressed: () {
              Get.back();
            },
            child: Text("取消".tr),
          ),
          CupertinoDialogAction(
            isDefaultAction: true,
            onPressed: () async {
              await _changeRecitePlan(index);
            },
            child: Text("确定".tr),
          ),
        ],
      ),
    );
  }

  _changeRecitePlan(int index) async {
    try {
      Posthog().capture(
        eventName: '更改学习计划',
        properties: {'id': allPlans[index].id},
      );
      await changePlan(allPlans[index].id.toString());

      var newOnGoingPlan = allPlans[index];
      allPlans.removeAt(index);
      allPlans.add(onGoingPlan.value);
      onGoingPlan(newOnGoingPlan);

      // 计划列表排序
      sortPlanList();

      await changeCurrentPlan(newOnGoingPlan.id, newOnGoingPlan.bookId);
    } catch (e, s) {
      Sentry.captureException(e, stackTrace: s);
    } finally {
      Get.back();
    }
  }

  changeCurrentPlan(int planId, int bookId) async {
    // 修改全局状态
    GlobalService.to.changeCurrentPlan(planId, bookId);

    // 清空当前进度
    await GlobalService.to.isar
        .writeTxn(() => GlobalService.to.isar.progressModels.clear());

    await ReciteProgressController.to.resetData();
  }
}
