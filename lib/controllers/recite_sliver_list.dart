import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/api/plan.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';

/// 偈颂列表外层TabController
class ReciteListTabController extends GetxController
    with GetSingleTickerProviderStateMixin {
  static ReciteListTabController get to => Get.find();

  TabController? tabController;

  @override
  void onInit() {
    tabController = TabController(
      length: 4,
      initialIndex: 0,
      vsync: this,
    );
    tabController!.addListener(() {
      // TODO：log 监听tab切换事件，全部、已背熟、不熟、未背
      if (tabController!.index.toDouble() == tabController!.animation!.value) {
        Posthog().capture(
          eventName: '切换偈颂列表tab',
          properties: {
            'previous':
                reciteStatusTitleMap[tabController!.previousIndex] ?? '',
            'current': reciteStatusTitleMap[tabController!.index] ?? '',
          },
        );
      }
    });
    super.onInit();
  }

  getCurrentTab() => reciteStatusTitleMap[tabController!.index];
}

/// tabView中偈颂列表控制器
///
/// 初始化时传入当前tab需要展示的偈颂列表
class ReciteSliverListController extends GetxController {
  /// 当前计划包含的所有偈颂，直接从后端获取
  /// FIXME：当前计划列表有概率不同步
  RxList<GathaModel?> reciteGathaList = RxList();

  /// 当前偈颂卡片索引
  RxInt currentIndex = RxInt(0);

  /// 当前偈颂内容所在章节
  RxString chapterTitle = "".obs;

  /// 当前显示白话解释的偈颂索引
  RxInt showExp = RxInt(-1);

  /// 当前显示白话解释的偈颂索引
  RxInt showChart = RxInt(-1);

  /// 上次背诵到的位置
  RxInt lastIndex = RxInt(0);

  ScrollController? reciteListController;

  /// PageView 背诵卡片滚动控制器
  final Rx<PageController> _controller = Rx(PageController(
    initialPage: 0,
    viewportFraction: .9,
  ));

  /// 获取长度
  int get length => reciteGathaList.length;

  /// 偈颂详情页面初始化
  initPageViewController() {
    _controller(PageController(
      initialPage: currentIndex.value,
      viewportFraction: .9,
    ));

    chapterTitle.value = getCurrentGathaData().chapter.value?.name ?? "章节错误";

    // 监听背诵卡片切换
    _controller().addListener(() {
      // 切换后读取当前偈颂所在章节、背诵状态
      currentIndex.value =
          _controller.value.page?.round() ?? currentIndex.value;
      var model = getCurrentGathaData();
      chapterTitle.value = model.chapter.value?.name ?? "章节错误";
      if (_controller().page == _controller().page?.round()) {
        Posthog().capture(
          eventName: '（滑动）切换偈颂',
          properties: {
            'current': getCurrentGathaData().id,
          },
        );
      }
      update();
    });
  }

  PageController get reciteCardController => _controller.value;

  void sortGathaList() {
    // 根据最后更新时间 progress.updatedAt 信息进行降序排序
    reciteGathaList.sort((a, b) =>
        b!.progress.value!.updatedAt.compareTo(a!.progress.value!.updatedAt));

    /// 上次背诵的偈颂
    GathaModel lastGatha = reciteGathaList[0]!;

    // 根据order信息进行升序排序
    reciteGathaList.sort((a, b) => a!.order.compareTo(b!.order));
    var lastIndex = 0;
    for (GathaModel? element in reciteGathaList) {
      if (element!.id == lastGatha.id) {
        this.lastIndex.value = lastIndex;
        break;
      }
      lastIndex++;
    }

    GathaModel? prev;
    for (GathaModel? element in reciteGathaList) {
      if (prev != null) {
        prev.next = element;
        element?.prev = prev;
      }
      prev = element;
    }
  }

  GathaModel getGathaData(int index) {
    return reciteGathaList[index]!;
  }

  GathaModel getCurrentGathaData() {
    return reciteGathaList[currentIndex.value]!;
  }

  toPreviousGatha() {
    reciteCardController.previousPage(
        duration: const Duration(milliseconds: 600), curve: Curves.easeOutBack);
  }

  toNextGatha() {
    reciteCardController.nextPage(
        duration: const Duration(milliseconds: 600), curve: Curves.easeOutBack);
  }

  /// 更新偈颂状态
  ///
  /// 更新当前背诵卡片的状态
  /// 更新背诵进度为当前卡片
  /// 同步更新本地缓存，并尝试网络同步
  Future<void> onUpdate(ReciteStatus status) async {
    // 等待动画完成才可点击不熟、已完成按钮
    var pageIndex = reciteCardController.page;
    if (pageIndex?.toInt() != pageIndex) {
      return;
    }
    lastIndex.value = pageIndex!.toInt();

    // 不熟、已背熟状态均视为完成
    // 如果状态最后变更时间在今日之前，或原状态为none，则完成度+1
    int lastUpdatedAt =
        reciteGathaList[currentIndex.value]!.progress.value!.updatedAt;
    ReciteStatus lastStatus =
        reciteGathaList[currentIndex.value]!.progress.value!.status;
    int timestampToday =
        (DateTime(DateTime.now().year, DateTime.now().month, DateTime.now().day)
                    .millisecondsSinceEpoch /
                1000)
            .floor();
    if (lastUpdatedAt < timestampToday || lastStatus == ReciteStatus.none) {
      GlobalService.to.data.update((val) {
        val!.finishedRecitesToday++;
        val.finishedRecitesTotal++;
      });
    }

    int nowTS = (DateTime.now().millisecondsSinceEpoch / 1000).floor();
    reciteGathaList[currentIndex.value]!.progress.value!
      ..status = status
      ..updatedAt = nowTS;

    await updateProgress(
      GlobalService.to.currentPlanId.value,
      reciteGathaList[currentIndex.value]!.id,
      status.index,
      nowTS,
    );

    await GlobalService.to.isar.writeTxn(() async {
      await GlobalService.to.isar.progressModels
          .put(reciteGathaList[currentIndex.value]!.progress.value!);
    });

    if (GlobalService.to.data().finishedRecitesTotal ==
        GlobalService.to.data().totalRecites) {
      // 完成计划弹出计划完成页面，如果已经弹出过则不重复弹出
      // 完成计划提示文案变更
      if (!GlobalService.to.data.value.isPlanFinished) {
        GlobalService.to.setGlobalData(isPlanFinished: true);
        Get.toNamed(Routes.DAILY_FINISHED, arguments: true);
      }
    } else if (GlobalService.to.data.value.finishedRecitesToday ==
        GlobalService.to.data.value.speed) {
      // 完成今日任务后弹出任务完成页
      Get.toNamed(Routes.DAILY_FINISHED);
    }

    if (getCurrentGathaData().next != null) {
      toNextGatha();
    } else {
      // 不知道为什么手动更新UI不更新，必须要触发listener中的方法才会触发UI更新
      reciteCardController.animateTo(
        reciteCardController.offset - 1,
        duration: const Duration(milliseconds: 1),
        curve: Curves.easeOutBack,
      );
    }

    update();
    return;
  }
}
