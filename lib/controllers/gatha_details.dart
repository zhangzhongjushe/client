import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/foundation.dart';
import 'package:get/get.dart';
import 'package:zzjs_client/api/gatha.dart';
import 'package:zzjs_client/entities/gatha.dart';
import 'package:zzjs_client/schemas/book.dart';

import 'global.dart';

class GathaDetailsController extends GetxController {
  /// 音频播放器
  final player = AudioPlayer();

  /// 播放当前偈颂音频
  RxInt showAudio = RxInt(0);

  /// 显示当前偈颂图表
  RxBool showImg = RxBool(false);

  /// 播放音频
  Rx<PlayerState> playState = Rx<PlayerState>(PlayerState.stopped);

  bool get isPlaying => playState.value == PlayerState.playing;

  String get durationText => duration.value.toString().split('.').first ?? '';

  String get positionText => position.value.toString().split('.').first ?? '';

  // 音频进度条
  Rx<Duration> duration = Rx<Duration>(Duration.zero);

  Rx<Duration> position = Rx<Duration>(Duration.zero);

  StreamSubscription? _durationSubscription;
  StreamSubscription? _positionSubscription;
  StreamSubscription? _playerCompleteSubscription;
  StreamSubscription? _playerStateChangeSubscription;

  @override
  onInit() {
    super.onInit();
    player.getDuration().then(
          (value) => duration.value = value!,
        );
    player.getCurrentPosition().then(
          (value) => position.value = value!,
        );
    _initStreams();
  }

  @override
  void onClose() {
    // TODO: implement onClose
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerStateChangeSubscription?.cancel();
    super.onClose();
  }

  void _initStreams() {
    _durationSubscription = player.onDurationChanged.listen((d) {
      duration.value = d;
      update();
    });

    _positionSubscription = player.onPositionChanged.listen((p) {
      position.value = p;
      update();
    });

    _playerCompleteSubscription = player.onPlayerComplete.listen((event) {
      playState.value = PlayerState.stopped;
      position.value = Duration.zero;
      update();
    });

    _playerStateChangeSubscription =
        player.onPlayerStateChanged.listen((state) {
      playState.value = state;
      update();
    });
  }

  Future<void> play() async {
    await player.resume();
    playState.value = PlayerState.playing;
    update();
  }

  Future<void> pause() async {
    await player.pause();
    playState.value = PlayerState.paused;
    update();
  }

  Future<void> stop() async {
    await player.stop();
    playState.value = PlayerState.stopped;
    position.value = Duration.zero;
    update();
  }

  Future<void> getAudioAndPlay(GathaModel gatha, int index) async {
    showAudio.value = index;
    playState.value = PlayerState.playing;

    GathaDetails? gd = await getGathaDetails(gatha.id);

    final String? path = await getAudio(gatha, gd!.gathaVersion, gd.audioUrl);

    gatha.audioUrl = path;
    gatha.graphUrl = gd.graphUrl;

    await GlobalService.to.isar.writeTxn(() async {
      await GlobalService.to.isar.gathaModels.put(gatha);
    });

    // 加载音频文件并获取总时长
    player.onDurationChanged.listen((duration) {
      duration = duration;
    });
    await player.stop();
    await player.setSource(DeviceFileSource(path!));
    await player.play(DeviceFileSource(path));
    player.onPlayerComplete.listen((event) {
      if (kDebugMode) {
        print('player is complete');
      }
    });
    update();
  }
}
