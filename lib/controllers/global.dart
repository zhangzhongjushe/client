import 'dart:math';

import 'package:device_info_plus/device_info_plus.dart';
import 'package:get/get.dart';
import 'package:isar/isar.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:path_provider/path_provider.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/api/api.dart';
import 'package:zzjs_client/api/user.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';
import 'package:zzjs_client/schemas/global.dart';

/// 全局状态管理
class GlobalService extends GetxService {
  static GlobalService get to => Get.find();

  /// 本地数据库实例
  late Isar isar;

  RxBool isLogin = false.obs;
  RxString username = "".obs;
  RxString userId = "".obs;
  RxInt currentPlanId = RxInt(-1);
  RxInt currentBookId = RxInt(-1);

  /// 当前globalService实例模型
  Rx<GlobalModel> data = Rx(GlobalModel());

  /// 读取后不会更新的变量
  String packageInfoString = "";
  String deviceInfoString = "";

  RxInt monthFinished = 0.obs;
  RxInt workingDays = 0.obs;
  RxMap aggMonthData = RxMap();
  RxList memList = RxList();

  /// 初始化
  Future<GlobalService> init() async {
    final dir = await getApplicationDocumentsDirectory();
    isar = await Isar.open([
      GlobalModelSchema,
      BookModelSchema,
      ChapterModelSchema,
      GathaModelSchema,
      ProgressModelSchema
    ], inspector: true, directory: dir.path);

    final globalModel = await isar.globalModels.get(1);
    data(globalModel);

    if (globalModel != null && globalModel.isLogin == true) {
      userId = globalModel.userId.obs;
      username = globalModel.username.obs;
      isLogin = globalModel.isLogin.obs;
      currentPlanId = globalModel.currentPlanId.obs;
      currentBookId = globalModel.currentBookId.obs;
    } else {
      final user = GlobalModel()
        ..id = 1
        ..userId = userId.value
        ..username = username.value
        ..isLogin = isLogin.value;
      await isar.writeTxn(() async {
        await isar.globalModels.put(user);
      });
    }
    // 设置
    // timeago.setLocaleMessages('zh_CN', timeago.ZhCnMessages());
    return this;
  }

  getDeviceInfo() async {
    // 设置初始化信息
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    String brand = "";
    String sysVer = "";

    Http().request.options.headers.addAll({"ApiVer": packageInfo.version});

    packageInfoString = "${packageInfo.version}+${packageInfo.buildNumber}";

    if (GetPlatform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      deviceInfoString = androidInfo.model;
      brand = androidInfo.brand;
      sysVer = androidInfo.version.toString();
    } else if (GetPlatform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      deviceInfoString = iosInfo.utsname.machine ?? "ios unknown";
      brand = iosInfo.name ?? "-";
      sysVer = iosInfo.systemVersion ?? "-";
    } else if (GetPlatform.isWeb) {
      WebBrowserInfo webBrowserInfo = await deviceInfo.webBrowserInfo;
      deviceInfoString = webBrowserInfo.userAgent ?? "web unknown";
      brand = webBrowserInfo.browserName.name;
      sysVer = webBrowserInfo.appVersion ?? "-";
    }

    Posthog().capture(
      eventName: "启动应用",
      properties: {
        "ver": packageInfoString,
        "device": deviceInfoString,
        "brand": brand,
        "sysVer": sysVer,
      },
    );
  }

  // 登录操作，更新状态并缓存
  login(int id, String user, int planId) async {
    userId = id.toString().obs;
    username = user.obs;
    isLogin = true.obs;
    currentPlanId = planId.obs;

    Posthog().identify(
      userId: userId.value,
      userProperties: {
        "username": username.value,
        "currentPlanId": currentPlanId.value,
      },
    );
    Posthog().capture(eventName: '登录');

    Sentry.configureScope(
      (scope) => scope.setUser(SentryUser(
        id: userId.value,
        username: username.value,
      )),
    );

    await isar.writeTxn(() async {
      final globalModel = (await isar.globalModels.get(1))!
        ..userId = userId.value
        ..username = username.value
        ..isLogin = isLogin.value
        ..currentPlanId = currentPlanId.value;

      await isar.globalModels.put(globalModel);
      data(globalModel);
    });
  }

  /// 退出登录，并删除本地缓存
  Future<void> logout() async {
    isLogin = false.obs;
    username = "".obs;
    userId = "".obs;
    currentPlanId = (-1).obs;
    currentBookId = (-1).obs;

    Posthog().capture(eventName: '登出');
    Posthog().reset();
    Sentry.configureScope((scope) => scope.setUser(null));

    try {
      await userLogout();
    } catch (e, s) {
      Sentry.captureException(e, stackTrace: s);
    } finally {
      await isar.writeTxn(() async {
        final globalModel = (await isar.globalModels.get(1))!
          ..userId = ""
          ..username = ""
          ..isLogin = false
          ..currentPlanId = -1
          ..currentBookId = -1;

        await isar.globalModels.put(globalModel);
        data(globalModel);
      });
      Get.offAllNamed(Routes.LOGIN);
    }
  }

  Future<void> setGlobalData({
    int? speed,
    int? restDays,
    int? finishedRecitesTotal,
    int? totalRecites,
    int? finishedRecitesToday,
    int? lastReciteId,
    bool? isPlanFinished,
  }) async {
    await isar.writeTxn(() async {
      final globalModel = (await isar.globalModels.get(1))!;
      globalModel
        ..speed = speed ?? globalModel.speed
        ..restDays = restDays ?? globalModel.restDays
        ..finishedRecitesTotal =
            finishedRecitesTotal ?? globalModel.finishedRecitesTotal
        ..totalRecites = totalRecites ?? globalModel.totalRecites
        ..finishedRecitesToday =
            finishedRecitesToday ?? globalModel.finishedRecitesToday
        ..lastReciteId = lastReciteId ?? globalModel.lastReciteId
        ..isPlanFinished = isPlanFinished ?? globalModel.isPlanFinished;

      await isar.globalModels.put(globalModel);
      data(globalModel);
    });
  }

  int get todayRestGatha {
    int restTotal = isar.progressModels
        .filter()
        .statusEqualTo(ReciteStatus.none)
        .countSync();
    int restToday = data().speed - data().finishedRecitesToday;
    if (restToday > 0) {
      return min(restToday, restTotal);
    }
    return 0;
  }

  memorizedList() async {
    var data = await isar.gathaModels
        .filter()
        .progress((q) => q.statusEqualTo(ReciteStatus.memorized))
        .findAll();
    List<Map<String, String>> list = [];
    for (var item in data) {
      list.add({
        'id': item.id.toString(),
        'content': item.content,
        'name': item.name
      });
    }
    memList.value = list;
  }

  /// 修改当前计划id，并更新缓存
  changeCurrentPlan(int planId, int bookId) async {
    currentPlanId.value = planId;
    currentBookId.value = bookId;

    await isar.writeTxn(() async {
      final globalModel = (await isar.globalModels.get(1))!
        ..currentPlanId = planId
        ..currentBookId = bookId
        ..isPlanFinished = false;

      await isar.globalModels.put(globalModel);
      data(globalModel);
    });
  }

  /// 汇总月度数据
  void cacuAggMonthData() async {
    // 获取全部数据
    var allModels = await isar.progressModels
        .filter()
        .statusEqualTo(ReciteStatus.memorized)
        .or()
        .statusEqualTo(ReciteStatus.forget)
        .findAll();

    // 按日期分组
    Map group = {};
    Set totalDays = {};
    for (var e in allModels) {
      String key = e.updatedAt.toString().substring(0, 7);
      List value = group[key] ?? [];
      value.add(e);
      totalDays.add(DateTime.fromMillisecondsSinceEpoch(e.updatedAt * 1000)
          .toString()
          .substring(0, 10));
      group[key] = value;
    }
    aggMonthData.value = group;
    monthFinished.value = allModels.length;
    workingDays.value = totalDays.length;
  }

  /// 重置本地数据
  Future<void> resetData() async {
    isar.writeTxn(() async {
      await GlobalService.to.isar.bookModels.clear();
    });
  }
}
