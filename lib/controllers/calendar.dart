import 'package:get/get.dart';
import 'package:isar/isar.dart';

import 'package:zzjs_client/api/plan.dart';
import 'package:zzjs_client/schemas/book.dart';

import 'global.dart';

class CalendarController extends GetxController with StateMixin {
  var finishedList = [].obs;
  var taskTracker = [].obs;
  var selectedDayGatha = [].obs;
  var isFinished = false.obs;
  var isMove = false.obs;
  Rx<DateTime> targetDateTime = DateTime.now().obs;
  static CalendarController get to => Get.find();

  @override
  Future<void> onInit() async {
    super.onInit();
    getFinished();
    getGatha(targetDateTime.value);
  }
  getFinished(){
    List<Map<String, String>> gatha = [];
    List<Map<String, String>> progress = [];
    //获取当前计划中所有已背熟列表
    List<GathaModel> gathaData = GlobalService.to.isar.gathaModels
        .filter()
        .progress((q) => q.statusEqualTo(ReciteStatus.memorized))
        .findAllSync();

    for (var item in gathaData) {
      gatha.add({'id':item.id.toString(),'content': item.content, 'name': item.name});
    }
    //获取当前计划中已背熟的进度列表
    List<ProgressModel> progressData = GlobalService.to.isar.progressModels.filter().statusEqualTo(ReciteStatus.memorized).findAllSync();

    for(var i in progressData){
      progress.add({'id':i.gathaId.toString(),'status': i.status.toString(), 'updateAt': DateTime.fromMillisecondsSinceEpoch(i.updatedAt*1000).toString().substring(0,10)});
    }
    //两组列表拼接为一组
    List<Map<String, String>> combine = [
      for (final item1 in progress)
        {
          ...item1,
          ...gatha.firstWhere(((item2) => item1['id'] == item2['id']))
        },
    ];
    finishedList.value = combine;
    taskTracker.value = combine.fold<List<Map<String, dynamic>>>([], (result, currentMap) {
      var updateAtValue = currentMap['updateAt'];
      if (!result.any((map) => map['updateAt'] == updateAtValue)) {
        result.add(currentMap);
      }
      return result;
    });

    update();
  }
  getGatha(DateTime date) {
    targetDateTime.value = date;
    List<Map<String, String>> selectedDay = [];
    for(var i in finishedList){
      if(i['updateAt'] == date.toString().substring(0,10)){
        selectedDay.add(i);
      }
    }
    selectedDay.isEmpty?isFinished.value = false:isFinished.value = true;
    selectedDayGatha.value = selectedDay;
    update();
  }
  // Future<void> getFinishedList(year, month) async {
  //   finishedList = await getMonthProgress(year, month);
  //   update();
  // }

  calendar(List response) async {
    finishedList = response.obs;
    update();
  }

  onChange(DateTime date) async {
    targetDateTime.value = date;
    update();
  }
}
