import 'package:get/get.dart';
import 'package:zzjs_client/entities/recite_plan.dart';

/// 页面状态管理Demo
class DemoController extends GetxController {
  static DemoController get to => Get.find();

  RxInt _count = 0.obs;

  set count(int value) => _count.value = value;
  int get count => _count.value;

  RecitePlanItem get current => onGoingPlan.value;

  /// 当前计划
  final Rx<RecitePlanItem> onGoingPlan = RecitePlanItem.zero().obs;

  @override
  onReady() async {
    super.onReady();
    await Future.delayed(const Duration(seconds: 1));

    onGoingPlan(RecitePlanItem.zero());

    _count = _count + 10;
  }

  increment() {
    _count++;
    onGoingPlan.update((val) {
      val!.total = val.total + 1;
    });
  }
}
