import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_rx/get_rx.dart';
import 'package:path_provider/path_provider.dart';
import 'package:zzjs_client/config.dart';

import '../api/audio.dart';
import '../entities/audio.dart';
import '../entities/audio_channel.dart';
import '../routes/pages.dart';

class AudioListController extends GetxController with StateMixin, GetSingleTickerProviderStateMixin {
  static AudioListController get to => Get.find();

  RxList<AudioChannelModel> titleChannelList = <AudioChannelModel>[].obs;

  RxList<List<AudioModel>> mListDate = <List<AudioModel>>[].obs;
  RxList<Widget> channelList = <Widget>[].obs;

   TabController? tabController;

  @override
  void onInit() {
    super.onInit();
    initData();
  }

  initData() async {

    ///拉取channel列表
    titleChannelList.value = await getAudioChannels();

    tabController = TabController(length: titleChannelList.length, vsync: this)
      ..addListener(() async {
        if (tabController!.indexIsChanging) {
          ///tab变化，拉去对应的列表
          print('object');
          // mListDate.value = await getAudiosForChannelId(titleChannelList[tabController.index].audioChannelId);
        }
      });

    if (titleChannelList.isNotEmpty) {

      ///拉取第一页
      for (int i = 0; i < titleChannelList.length; i++) {
        mListDate.add(await getAudiosForChannelId(titleChannelList[i].audioChannelId)) ;
      }
    }

    Config.audioPath = '${(await getApplicationDocumentsDirectory()).path}/jsaudio';
  }

  getGridItem(int channelIndex, int index) {
    ///添加grid item
    return InkWell(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        children: [
          AspectRatio(
          aspectRatio: 1.0 / 1.0,
            child: Container(
            width: double.infinity,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
                image: DecorationImage(
                image: NetworkImage(mListDate[channelIndex][index].audioThumb),
                  fit: BoxFit.contain,
                ),
              ),
            ),
          ),

          Text(
            mListDate[channelIndex][index].audioName,
            textAlign: TextAlign.center,
            style: const TextStyle(fontSize: 12,),
            maxLines: 2,
          ),
        ],
      ),
      onTap: () {
      Map<String, dynamic> params = {};
      params.putIfAbsent("entity", () => mListDate[channelIndex][index]);
      Get.toNamed(Routes.AUDIO, arguments: params);
          },
    );
  }
}
