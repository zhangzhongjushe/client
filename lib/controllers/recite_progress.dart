import 'package:dio/dio.dart' as dio;
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:isar/isar.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/api/book.dart';
import 'package:zzjs_client/api/plan.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_sliver_list.dart';
import 'package:zzjs_client/entities/plan.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';
import 'package:zzjs_client/schemas/global.dart';

/// 背诵进度状态管理
///
/// 记录当前激活的背诵计划详情，包括背诵进度以及书籍内容
/// 支持离线使用，需同步本地缓存
class ReciteProgressController extends GetxController
    with GetSingleTickerProviderStateMixin, StateMixin, WidgetsBindingObserver {
  static ReciteProgressController get to => Get.find();

  String bookName = "";
  RxList<GathaModel> allGatha = RxList<GathaModel>();
  RxList<GathaModel> forgetGatha = RxList<GathaModel>();
  RxList<GathaModel> memorizedGatha = RxList<GathaModel>();
  RxList<GathaModel> noneGatha = RxList<GathaModel>();

  /// 偈颂列表滚动控制器，初始化为全部偈颂
  ReciteSliverListController get currentController =>
      reciteListControllers[currentStatus]!;
  void setController(ReciteStatus key, ReciteSliverListController e) {
    reciteListControllers.update(
      key,
      (_) => e,
      ifAbsent: () => e,
    );
  }

  /// 全部状态对应列表控制器
  final RxMap<ReciteStatus, ReciteSliverListController> reciteListControllers =
      RxMap();

  /// 当前显示的状态，初始化为全部
  /// [ReciteStatus]枚举索引顺序与展示顺序相反
  ReciteStatus get currentStatus =>
      ReciteStatus.values[3 - ReciteListTabController.to.tabController!.index];

  /// 4种不同状态对应的偈颂列表Map
  RxMap<ReciteStatus, List<GathaModel>> gathaListMap = RxMap();

  // 计算状态初始化时间用
  DateTime start = DateTime.now();

  /// 初始化书籍、当前计划进度
  ///
  /// 尝试获取全部书籍元数据（id、名称、章节）、当前计划进度，
  /// 如果书籍已更新、本地无书籍数据，则更新更新本地书籍
  /// 如果无网络则获取本地缓存内容
  ///
  /// 初始化之前点击开始背偈颂需弹窗等待
  /// TODO: 防抖
  @override
  onReady() async {
    super.onReady();
    // 页面初始化的时候，添加一个状态的监听者
    WidgetsBinding.instance.addObserver(this);
    await resetData();

    // TODO：monitor
    // Get.snackbar('完成初始化', '用时${DateTime.now().difference(start).inSeconds}秒');
    change(null, status: RxStatus.success());
  }

  @override
  onClose() async {
    //2.页面初始化的时候，添加一个状态的监听者
    WidgetsBinding.instance.removeObserver(this);
    super.onClose();
  }

  // 监听程序进入前后台的状态改变的方法
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    switch (state) {
      // 进入应用时候不会触发该状态 应用程序处于可见状态，并且可以响应用户的输入事件。它相当于 Android 中Activity的onResume
      case AppLifecycleState.resumed:
        print("应用进入前台======");
        Posthog().capture(eventName: '应用进入前台');

        // change(null, status: RxStatus.success());
        break;
      // 应用状态处于闲置状态，并且没有用户的输入事件，
      // 注意：这个状态切换到 前后台 会触发，所以流程应该是先冻结窗口，然后停止UI
      case AppLifecycleState.inactive:
        print("应用处于闲置状态，这种状态的应用应该假设他们可能在任何时候暂停 切换到后台会触发======");
        Posthog().capture(eventName: '应用处于闲置状态');
        break;
      // 当前页面即将退出
      case AppLifecycleState.detached:
        print("当前页面即将退出======");
        Posthog().capture(eventName: '当前页面即将退出');
        break;
      // 应用程序处于不可见状态
      case AppLifecycleState.paused:
        print("应用处于不可见状态 后台======");
        Posthog().capture(eventName: '应用处于后台');
        break;
    }
  }

  /// 根据当前计划id重新获取数据
  ///
  /// 已缓存数据不会重新获取
  Future<void> resetData({bool force = false}) async {
    // 从参数中获取上个页面
    String prePage = Get.previousRoute;

    BookModel? currentBook;

    try {
      // 获取当前计划
      PlanItem currentPlan =
          await getPlanDetail(GlobalService.to.currentPlanId.value.toString());

      // 查看当前计划书籍是否已下载且为最新
      currentBook =
          await GlobalService.to.isar.bookModels.get(currentPlan.bookId);

      ///currentPlan.bookId
      // 如果本地没有书籍信息，或书籍过期，则重新下载当前书籍
      if (currentBook == null || force) {
        currentBook = await downloadBook(currentPlan.bookId.toString());
      } else {
        var res = await getBookUpdateTime(currentBook.id.toString());
        var updatedAt = res.data!["update_at"];
        if (currentBook.updatedAt < updatedAt) {
          currentBook = await downloadBook(currentPlan.bookId.toString());
        }
      }
      bookName = currentBook.name;

      // 更新Global表的book ID
      GlobalModel? globalModel =
          await GlobalService.to.isar.globalModels.get(1);
      globalModel?.currentBookId = currentPlan.bookId;
      await GlobalService.to.isar.writeTxn(() async {
        await GlobalService.to.isar.globalModels.put(globalModel!);
      });

      // 如果本地没有数据，则获取进度数据
      // 如果本地已有进度数据，只需在登录后强制重新获取，否则不获取
      if (await GlobalService.to.isar.progressModels.count() == 0 ||
          prePage == Routes.LOGIN) {
        await fetchProgress(currentPlan.id);
      }

      // 此时每个需背诵偈颂都需要有一条进度信息，没有则补充
      // 获取没有进度信息的偈颂列表
      var allGathaId = currentPlan.chapterIds;
      List<GathaModel> gathaWithoutProgress =
          await GlobalService.to.isar.gathaModels
              .filter()
              .chapter((q) {
                var query = q.idEqualTo(allGathaId[0]);
                if (allGathaId.length > 1) {
                  for (var gathaId in allGathaId.sublist(1)) {
                    query = query.or().idEqualTo(gathaId);
                  }
                }
                return query;
              })
              .progressIsNull()
              .findAll();

      // 将所有没有进度信息的偈颂创建进度
      List<ProgressModel> progressModels = <ProgressModel>[];
      for (GathaModel g in gathaWithoutProgress) {
        var model = ProgressModel(
          gathaId: g.id,
          planId: currentPlan.id,
          updatedAt: (DateTime.now().millisecondsSinceEpoch / 1000).ceil(),
        )..gatha.value = g;
        progressModels.add(model);
      }

      await GlobalService.to.isar.writeTxn(() async {
        await GlobalService.to.isar.progressModels.putAll(progressModels);

        for (ProgressModel m in progressModels) {
          await m.gatha.save();
        }
      });

      int dayStart = (DateTime(DateTime.now().year, DateTime.now().month,
                      DateTime.now().day)
                  .millisecondsSinceEpoch /
              1000)
          .floor();
      int dayEnd = (DateTime(DateTime.now().year, DateTime.now().month,
                      DateTime.now().day + 1)
                  .millisecondsSinceEpoch /
              1000)
          .floor();

      int finishedRecitesToday = await GlobalService.to.isar.progressModels
          .filter()
          .updatedAtBetween(dayStart, dayEnd)
          .and()
          .group((q) => q
              .statusEqualTo(ReciteStatus.forget)
              .or()
              .statusEqualTo(ReciteStatus.memorized))
          .count();

      var lastReciteId = (await GlobalService.to.isar.progressModels
              .where()
              .sortByUpdatedAtDesc()
              .findFirst())
          ?.gathaId;

      await GlobalService.to.setGlobalData(
        speed: currentPlan.speed,
        restDays: ((currentPlan.total - currentPlan.finishedCount) /
                currentPlan.speed)
            .ceil(),
        finishedRecitesTotal: currentPlan.finishedCount,
        totalRecites: currentPlan.total,
        finishedRecitesToday: finishedRecitesToday,
        lastReciteId: lastReciteId,
      );

      List<GathaModel> all = await GlobalService.to.isar.gathaModels
          .filter()
          .not()
          .progressIsNull()
          .findAll();
      List<GathaModel> forget = await GlobalService.to.isar.gathaModels
          .filter()
          .not()
          .progressIsNull()
          .progress((q) => q.statusEqualTo(ReciteStatus.forget))
          .findAll();
      List<GathaModel> memorized = await GlobalService.to.isar.gathaModels
          .filter()
          .not()
          .progressIsNull()
          .progress((q) => q.statusEqualTo(ReciteStatus.memorized))
          .findAll();
      List<GathaModel> none = await GlobalService.to.isar.gathaModels
          .filter()
          .not()
          .progressIsNull()
          .progress((q) => q.statusEqualTo(ReciteStatus.none))
          .findAll();

      gathaListMap.addAll({
        ReciteStatus.all: all,
        ReciteStatus.forget: forget,
        ReciteStatus.memorized: memorized,
        ReciteStatus.none: none,
      });

      allGatha.value = all;
      forgetGatha.value = forget;
      memorizedGatha.value = memorized;
      noneGatha.value = none;
    } catch (e, s) {
      Sentry.captureException(e, stackTrace: s);
    }
  }

  /// 下载书籍并存储到isar
  Future<BookModel> downloadBook(String bookId) async {
    // 从服务端读取
    dio.Response<Map<String, dynamic>> bookMap = await getBookDetail(bookId);

    BookModel bookModel = BookModel.fromJson(bookMap.data!);

    // 保存到本地
    bookModel.chapters.addAll(bookModel.chapter);

    await GlobalService.to.isar.writeTxn(() async {
      // 先删除旧的书籍信息
      await GlobalService.to.isar.bookModels.clear();
      await GlobalService.to.isar.chapterModels.clear();
      await GlobalService.to.isar.gathaModels.clear();

      // 存入书籍信息
      await GlobalService.to.isar.bookModels.put(bookModel);

      await GlobalService.to.isar.chapterModels.putAll(bookModel.chapter);
      await bookModel.chapters.save();

      // FIXME：偶现错误：Unhandled Exception: IsarError: Cannot perform this operation from within an active transaction.
      for (ChapterModel chp in bookModel.chapters) {
        await GlobalService.to.isar.gathaModels.putAll(chp.gatha);
        chp.gathas.addAll(chp.gatha);
        await chp.gathas.save();
      }
    });

    return bookModel;
  }

  // ProgressModel _parseRetProgress(int planId, dynamic e) {
  //   e["plan_id"] = planId;
  //   int status = e["status"] as int;
  //   e["status"] = ReciteStatus.values[status].toString().split('.').last;
  //   return ProgressModel.fromJson(e);
  // }

  /// 获取背诵进度并覆盖本地缓存
  Future<void> fetchProgress(int planId) async {
    List<ProgressModel> progressItems =
        await getPlanProgress(planId.toString());

    // 保存记录到数据库
    await GlobalService.to.isar.writeTxn(() async {
      await GlobalService.to.isar.progressModels.clear();
      await GlobalService.to.isar.progressModels.putAll(progressItems);
      for (ProgressModel e in progressItems) {
        GathaModel? gatha =
            await GlobalService.to.isar.gathaModels.get(e.gathaId);
        e.gatha.value = gatha;
        await e.gatha.save();
      }
    });
  }
}
