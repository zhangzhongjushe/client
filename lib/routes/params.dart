class AgreementParams {
  final String title;
  final String content;

  AgreementParams(this.title, this.content);
}
