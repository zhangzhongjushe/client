// ignore_for_file: constant_identifier_names

part of './pages.dart';

abstract class Routes {
  /// 状态管理demo
  static const DEMO = '/demo';

  /// 首页
  static const HOME = '/home';

  /// 登录页
  static const LOGIN = '/login';

  /// 注册页
  static const REGISTER = '/register';

  /// 偈颂列表页
  static const GATHA_LIST = '/gatha/list';

  static const GATHA_DETAIL = '/gatha/detail';

  /// 计划管理页
  static const PLAN = '/plan';

  /// 听闻页
  static const AUDIO = '/audio';

  /// 听闻列表页
  static const AUDIO_LIST = '/audio_list';

  /// 设置页
  static const SETTING = '/setting';

  /// 设置页
  static const NOTIFICATION = '/notification';

  /// 打卡日历
  static const CALENDAR = '/calendar';

  /// 完成每日任务提醒页
  static const DAILY_FINISHED = '/finished';

  /// 关于我们
  static const ABOUT = '/about';

  /// 纯文本页面，如用户协议、隐私政策等
  static const AGREEMENT = '/agreement';

  /// 听闻页
  static const HEARING = '/hearing';
}
