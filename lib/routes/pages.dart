import 'package:get/get.dart';
import 'package:zzjs_client/pages/about.dart';
import 'package:zzjs_client/pages/agreement.dart';
import 'package:zzjs_client/pages/calendar.dart';
import 'package:zzjs_client/pages/daily_finished.dart';
import 'package:zzjs_client/pages/demo.dart';
import 'package:zzjs_client/pages/gatha_list.dart';
import 'package:zzjs_client/pages/hearing_list.dart';
import 'package:zzjs_client/pages/home.dart';
import 'package:zzjs_client/pages/login.dart';
import 'package:zzjs_client/pages/notification.dart';
import 'package:zzjs_client/pages/plan.dart';
import 'package:zzjs_client/pages/register.dart';
import 'package:zzjs_client/pages/setting.dart';

import '../pages/audio.dart';
import '../pages/audio_list.dart';
import '../pages/gatha_detail.dart';

part './routes.dart';

abstract class AppPages {
  static final pages = [
    GetPage(name: Routes.DEMO, page: () => const DemoPage()),
    GetPage(name: Routes.HOME, page: () => const HomePage()),
    GetPage(name: Routes.LOGIN, page: () => const LoginPage()),
    GetPage(name: Routes.GATHA_LIST, page: () => const GathaListPage()),
    GetPage(name: Routes.GATHA_DETAIL, page: () => const GathaDetailPage()),
    GetPage(name: Routes.PLAN, page: () => const PlanPage()),
    GetPage(name: Routes.AUDIO_LIST, page: () =>  AudioListPage()),
    GetPage(name: Routes.AUDIO, page: () =>  AudioDetailPage()),
    GetPage(name: Routes.SETTING, page: () => const SettingPage()),
    GetPage(name: Routes.NOTIFICATION, page: () => const NotificationPage()),
    GetPage(name: Routes.CALENDAR, page: () => const CalendarPage()),
    GetPage(name: Routes.DAILY_FINISHED, page: () => DailyFinishedPage()),
    GetPage(name: Routes.ABOUT, page: () => const AboutPage()),
    GetPage(name: Routes.AGREEMENT, page: () => const AgreementPage()),
    GetPage(name: Routes.REGISTER, page: () => const RegisterPage()),
    GetPage(name: Routes.HEARING, page: () => const HearingListPage()),
  ];
}
