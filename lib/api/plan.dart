import 'package:dio/dio.dart';
import 'package:zzjs_client/api/api.dart';
import 'package:zzjs_client/entities/plan.dart';
import 'package:zzjs_client/entities/recite_plan.dart';
import 'package:zzjs_client/schemas/book.dart';
import 'package:zzjs_client/controllers/calendar.dart';

/// 创建背诵计划
Future<Response<Map>> createPlan(
    int bookId, int speed, List<int> chapters) async {
  Response<Map> resp = await Http().request.post<Map>(
    "/api/plan",
    data: {
      "book_id": bookId,
      "speed": speed,
      "chapters": chapters,
    },
  );

  return resp;
}

/// 获取全部背诵计划
Future<List<RecitePlanItem>> getAllPlan() async {
  Response<Map> resp = await Http().request.get<Map>("/api/plan");

  if (resp.data != null) {
    List<RecitePlanItem> plans = [];
    for (var e in resp.data!["plans"]) {
      var plan = RecitePlanItem.fromMap(e);
      if (plan.total == plan.finishedCount) {
        plan.isFinished = true;
      }
      plans.add(plan);
    }
    return plans;
  }

  return <RecitePlanItem>[];
}

/// 根据id获取背诵计划
Future<PlanItem> getPlanDetail(String id) async {
  Response<Map<String, dynamic>> resp =
      await Http().request.get<Map<String, dynamic>>("/api/plan/$id");

  var plan = PlanItem.fromMap(resp.data!);

  return plan;
}

// /// 根据当前登录用户获取当前计划
// Future<Response<Map>> getUserCurrentPlan() async {
//   Response<Map> resp = await Http().request.get<Map>("/api/plan/login_user");

//   return resp;
// }

/// 删除背诵计划
Future<Response<Map>> deletePlan(String id) async {
  Response<Map> resp = await Http().request.delete<Map>("/api/plan/$id");

  return resp;
}

/// 选择要进行的背诵计划
Future<Response<Map>> changePlan(String id) async {
  Response<Map> resp = await Http().request.put<Map>("/api/plan/active/$id");

  return resp;
}

/// 获取计划的进度详情
Future<List<ProgressModel>> getPlanProgress(String id) async {
  Response<Map<String, dynamic>> resp =
      await Http().request.get<Map<String, dynamic>>("/api/plan/progress/$id");

  var progress = (resp.data!["progress"] as List<dynamic>)
      .map((e) => ProgressModel.fromJson(e as Map<String, dynamic>))
      .toList();

  return progress;
}

/// 修改计划
Future<Response<Map>> updatePlan(String id, int speed) async {
  Response<Map> resp = await Http().request.put<Map>("/api/plan/$id", data: {
    "speed": speed,
  });

  return resp;
}

/// 更新背诵进度
Future<Response<Map>> updateProgress(
    int planId, int gathaId, int status, int updatedAt) async {
  Response<Map> resp =
      await Http().request.put<Map>("/api/plan/progress", data: {
    "plan_id": planId,
    "gatha_id": gathaId,
    "status": status,
    "updated_at": updatedAt,
  });

  return resp;
}

/// 获取月进度汇总
Future getMonthProgress(int year, int month) async {
  var resp = await Http().request.get<List>("/api/plan/result",
      queryParameters: {'year': year, 'month': month});
  CalendarController.to.calendar(resp.data!);
  return resp;
}
