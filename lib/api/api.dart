import 'dart:async';
import 'dart:io';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart' as g;
import 'package:get/get_navigation/get_navigation.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/api/user.dart';
import 'package:zzjs_client/config.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/global.dart';

/// TODO：新版本Dio支持继承，应继承[DioForNative]来实现相关接口
class Http {
  String? accessToken;

  String? refreshToken;

  DateTime? expiresAt;

  /// 网络请求实例
  Dio request = Dio();

  static final Http _instance = Http._internal();

  /// 网络请求全局单例
  factory Http() => _instance;

  // 初始化
  Http._internal() {
    request
      // ..httpClientAdapter = Http2Adapter(ConnectionManager())
      ..options.responseType = ResponseType.json
      ..options.baseUrl = Config.baseUrl
      ..options.headers['Content-Type'] = 'application/json'
      ..options.validateStatus = (status) {
        return status != null && status >= 200 && status < 300;
        //return status! >= 200 && status < 300;
      }
      ..interceptors.add(_getInterceptor())
      ..interceptors.add(LogInterceptor(
        requestBody: true, //显示请求体
        responseBody: true, //显示响应体
      ));
    if (accessToken == null || accessToken == "") {
      // 尝试从数据库内读取缓存数据
      var res = GlobalService.to.isar.globalModels.getSync(1);
      if (res != null &&
          res.expiresAt != null &&
          DateTime.now().isBefore(res.expiresAt!)) {
        accessToken = res.accessToken;
        refreshToken = res.refreshToken;
        expiresAt = res.expiresAt;

        // 登录后插入鉴权header
        request.options.headers['Authorization'] = 'Bearer $accessToken';
      } else if (GlobalService.to.isLogin.value) {
        GlobalService.to.logout();
      }
    }
  }

  /// 设置登录鉴权信息ak、sk
  Future<void> setAuth(
      {String? accessToken, String? refreshToken, int? expires}) async {
    this.accessToken = accessToken ?? this.accessToken;
    this.refreshToken = refreshToken ?? this.refreshToken;
    expiresAt = expires != null
        ? DateTime.now().add(Duration(seconds: expires - 10))
        : expiresAt;
    // 登录后插入鉴权header
    request.options.headers['Authorization'] = 'Bearer $accessToken';

    // 持久化登录信息
    await GlobalService.to.isar.writeTxn(() async {
      var globalSettings = await GlobalService.to.isar.globalModels.get(1);
      if (globalSettings != null) {
        globalSettings.accessToken = accessToken ?? this.accessToken;
        globalSettings.refreshToken = refreshToken ?? this.refreshToken;
        globalSettings.expiresAt = expiresAt;
        await GlobalService.to.isar.globalModels.put(globalSettings);
      }
    });
  }

  // 尝试刷新令牌，更新令牌期间不允许发送请求
  Future<void> _refreshToken() async {
    if (refreshToken == null || refreshToken == "") {
      await GlobalService.to.logout();
    }

    try {
      // 更新token时候，使请求只能顺序进行，阻塞其他请求
      request.interceptors.add(QueuedInterceptorsWrapper());
      // 使用不鉴权中间件替代当前鉴权退出中间件
      request.interceptors.setAll(0, [_getPureInterceptor()]);

      Response<Map> resp = await request.post<Map>("/api/user/refresh",
          options: Options(headers: {'Authorization': 'Bearer $refreshToken'}));

      await setAuth(
        accessToken: resp.data!["access_token"],
        refreshToken: resp.data!["refresh_token"],
        expires: resp.data!["expires"],
      );

      if (resp.data!['user_status'] == 2) {
        await _resetData();
      }
    } catch (e) {
      // 如果refreshToken也失败，则退出登录
      // FIXME: log error
      // await GlobalService.to.logout();
    } finally {
      request.interceptors.setAll(0, [_getInterceptor()]);
      request.interceptors.removeLast();
    }
  }

  Future<Response<Map>> login(String username, String password) async {
    if (request.options.headers['ApiVersion'] == null) {
      g.Get.showSnackbar(g.GetSnackBar(
        messageText: Text(
          "当前版本：${request.options.headers['ApiVer']}",
          style: const TextStyle(color: Colors.black54),
        ),
        borderRadius: 20,
        backgroundColor: Colors.white.withAlpha(100),
        duration: const Duration(seconds: 3),
        snackPosition: SnackPosition.TOP,
      ));
      PackageInfo packageInfo = await PackageInfo.fromPlatform();
      request.options.headers.addAll({"ApiVer": packageInfo.version});
    }
    // 使用不鉴权中间件替代当前鉴权退出中间件
    request.interceptors.setAll(0, [_getPureInterceptor()]);
    Response<Map>? resp;
    try {
      resp = await request.post<Map>(
        "/api/user/login",
        data: {
          "username": username,
          "password": password,
        },
      );

      if (resp.statusCode != HttpStatus.ok) {
        return resp;
      }

      // 后端返回4xx不会执行下面内容
      await setAuth(
        accessToken: resp.data!["access_token"],
        refreshToken: resp.data!["refresh_token"],
        expires: resp.data!["expires"],
      );

      if (resp.data!['user_status'] == 2) {
        await _resetData();
      }
    } catch (e, s) {
      Sentry.captureException(
        e,
        stackTrace: s,
        withScope: (p0) {
          p0.setExtra("response", resp?.data ?? "");
        },
      );
    } finally {
      request.interceptors.setAll(0, [_getInterceptor()]);
    }

    return resp!;
  }

  Future<void> logout() async {
    accessToken == null;
    expiresAt = null;
    refreshToken = null;

    Response<Map>? authRes;
    try {
      // 不使用中间件，不需要处理退出登录时候的错误
      request.interceptors.removeAt(0);
      authRes = await request.post<Map>("/api/user/logout");
    } catch (e, s) {
      Sentry.captureException(
        e,
        stackTrace: s,
        withScope: (p0) {
          p0.setExtra("response", authRes?.data ?? "");
        },
      );
    } finally {
      request.interceptors.insert(0, _getInterceptor());
    }
  }

  /// 异常后尝试重发请求
  Future<Response<T>> _resendRequest<T>(RequestOptions opts) async {
    opts.headers['Authorization'] = 'Bearer $accessToken';
    Response<T> res = Response(requestOptions: opts);

    // 使用不鉴权中间件替代当前鉴权退出中间件
    request.interceptors.setAll(0, [_getPureInterceptor()]);
    try {
      res = await request.fetch(opts);
    } catch (e, s) {
      Sentry.captureException(
        e,
        stackTrace: s,
        withScope: (p0) {
          p0.setExtra("response", res.data ?? "");
        },
      );
    } finally {
      request.interceptors.setAll(0, [_getInterceptor()]);
    }
    return res;
  }

  /// 重置本地数据
  Future<void> _resetData() async {
    await GlobalService.to.resetData();
    await userRest();
  }

  /// 无鉴权处理中间件，如果请求失败则退出登录
  Interceptor _getPureInterceptor() => InterceptorsWrapper(
        onError: (e, handler) async {
          Sentry.captureException(
            e.error,
            stackTrace: e.stackTrace,
            withScope: (p0) {
              p0.setExtra("response", e.response?.data ?? "");
            },
          );
          // g.Get.showSnackbar(g.GetSnackBar(
          //   message:
          //       "请求异常 ${e.requestOptions.method} ${e.requestOptions.uri} response: ${e.response?.statusCode} - ${e.response?.data['error'] ?? 'unknown'}",
          //   duration: const Duration(seconds: 3),
          // ));

          if (g.Get.currentRoute != Routes.LOGIN) {
            await GlobalService.to.logout();
          } else {
            await shouldRetry(e);
            return handler.next(e); //continue
          }
        },
      );

  /// 带鉴权中间件
  Interceptor _getInterceptor() => InterceptorsWrapper(
        onRequest: (options, handler) async {
          // 1. 判断ak令牌是否过期，距过期不足1min则视为过期，尝试更新令牌。refresh失败则退出登录
          if (expiresAt != null && DateTime.now().isAfter(expiresAt!)) {
            await Http()._refreshToken();
            options.headers['Authorization'] = 'Bearer $refreshToken';
          }

          // 2. 更新令牌后继续发送请求
          return handler.next(options); //continue

          // If you want to resolve the request with some custom data，
          // you can resolve a `Response` object eg: return `dio.resolve(response)`.
          // If you want to reject the request with a error message,
          // you can reject a `DioError` object eg: return `dio.reject(dioError)`
        },
        onResponse: (response, handler) {
          // Do something with response data
          return handler.next(response); // continue
          // If you want to reject the request with a error message,
          // you can reject a `DioError` object eg: return `dio.reject(dioError)`
        },
        onError: (DioException e, handler) async {
          // 如果status code是401，尝试refresh token，成功则尝试重发请求，重发失败退出登录，refresh失败则直接跳转至登录页
          // 如果是409，则提示其他设备已登录，跳转至登录页
          switch (e.response?.statusCode) {
            case HttpStatus.unauthorized: // 401
              // 尝试refresh token
              await Http()._refreshToken();

              // 重发请求
              return handler.resolve(await _resendRequest(e.requestOptions));

            case HttpStatus.conflict: // 409
              await GlobalService.to.logout();
              break;

            default:
              Sentry.captureException(
                e.error,
                stackTrace: e.stackTrace,
                withScope: (p0) {
                  p0.setExtra("response", e.response?.data ?? "");
                },
              );

              // 重发请求失败则展示错误弹窗
              // g.Get.showSnackbar(g.GetSnackBar(
              //   message:
              //       "请求异常 ${e.requestOptions.method} ${e.requestOptions.uri} response: ${e.response?.statusCode} - ${e.response?.data['error'] ?? 'unknown'}",
              //   duration: const Duration(seconds: 3),
              // ));
              try {
                await shouldRetry(e);
                // g.Get.showSnackbar(g.GetSnackBar(
                //   message: e.response?.data['error'] ?? 'unknown',
                //   duration: const Duration(seconds: 3),
                // ));
              } catch (_) {}

              // 如果是其他错误，且为Get方法，尝试重发请求
              if (e.requestOptions.method == 'GET') {
                return handler.resolve(await _resendRequest(e.requestOptions));
              }
          }

          return handler.next(e); //continue
          // If you want to resolve the request with some custom data，
          // you can resolve a `Response` object eg: return `dio.resolve(response)`.
        },
      );
  FutureOr<bool> shouldRetry(DioException e) async {
    final List<ConnectivityResult> connectivityResult =
        await (Connectivity().checkConnectivity());
    //TODO:Retry Request
    var hasRetry = e.type != DioExceptionType.cancel &&
        e.type != DioExceptionType.badResponse;
    if (hasRetry) {
      g.Get.showSnackbar(g.GetSnackBar(
        message: connectivityResult.contains(ConnectivityResult.none)
            ? 'Please check your internet connection'.tr
            : 'Please Retry...'.tr,
        duration: const Duration(seconds: 3),
      ));
    } else {
      g.Get.showSnackbar(g.GetSnackBar(
        message: e.response?.data['error'],
        duration: const Duration(seconds: 3),
      ));
    }
    return hasRetry;
  }
}
