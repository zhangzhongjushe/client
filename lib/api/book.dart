import 'package:dio/dio.dart';
import 'package:zzjs_client/api/api.dart';
import 'package:zzjs_client/entities/book.dart';

/// 获取书籍更新时间
Future<Response<Map>> getBookUpdateTime(String id) async {
  Response<Map> resp = await Http().request.get<Map>("/api/book/updated/$id");

  return resp;
}

/// TODO: 未使用接口 获取书籍列表及元数据
Future<Response<Map>> getBooksMeta() async {
  Response<Map> resp = await Http().request.get<Map>("/api/book/list");

  return resp;
}

/// 获取书籍详情
Future<Response<Map<String, dynamic>>> getBookDetail(String id) async {
  Response<Map<String, dynamic>> resp =
      await Http().request.get<Map<String, dynamic>>("/api/book/$id");

  return resp;
}

//1、新增搜索查询接口
// API:/api/book/search
// METHOD:GET
// REQUEST:
//    bookId  查询书籍
//    key       查询关键字，模糊匹配
//    pageNum   分页控制-当前页，可缺省，后台默认1
//    pageCount 分页控制-每页条数，可缺省，后台默认10
// RETURN SUCCESS:
//    object [] 列表
//    object:
//       "book_id": int 稽子所属书本ID
//       "chapter_id": int 稽子所属章节ID
//       "content": string 匹配查询内容
//       "order": int 稽子在书中序号
// RETURN ERROR:
//     看返回错误消息
/// 搜索内容
Future<List<BookItem>> search(
    String bookId, String key, int pageNum, int pageCount) async {
  final Map<String, dynamic> queryParams = {
    'book_id': bookId,
    'key': key,
    'pageNum': pageNum,
    'pageCount': pageCount
  };

  Response resp = await Http().request.get(
        "/api/book/search",
        queryParameters: queryParams,
      );
  List<BookItem> items = [];
  for (var item in resp.data) {
    if (item is Map) {
      items.add(BookItem(
          id: item['id'],
          chapterId: item['chapter_id'],
          bookId: item['book_id'],
          order: item['order'],
          name: item['name'],
          content: item['content'],
          explanation: item['explanation']));
    }
  }
  return items;
}
