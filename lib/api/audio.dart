


import 'package:dio/dio.dart';

import '../entities/audio.dart';
import '../entities/audio_channel.dart';
import 'api.dart';

/// 获取音频channel
Future<List<AudioChannelModel>> getAudioChannels() async {
  Response<List> resp = await Http().request.get<List>("/api/audio/channel");

  if (resp.data != null) {
    List<AudioChannelModel> audioChannels = [];
    for (var e in resp.data!) {
      var audioChannel = AudioChannelModel.fromMap(e);
      audioChannels.add(audioChannel);
    }
    return audioChannels;
  }

  return <AudioChannelModel>[];
}

/// 获取音频 for id
Future<List<AudioModel>> getAudiosForChannelId(int id) async {
  Response<List> resp = await Http().request.get<List>("/api/audio/channel/$id");

  if (resp.data != null) {
    List<AudioModel> audios = [];
    for (var e in resp.data!) {
      var audio = AudioModel.fromMap(e);
      audios.add(audio);
    }
    return audios;
  }

  return <AudioModel>[];
}