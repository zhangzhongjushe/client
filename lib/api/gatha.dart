import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart';
import 'package:zzjs_client/entities/gatha.dart';
import 'package:zzjs_client/schemas/book.dart';

import 'api.dart';

// 偈颂音频及其他信息，用户需登录 偈颂扩展内容(查一条记录)
Future<GathaDetails?> getGathaDetails(int gathaId) async {
  Response resp = await Http().request.get<Map>("/api/gatha/$gathaId");
  if (resp.statusCode == 200) {
    return GathaDetails(
        id: resp.data['id'],
        gathaId: resp.data['gatha_id'],
        chapterId: resp.data['chapter_id'],
        bookId: resp.data['book_id'],
        gathaVersion: resp.data['gatha_version'],
        audioUrl: resp.data['audio_url'],
        chineseMemo: resp.data['chinese_memo'],
        englishMemo: resp.data['english_memo'],
        graphUrl: resp.data['graph_url']);
  }
  return null;
}

Future<String?> getAudio(GathaModel gatha, int gathaVersion, String url) async {
  String filename = path.basename(url);
  final appDir = await getApplicationDocumentsDirectory();
  final filePath = '${appDir.path}/$filename';
  final file = File(filePath);

  if (gatha.audioUrl != null && gatha.gathaVersion == gathaVersion) {
    if (kDebugMode) {
      print('MP3 file already exists at: $filePath');
    }
    return gatha.audioUrl!;
  }

  final resp = await Http().request.download(url, filePath);
  if (resp.statusCode == 200) {
    return filePath;
  }
  return null;
}
