import 'package:dio/dio.dart';
import 'package:zzjs_client/api/api.dart';
import 'package:zzjs_client/controllers/global.dart';

/// 登录
Future<Response<Map>> loginByPassword(String username, String password) async {
  var a = await Http().login(username, password);
  print('loginByPassword":$a');
  return a;
}

/// 获取验证码
Future<Response<Map>> getVerifyCode(String username) async {
  var a = await Http().request.post<Map>("/api/user/verifyCode", data: {
    "username": username,
  });
  print('getVerifyCode":$a');
  return a;
}

/// 注册
Future<Response<Map>> registerByPassword(
    String username, String code, String password) async {
  return await Http().request.post<Map>("/api/user/register",
      data: {"username": username, "verifyCode": code, "password": password});
}

/// 获取用户信息
Future<Response<Map>> getUserInfo() async {
  Response<Map> resp = await Http().request.get<Map>("/api/user/profile");

  GlobalService.to.login(resp.data!['user_id'], resp.data!['username'],
      resp.data!['current_plan_id']);

  return resp;
}

/// 退出登录
Future<void> userLogout() async {
  await Http().logout();
}

/// 恢复用户状态
Future<Response<Map>> userRest() async {
  return await Http().request.post<Map>("/api/user/reset");
}
