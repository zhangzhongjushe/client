import 'package:dio/dio.dart';
import 'package:zzjs_client/entities/news.dart';

import 'api.dart';

// 官方贴列表
Future<List<NewsItem>> getNewsList(int pageNum, int pageCount,
    {int? typeId, int? year, int? month, int? day}) async {
  typeId ??= 0;

  final Map<String, dynamic> queryParams = {
    'typeId': typeId,
    'pageNum': 1,
    'pageCount': 10
  };
  if (year != null) {
    queryParams['year'] = year;
  }
  if (month != null) {
    queryParams['month'] = month;
  }

  if (day != null) {
    queryParams['day'] = day;
  }
  Response resp = await Http()
      .request
      .get<List<dynamic>>("/api/news/list", queryParameters: queryParams);

  List<NewsItem> items = [];

  for (var item in resp.data) {
    if (item is Map) {
      items.add(
        NewsItem(
            id: item['id'],
            typeId: item['type_id'],
            date: DateTime.parse(item['date']).millisecondsSinceEpoch,
            status: item['status'],
            typeName: item['type_name'],
            title: item['title'],
            thumb: item['thumb'],
            order: item['order']),
      );
    }
  }
  return items;
}

// 官方贴明细
Future<Response<Map>> getNewsDetails(int articleId) async {
  Response<Map> resp = await Http().request.get<Map>("/api/news/$articleId");
  return resp;
}
