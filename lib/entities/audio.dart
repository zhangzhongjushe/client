import 'dart:convert';
import 'package:collection/collection.dart';

import 'package:json_annotation/json_annotation.dart';

 import 'package:isar/isar.dart';

part 'audio.g.dart';

/// 音频实体类
@JsonSerializable()
@collection
class AudioModel {
  Id id = Isar.autoIncrement;

  /// 听闻栏目ID
  int audioChannelId;

  ///  听闻 ID
  int audioId;

  ///  听闻标题
  String audioName;

  ///  听闻缩略图（图片URL）
  String audioThumb;

  ///  听闻详情页文字
  String audioText;

  ///  听闻内容（音频URL）
  String audioUrl;

  ///  听闻音频版本号
  int audioVersion;

  AudioModel({
    required this.audioChannelId,
    required this.audioId,
    required this.audioName,
    required this.audioThumb,
    required this.audioText,
    required this.audioUrl,
    required this.audioVersion,
  });

  factory AudioModel.fromMap(Map<String, dynamic> date) => AudioModel(
        audioChannelId: date['audio_channel_id'] as int,
        audioId: date['audio_id'] as int,
        audioName: date['audio_name'] as String,
        audioThumb: date['audio_thumb'] as String,
        audioText: date['audio_text'] as String,
        audioUrl: date['audio_url'] as String,
        audioVersion: date['audio_version'] as int,
      );

  Map<String, dynamic> toMap() => {
    'audio_channel_id':audioChannelId,
    'audio_id':audioId,
    'audio_name':audioName,
    'audio_thumb':audioThumb,
    'audio_text':audioText,
    'audio_url':audioUrl,
    'audio_version':audioVersion,
  };

  factory AudioModel.fromJson(String date){
    return AudioModel.fromMap(json.decode(date) as Map<String, dynamic>);
  }

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'AudioModel(audioChannelId: $audioChannelId, audioId: $audioId, '
        'audioName: $audioName, audioThumb: $audioThumb, audioText: $audioText,'
        ' audioUrl: $audioUrl, audioVersion: $audioVersion)';

  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    if (other is! AudioModel) return false;
    final mapEquals = const DeepCollectionEquality().equals;
    return mapEquals(other.toMap(), toMap());
  }

  @override
  int get hashCode {
    return  audioChannelId.hashCode ^
    audioId.hashCode ^
    audioName.hashCode ^
    audioThumb.hashCode ^
    audioText.hashCode ^
    audioUrl.hashCode ^
    audioVersion.hashCode ;
  }
}
