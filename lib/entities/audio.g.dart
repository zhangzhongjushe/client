// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'audio.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetAudioModelCollection on Isar {
  IsarCollection<AudioModel> get audioModels => this.collection();
}

const AudioModelSchema = CollectionSchema(
  name: r'AudioModel',
  id: 579416068458953487,
  properties: {
    r'audioChannelId': PropertySchema(
      id: 0,
      name: r'audioChannelId',
      type: IsarType.long,
    ),
    r'audioId': PropertySchema(
      id: 1,
      name: r'audioId',
      type: IsarType.long,
    ),
    r'audioName': PropertySchema(
      id: 2,
      name: r'audioName',
      type: IsarType.string,
    ),
    r'audioText': PropertySchema(
      id: 3,
      name: r'audioText',
      type: IsarType.string,
    ),
    r'audioThumb': PropertySchema(
      id: 4,
      name: r'audioThumb',
      type: IsarType.string,
    ),
    r'audioUrl': PropertySchema(
      id: 5,
      name: r'audioUrl',
      type: IsarType.string,
    ),
    r'audioVersion': PropertySchema(
      id: 6,
      name: r'audioVersion',
      type: IsarType.long,
    ),
    r'hashCode': PropertySchema(
      id: 7,
      name: r'hashCode',
      type: IsarType.long,
    )
  },
  estimateSize: _audioModelEstimateSize,
  serialize: _audioModelSerialize,
  deserialize: _audioModelDeserialize,
  deserializeProp: _audioModelDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _audioModelGetId,
  getLinks: _audioModelGetLinks,
  attach: _audioModelAttach,
  version: '3.1.0+1',
);

int _audioModelEstimateSize(
  AudioModel object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  bytesCount += 3 + object.audioName.length * 3;
  bytesCount += 3 + object.audioText.length * 3;
  bytesCount += 3 + object.audioThumb.length * 3;
  bytesCount += 3 + object.audioUrl.length * 3;
  return bytesCount;
}

void _audioModelSerialize(
  AudioModel object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeLong(offsets[0], object.audioChannelId);
  writer.writeLong(offsets[1], object.audioId);
  writer.writeString(offsets[2], object.audioName);
  writer.writeString(offsets[3], object.audioText);
  writer.writeString(offsets[4], object.audioThumb);
  writer.writeString(offsets[5], object.audioUrl);
  writer.writeLong(offsets[6], object.audioVersion);
  writer.writeLong(offsets[7], object.hashCode);
}

AudioModel _audioModelDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = AudioModel(
    audioChannelId: reader.readLong(offsets[0]),
    audioId: reader.readLong(offsets[1]),
    audioName: reader.readString(offsets[2]),
    audioText: reader.readString(offsets[3]),
    audioThumb: reader.readString(offsets[4]),
    audioUrl: reader.readString(offsets[5]),
    audioVersion: reader.readLong(offsets[6]),
  );
  object.id = id;
  return object;
}

P _audioModelDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readLong(offset)) as P;
    case 1:
      return (reader.readLong(offset)) as P;
    case 2:
      return (reader.readString(offset)) as P;
    case 3:
      return (reader.readString(offset)) as P;
    case 4:
      return (reader.readString(offset)) as P;
    case 5:
      return (reader.readString(offset)) as P;
    case 6:
      return (reader.readLong(offset)) as P;
    case 7:
      return (reader.readLong(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _audioModelGetId(AudioModel object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _audioModelGetLinks(AudioModel object) {
  return [];
}

void _audioModelAttach(IsarCollection<dynamic> col, Id id, AudioModel object) {
  object.id = id;
}

extension AudioModelQueryWhereSort
    on QueryBuilder<AudioModel, AudioModel, QWhere> {
  QueryBuilder<AudioModel, AudioModel, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension AudioModelQueryWhere
    on QueryBuilder<AudioModel, AudioModel, QWhereClause> {
  QueryBuilder<AudioModel, AudioModel, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterWhereClause> idNotEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AudioModelQueryFilter
    on QueryBuilder<AudioModel, AudioModel, QFilterCondition> {
  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioChannelIdEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioChannelId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioChannelIdGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioChannelId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioChannelIdLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioChannelId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioChannelIdBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioChannelId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioIdEqualTo(
      int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioIdGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioIdLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioId',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioIdBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioNameGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioName',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioNameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'audioName',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioNameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'audioName',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioNameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioName',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioNameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'audioName',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioTextGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioText',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioTextStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'audioText',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioTextMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'audioText',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioTextIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioText',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioTextIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'audioText',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioThumbEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioThumbBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioThumb',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'audioThumb',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioThumbMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'audioThumb',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioThumb',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioThumbIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'audioThumb',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioUrlGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioUrl',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioUrlStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'audioUrl',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> audioUrlMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'audioUrl',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioUrlIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioUrlIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'audioUrl',
        value: '',
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioVersionEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'audioVersion',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioVersionGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'audioVersion',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioVersionLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'audioVersion',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      audioVersionBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'audioVersion',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> hashCodeEqualTo(
      int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'hashCode',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition>
      hashCodeGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'hashCode',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> hashCodeLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'hashCode',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> hashCodeBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'hashCode',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension AudioModelQueryObject
    on QueryBuilder<AudioModel, AudioModel, QFilterCondition> {}

extension AudioModelQueryLinks
    on QueryBuilder<AudioModel, AudioModel, QFilterCondition> {}

extension AudioModelQuerySortBy
    on QueryBuilder<AudioModel, AudioModel, QSortBy> {
  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioChannelId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioChannelId', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy>
      sortByAudioChannelIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioChannelId', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioId', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioId', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioName', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioName', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioText', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioText', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioThumb() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioThumb', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioThumbDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioThumb', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioUrl', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioUrl', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioVersion() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioVersion', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByAudioVersionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioVersion', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByHashCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hashCode', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> sortByHashCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hashCode', Sort.desc);
    });
  }
}

extension AudioModelQuerySortThenBy
    on QueryBuilder<AudioModel, AudioModel, QSortThenBy> {
  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioChannelId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioChannelId', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy>
      thenByAudioChannelIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioChannelId', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioId', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioId', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioName() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioName', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioNameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioName', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioText() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioText', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioTextDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioText', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioThumb() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioThumb', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioThumbDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioThumb', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioUrl() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioUrl', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioUrlDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioUrl', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioVersion() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioVersion', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByAudioVersionDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'audioVersion', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByHashCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hashCode', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByHashCodeDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'hashCode', Sort.desc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }
}

extension AudioModelQueryWhereDistinct
    on QueryBuilder<AudioModel, AudioModel, QDistinct> {
  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioChannelId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioChannelId');
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioId');
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioName(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioName', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioText(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioText', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioThumb(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioThumb', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioUrl(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioUrl', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByAudioVersion() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'audioVersion');
    });
  }

  QueryBuilder<AudioModel, AudioModel, QDistinct> distinctByHashCode() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'hashCode');
    });
  }
}

extension AudioModelQueryProperty
    on QueryBuilder<AudioModel, AudioModel, QQueryProperty> {
  QueryBuilder<AudioModel, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<AudioModel, int, QQueryOperations> audioChannelIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioChannelId');
    });
  }

  QueryBuilder<AudioModel, int, QQueryOperations> audioIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioId');
    });
  }

  QueryBuilder<AudioModel, String, QQueryOperations> audioNameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioName');
    });
  }

  QueryBuilder<AudioModel, String, QQueryOperations> audioTextProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioText');
    });
  }

  QueryBuilder<AudioModel, String, QQueryOperations> audioThumbProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioThumb');
    });
  }

  QueryBuilder<AudioModel, String, QQueryOperations> audioUrlProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioUrl');
    });
  }

  QueryBuilder<AudioModel, int, QQueryOperations> audioVersionProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'audioVersion');
    });
  }

  QueryBuilder<AudioModel, int, QQueryOperations> hashCodeProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'hashCode');
    });
  }
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AudioModel _$AudioModelFromJson(Map<String, dynamic> json) => AudioModel(
      audioChannelId: (json['audioChannelId'] as num).toInt(),
      audioId: (json['audioId'] as num).toInt(),
      audioName: json['audioName'] as String,
      audioThumb: json['audioThumb'] as String,
      audioText: json['audioText'] as String,
      audioUrl: json['audioUrl'] as String,
      audioVersion: (json['audioVersion'] as num).toInt(),
    )..id = (json['id'] as num).toInt();

Map<String, dynamic> _$AudioModelToJson(AudioModel instance) =>
    <String, dynamic>{
      'id': instance.id,
      'audioChannelId': instance.audioChannelId,
      'audioId': instance.audioId,
      'audioName': instance.audioName,
      'audioThumb': instance.audioThumb,
      'audioText': instance.audioText,
      'audioUrl': instance.audioUrl,
      'audioVersion': instance.audioVersion,
    };
