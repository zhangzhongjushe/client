class BookItem {
  int id;
  int chapterId;
  int bookId;
  int order;
  String name;
  String content;
  String explanation;

  BookItem(
      {required this.id,
      required this.chapterId,
      required this.bookId,
      required this.order,
      required this.name,
      required this.content,
      required this.explanation});

  factory BookItem.fromJson(Map<dynamic, dynamic> json) {
    return BookItem(
        id: json['id'],
        bookId: json['book_id'],
        chapterId: json['chapter_id'],
        order: json['order'],
        name: json['name'],
        content: json['content'],
        explanation: json['explanation']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'book_id': bookId,
      'chapter_id': chapterId,
      'order': order,
      'name': name,
      'content': content,
      'explanation': explanation
    };
  }

  @override
  String toString() {
    return 'BookItem{id: $id, name: $name, age: $content}';
  }
}
