import 'dart:convert';
import 'package:collection/collection.dart';

/**
 * 音频栏目实体类
 */

class AudioChannelModel {
  /**
   *  audio_channel_id integer       音频栏目ID
   */
  int audioChannelId;

  /**
   *  audio_channel_name string      音频栏目名称
   */
  String audioChannelName;

  AudioChannelModel({
    required this.audioChannelId,
    required this.audioChannelName,
  });

  factory AudioChannelModel.fromMap(Map<String, dynamic> data) =>
      AudioChannelModel(
        audioChannelId: data['audio_channel_id'] as int,
        audioChannelName: data['audio_channel_name'] as String,
      );

  Map<String, dynamic> toMap() => {
        'audio_channel_id': audioChannelId,
        'audio_channel_name': audioChannelName,
      };

  factory AudioChannelModel.fromJson(String data) =>
      AudioChannelModel.fromMap(json.decode(data) as Map<String, dynamic>);

  String toJson() => json.encode(toMap());

  @override
  String toString() {
    return 'AudioChannelModel(audioChannelId: $audioChannelId, audioChannelName: $audioChannelName)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    if (other is! AudioChannelModel) return false;
    final mapEquals = const DeepCollectionEquality().equals;
    return mapEquals(other.toMap(), toMap());
  }

  @override
  int get hashCode {
    return  audioChannelId.hashCode ^
    audioChannelName.hashCode ;
  }
}
