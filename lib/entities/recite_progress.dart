import 'package:zzjs_client/schemas/book.dart';

/// 当前背诵进度
class ReciteProgress {
  int bookId;
  int planId;

  List<ProgressModel> currentPS;

  ReciteProgress({
    required this.bookId,
    required this.planId,
    required this.currentPS,
  });

  /// 获取当前卡片背诵状态
  ReciteStatus getProgressStatus(String gathaId) {
    for (ProgressModel pg in currentPS) {
      if (pg.gathaId.toString() == gathaId) return pg.status;
    }

    return ReciteStatus.none;
  }

  ProgressModel? getProgressById(int reciteId) {
    for (ProgressModel pg in currentPS) {
      if (pg.id == reciteId) return pg;
    }

    return null;
  }

  ProgressModel? getProgressByGathaId(int gathaId) {
    for (ProgressModel pg in currentPS) {
      if (pg.gathaId == gathaId) return pg;
    }

    return null;
  }

  ProgressModel setProgress(
      int reciteId, int contentId, ReciteStatus st, int updateAt) {
    ProgressModel? pg = getProgressByGathaId(contentId);
    if (pg == null) {
      pg = ProgressModel(
          id: reciteId,
          planId: planId,
          updatedAt: updateAt,
          gathaId: contentId,
          status: st);
      currentPS.add(pg);
    } else {
      pg.status = st;
      pg.updatedAt = updateAt;
    }
    return pg;
  }

  int getTotalFinished() {
    int r = 0;
    for (var element in currentPS) {
      if (element.status == ReciteStatus.memorized) r++;
    }
    return r;
  }

  int _getTodayStartMS() {
    DateTime dt = DateTime.now();
    Duration dur = Duration(
        hours: dt.hour,
        minutes: dt.minute,
        seconds: dt.second,
        milliseconds: dt.millisecond);
    return dt.subtract(dur).millisecondsSinceEpoch;
  }

  int getTodayFinished() {
    int ms = _getTodayStartMS();
    int r = 0;
    for (var element in currentPS) {
      if (element.status == ReciteStatus.memorized && element.updatedAt > ms) {
        r++;
      }
    }
    return r;
  }
}
