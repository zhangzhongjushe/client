class GathaDetails {
  //{
  //     "id": 1054,
  //     "gatha_id": 1825,
  //     "chapter_id": 2,
  //     "book_id": 1,
  //     "audio_url": "http://zzjs-dev.bj-kyhx.com:1081/audio/C2_GEN_PIN_15.mp3",
  //     "chinese_memo": "",
  //     "english_memo": "",
  //     "graph_url": "http://zzjs-dev.bj-kyhx.com:1081/image/gatha-graph-1-2-15.jpg",
  //     "gatha_version": 1
  // }
  int id;

  int gathaId;

  int chapterId;

  int bookId;

  int gathaVersion;

  String audioUrl;

  String chineseMemo;

  String englishMemo;

  String graphUrl;

  GathaDetails(
      {required this.id,
      required this.gathaId,
      required this.chapterId,
      required this.bookId,
      required this.gathaVersion,
      required this.audioUrl,
      required this.chineseMemo,
      required this.englishMemo,
      required this.graphUrl});
  @override
  String toString() {
    return "GathaDetails{id: $id, gathaId: $gathaId, chapterId: $chapterId, bookId: $bookId, gathaVersion: $gathaVersion, audioUrl: $audioUrl, chineseMemo: $chineseMemo, englishMemo: $englishMemo, graphUrl: $graphUrl";
  }
}
