import 'dart:convert';

// ignore: depend_on_referenced_packages
import 'package:collection/collection.dart';

class PlanItem {
  int id;
  int bookId;
  bool isCurrent;
  int speed;
  int total;
  int finishedCount;
  int lastContentId;
  List<int> chapterIds;

  PlanItem({
    required this.id,
    required this.bookId,
    required this.isCurrent,
    required this.speed,
    required this.total,
    required this.finishedCount,
    required this.lastContentId,
    required this.chapterIds,
  });

  @override
  String toString() {
    return 'Plan(id: $id, bookId: $bookId, isCurrent: $isCurrent, speed: $speed, total: $total, finishedCount: $finishedCount, lastContentId: $lastContentId, chapterIds: $chapterIds)';
  }

  factory PlanItem.fromMap(Map<String, dynamic> data) => PlanItem(
        id: data['id'] as int,
        bookId: data['book_id'] as int,
        isCurrent: data['is_current'] as bool,
        speed: data['speed'] as int,
        total: data['total'] as int,
        finishedCount: data['finished_count'] as int,
        lastContentId: data['last_content_id'] as int,
        chapterIds: transList(data['chapter_ids']),
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'book_id': bookId,
        'is_current': isCurrent,
        'speed': speed,
        'total': total,
        'finished_count': finishedCount,
        'last_content_id': lastContentId,
        'chapter_ids': chapterIds,
      };

  /// `dart:convert`
  ///
  /// Parses the string and returns the resulting Json object as [PlanItem].
  factory PlanItem.fromJson(String data) {
    return PlanItem.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  /// `dart:convert`
  ///
  /// Converts [PlanItem] to a JSON string.
  String toJson() => json.encode(toMap());

  static List<int> transList(List<dynamic> array) {
    //vs.map((e) => e as int).toList();
    List<int> ret = [];
    for (var element in array) {
      ret.add(element as int);
    }
    return ret;
  }

  PlanItem copyWith({
    int? id,
    int? bookId,
    bool? isCurrent,
    int? speed,
    int? total,
    int? finishedCount,
    int? lastContentId,
    List<int>? chapterIds,
  }) {
    return PlanItem(
      id: id ?? this.id,
      bookId: bookId ?? this.bookId,
      isCurrent: isCurrent ?? this.isCurrent,
      speed: speed ?? this.speed,
      total: total ?? this.total,
      finishedCount: finishedCount ?? this.finishedCount,
      lastContentId: lastContentId ?? this.lastContentId,
      chapterIds: chapterIds ?? this.chapterIds,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    if (other is! PlanItem) return false;
    final mapEquals = const DeepCollectionEquality().equals;
    return mapEquals(other.toMap(), toMap());
  }

  @override
  int get hashCode =>
      id.hashCode ^
      bookId.hashCode ^
      isCurrent.hashCode ^
      speed.hashCode ^
      total.hashCode ^
      finishedCount.hashCode ^
      lastContentId.hashCode ^
      chapterIds.hashCode;
}
