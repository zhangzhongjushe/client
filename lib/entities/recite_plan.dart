import 'dart:convert';

import 'package:get/get.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/schemas/book.dart';

class RecitePlanItem {
  int id;
  int bookId;
  bool isCurrent;
  bool isFinished;
  List<int> chapterIds;
  int finishedCount;
  int total;
  int speed;
  String bookName;
  String bookImage;
  String planName;
  int createdAt;
  int lastFinishedAt;

  /// 上次背诵位置,上次卡片id
  final int lastContentId;

  RecitePlanItem({
    required this.id,
    required this.bookId,
    required this.isCurrent,
    required this.isFinished,
    required this.chapterIds,
    required this.finishedCount,
    required this.total,
    required this.speed,
    required this.lastContentId,
    required this.bookName,
    required this.bookImage,
    required this.planName,
    required this.createdAt,
    required this.lastFinishedAt,
  });

  static List<int> transList(List<dynamic> vs) {
    //vs.map((e) => e as int).toList();
    List<int> ret = [];
    for (var element in vs) {
      ret.add(element as int);
    }
    return ret;
  }

  factory RecitePlanItem.zero() => RecitePlanItem(
        id: 0,
        bookId: 0,
        isCurrent: false,
        chapterIds: [0],
        finishedCount: 0,
        total: 0,
        speed: 0,
        lastContentId: 0,
        isFinished: true,
        bookImage: "",
        bookName: "",
        planName: "",
        createdAt: 0,
        lastFinishedAt: 0,
      );

  factory RecitePlanItem.fromMap(Map<dynamic, dynamic> data) {
    BookModel? book =
        GlobalService.to.isar.bookModels.getSync(data['book_id'] as int);
    if (book == null) {
      // book未找到则报错
      String errMsg = "未找到当前书籍，请重试";
      Get.showSnackbar(GetSnackBar(
        message: errMsg.tr,
        duration: const Duration(seconds: 3),
      ));
      Sentry.captureMessage(errMsg);
      return RecitePlanItem.zero();
    }

    String planName = "";

    List<ChapterModel?> chapters = GlobalService.to.isar.chapterModels
        .getAllSync(transList(data['chapter_ids']));
    for (var e in chapters) {
      planName += e!.name;
      planName += "、";
    }
    planName = planName.substring(0, planName.length - 1);

    return RecitePlanItem(
      id: data['id'] as int,
      bookId: data['book_id'] as int,
      chapterIds: transList(data['chapter_ids']),
      finishedCount: data['finished_count'] as int,
      total: data['total'] as int,
      speed: data['speed'] as int,
      isCurrent: data["is_current"],
      isFinished: data['finished_count'] as int == data['total'] as int,
      lastContentId: data['last_content_id'] as int,
      bookName: book.name,
      bookImage: "${book.name}\nPlanId: ${data['id'] as int}",
      planName: planName,
      createdAt: data['created_at'] as int,
      lastFinishedAt: data['last_finished_at'] as int,
    );
  }

  Map<String, dynamic> toMap() => {
        'id': id,
        'book_id': bookId,
        'chapter_ids': chapterIds,
        'finished_count': finishedCount,
        'total': total,
        'speed': speed,
        'last_content_id': lastContentId,
        'created_at': createdAt,
        'last_finished_at': lastFinishedAt,
      };

  /// `dart:convert`
  ///
  /// Parses the string and returns the resulting Json object as [RecitePlanItem].
  factory RecitePlanItem.fromJson(String data) {
    return RecitePlanItem.fromMap(json.decode(data) as Map<String, dynamic>);
  }

  /// `dart:convert`
  ///
  /// Converts [RecitePlanItem] to a JSON string.
  String toJson() => json.encode(toMap());

  bool get stringify => true;

  List<Object?> get props =>
      [id, bookId, chapterIds, finishedCount, total, speed, lastContentId];
}
