class NewsItem {
  int id;
  // 帖子所属栏目Id
  int typeId;

  // 发布时间
  int date;

  // 状态 0：下架；1：已发布
  int status;

  // 所属栏目名称，用于前端展示
  String typeName;

  // 标题
  String title;

  // 缩略图
  String thumb;

  //  官方帖内部排序，后端按此推送，前端按此顺序展示
  int order;

  NewsItem(
      {required this.id,
      required this.typeId,
      required this.date,
      required this.status,
      required this.typeName,
      required this.title,
      required this.thumb,
      required this.order});
  @override
  String toString() {
    return "NewsItem{id: $id, typeId: $typeId, date: ${DateTime.fromMillisecondsSinceEpoch(date)}, status: $status, typeName: $typeName, title: $title, thumb: $thumb, order: $order";
  }
}
