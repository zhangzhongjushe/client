import 'package:isar/isar.dart';
import 'package:json_annotation/json_annotation.dart';

part 'book.g.dart';

/// 卡片背诵状态
@JsonEnum(valueField: "status")
enum ReciteStatus {
  /// 未背诵
  none(0),

  /// 不熟
  forget(1),

  /// 已背熟
  memorized(2),

  /// 全部
  all(3);

  const ReciteStatus(this.status);

  final short status;
}

const reciteStatusTitleMap = {
  ReciteStatus.all: '全部',
  ReciteStatus.memorized: '已背熟',
  ReciteStatus.forget: '不熟',
  ReciteStatus.none: '未背',
};

@JsonSerializable()
@collection
class BookModel {
  Id id = Isar.autoIncrement;
  late String name;

  /// 从1970-01-01到当前的毫秒数
  @JsonKey(name: "updated_at")
  late int updatedAt;

  /// json解析内容
  @ignore
  @JsonKey(name: "chapters")
  List<ChapterModel> chapter;

  // bool isDownload = false;

  /// Isar数据库关联关系
  @Backlink(to: "book")
  final chapters = IsarLinks<ChapterModel>();

  // TODO：错误提示不友好，如果JSON缺少相关字段，添加assert提示
  BookModel({
    required this.name,
    required this.id,
    required this.updatedAt,
    List<ChapterModel>? chapter,
  }) : chapter = chapter ?? <ChapterModel>[];

  factory BookModel.fromJson(Map<String, dynamic> json) =>
      _$BookModelFromJson(json);

  Map<String, dynamic> toJson() => _$BookModelToJson(this);
}

@JsonSerializable()
@collection
class ChapterModel {
  Id id = Isar.autoIncrement;
  late String name;
  late int length;

  @ignore
  @JsonKey(name: "gathas")
  List<GathaModel> gatha;

  @Backlink(to: 'chapter')
  final gathas = IsarLinks<GathaModel>();
  final book = IsarLink<BookModel>();

  ChapterModel({
    required this.id,
    required this.name,
    required this.length,
    List<GathaModel>? gatha,
  }) : gatha = gatha ?? <GathaModel>[];

  factory ChapterModel.fromJson(Map<String, dynamic> json) =>
      _$ChapterModelFromJson(json);

  Map<String, dynamic> toJson() => _$ChapterModelToJson(this);
}

@JsonSerializable()
@collection
class GathaModel {
  late Id id;
  late String name;
  late String content;
  late String explanation;

  String? audioUrl;
  String? chineseMemo;
  String? englishMemo;
  String? graphUrl;
  int? gathaVersion;

  @Index()
  short order;

  // 后续添加上一个、下一个偈颂关联
  @ignore
  GathaModel? prev;
  @ignore
  GathaModel? next;

  final chapter = IsarLink<ChapterModel>();

  @Backlink(to: 'gatha')
  final progress = IsarLink<ProgressModel>();

  GathaModel(
      {required this.id,
      required this.name,
      required this.content,
      required this.explanation,
      required this.order});

  factory GathaModel.fromJson(Map<String, dynamic> json) =>
      _$GathaModelFromJson(json);

  Map<String, dynamic> toJson() => _$GathaModelToJson(this);
}

/// 偈颂进度
///
/// 在线同步只需同步此部分内容
@JsonSerializable()
@collection
class ProgressModel {
  Id id = Isar.autoIncrement;

  /// 偈颂状态更新时间，为秒时间戳，最大值为上次背诵的内容
  @Index()
  @JsonKey(name: "updated_at")
  late int updatedAt;

  @Index()
  @JsonKey(name: "plan_id")
  late int planId;

  @JsonKey(name: "content_id")
  late int gathaId;

  @Enumerated(EnumType.value, "status")
  @JsonKey(defaultValue: ReciteStatus.none)
  late ReciteStatus status;

  final gatha = IsarLink<GathaModel>();

  ProgressModel({
    this.id = Isar.autoIncrement,
    required this.updatedAt,
    required this.planId,
    required this.gathaId,
    this.status = ReciteStatus.none,
  });

  factory ProgressModel.fromJson(Map<String, dynamic> json) =>
      _$ProgressModelFromJson(json);

  Map<String, dynamic> toJson() => _$ProgressModelToJson(this);
}
