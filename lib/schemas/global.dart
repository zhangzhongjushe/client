import 'package:isar/isar.dart';

part 'global.g.dart';

@collection
class GlobalModel {
  Id id = Isar.autoIncrement; // you can also use id = null to auto increment

  bool isLogin = false;

  String username = "";

  String userId = "";

  String? accessToken;

  String? refreshToken;

  DateTime? expiresAt;

  /// 当前背诵的计划ID
  /// 如果小于0，则说明无可用计划
  int currentPlanId = -1;

  int currentBookId = -1;

  /// 上次背诵卡片id
  /// 如果小于0，则说明无记录
  int lastReciteId = -1;

  /// 背诵速度，每天需背诵数量
  int speed = -1;

  /// 剩余背诵天数
  int restDays = -1;

  /// 已完成背诵的数量
  int finishedRecitesTotal = -1;

  /// 需背诵总数
  int totalRecites = -1;

  /// 今日完成的背诵数，可能大于背诵速度
  int finishedRecitesToday = -1;

  /// 当前计划是否已完成，并且弹出过计划完成页面
  bool isPlanFinished = false;
}
