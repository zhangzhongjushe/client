// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'global.dart';

// **************************************************************************
// IsarCollectionGenerator
// **************************************************************************

// coverage:ignore-file
// ignore_for_file: duplicate_ignore, non_constant_identifier_names, constant_identifier_names, invalid_use_of_protected_member, unnecessary_cast, prefer_const_constructors, lines_longer_than_80_chars, require_trailing_commas, inference_failure_on_function_invocation, unnecessary_parenthesis, unnecessary_raw_strings, unnecessary_null_checks, join_return_with_assignment, prefer_final_locals, avoid_js_rounded_ints, avoid_positional_boolean_parameters, always_specify_types

extension GetGlobalModelCollection on Isar {
  IsarCollection<GlobalModel> get globalModels => this.collection();
}

const GlobalModelSchema = CollectionSchema(
  name: r'GlobalModel',
  id: 646955440575769524,
  properties: {
    r'accessToken': PropertySchema(
      id: 0,
      name: r'accessToken',
      type: IsarType.string,
    ),
    r'currentBookId': PropertySchema(
      id: 1,
      name: r'currentBookId',
      type: IsarType.long,
    ),
    r'currentPlanId': PropertySchema(
      id: 2,
      name: r'currentPlanId',
      type: IsarType.long,
    ),
    r'expiresAt': PropertySchema(
      id: 3,
      name: r'expiresAt',
      type: IsarType.dateTime,
    ),
    r'finishedRecitesToday': PropertySchema(
      id: 4,
      name: r'finishedRecitesToday',
      type: IsarType.long,
    ),
    r'finishedRecitesTotal': PropertySchema(
      id: 5,
      name: r'finishedRecitesTotal',
      type: IsarType.long,
    ),
    r'isLogin': PropertySchema(
      id: 6,
      name: r'isLogin',
      type: IsarType.bool,
    ),
    r'isPlanFinished': PropertySchema(
      id: 7,
      name: r'isPlanFinished',
      type: IsarType.bool,
    ),
    r'lastReciteId': PropertySchema(
      id: 8,
      name: r'lastReciteId',
      type: IsarType.long,
    ),
    r'refreshToken': PropertySchema(
      id: 9,
      name: r'refreshToken',
      type: IsarType.string,
    ),
    r'restDays': PropertySchema(
      id: 10,
      name: r'restDays',
      type: IsarType.long,
    ),
    r'speed': PropertySchema(
      id: 11,
      name: r'speed',
      type: IsarType.long,
    ),
    r'totalRecites': PropertySchema(
      id: 12,
      name: r'totalRecites',
      type: IsarType.long,
    ),
    r'userId': PropertySchema(
      id: 13,
      name: r'userId',
      type: IsarType.string,
    ),
    r'username': PropertySchema(
      id: 14,
      name: r'username',
      type: IsarType.string,
    )
  },
  estimateSize: _globalModelEstimateSize,
  serialize: _globalModelSerialize,
  deserialize: _globalModelDeserialize,
  deserializeProp: _globalModelDeserializeProp,
  idName: r'id',
  indexes: {},
  links: {},
  embeddedSchemas: {},
  getId: _globalModelGetId,
  getLinks: _globalModelGetLinks,
  attach: _globalModelAttach,
  version: '3.1.0+1',
);

int _globalModelEstimateSize(
  GlobalModel object,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  var bytesCount = offsets.last;
  {
    final value = object.accessToken;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  {
    final value = object.refreshToken;
    if (value != null) {
      bytesCount += 3 + value.length * 3;
    }
  }
  bytesCount += 3 + object.userId.length * 3;
  bytesCount += 3 + object.username.length * 3;
  return bytesCount;
}

void _globalModelSerialize(
  GlobalModel object,
  IsarWriter writer,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  writer.writeString(offsets[0], object.accessToken);
  writer.writeLong(offsets[1], object.currentBookId);
  writer.writeLong(offsets[2], object.currentPlanId);
  writer.writeDateTime(offsets[3], object.expiresAt);
  writer.writeLong(offsets[4], object.finishedRecitesToday);
  writer.writeLong(offsets[5], object.finishedRecitesTotal);
  writer.writeBool(offsets[6], object.isLogin);
  writer.writeBool(offsets[7], object.isPlanFinished);
  writer.writeLong(offsets[8], object.lastReciteId);
  writer.writeString(offsets[9], object.refreshToken);
  writer.writeLong(offsets[10], object.restDays);
  writer.writeLong(offsets[11], object.speed);
  writer.writeLong(offsets[12], object.totalRecites);
  writer.writeString(offsets[13], object.userId);
  writer.writeString(offsets[14], object.username);
}

GlobalModel _globalModelDeserialize(
  Id id,
  IsarReader reader,
  List<int> offsets,
  Map<Type, List<int>> allOffsets,
) {
  final object = GlobalModel();
  object.accessToken = reader.readStringOrNull(offsets[0]);
  object.currentBookId = reader.readLong(offsets[1]);
  object.currentPlanId = reader.readLong(offsets[2]);
  object.expiresAt = reader.readDateTimeOrNull(offsets[3]);
  object.finishedRecitesToday = reader.readLong(offsets[4]);
  object.finishedRecitesTotal = reader.readLong(offsets[5]);
  object.id = id;
  object.isLogin = reader.readBool(offsets[6]);
  object.isPlanFinished = reader.readBool(offsets[7]);
  object.lastReciteId = reader.readLong(offsets[8]);
  object.refreshToken = reader.readStringOrNull(offsets[9]);
  object.restDays = reader.readLong(offsets[10]);
  object.speed = reader.readLong(offsets[11]);
  object.totalRecites = reader.readLong(offsets[12]);
  object.userId = reader.readString(offsets[13]);
  object.username = reader.readString(offsets[14]);
  return object;
}

P _globalModelDeserializeProp<P>(
  IsarReader reader,
  int propertyId,
  int offset,
  Map<Type, List<int>> allOffsets,
) {
  switch (propertyId) {
    case 0:
      return (reader.readStringOrNull(offset)) as P;
    case 1:
      return (reader.readLong(offset)) as P;
    case 2:
      return (reader.readLong(offset)) as P;
    case 3:
      return (reader.readDateTimeOrNull(offset)) as P;
    case 4:
      return (reader.readLong(offset)) as P;
    case 5:
      return (reader.readLong(offset)) as P;
    case 6:
      return (reader.readBool(offset)) as P;
    case 7:
      return (reader.readBool(offset)) as P;
    case 8:
      return (reader.readLong(offset)) as P;
    case 9:
      return (reader.readStringOrNull(offset)) as P;
    case 10:
      return (reader.readLong(offset)) as P;
    case 11:
      return (reader.readLong(offset)) as P;
    case 12:
      return (reader.readLong(offset)) as P;
    case 13:
      return (reader.readString(offset)) as P;
    case 14:
      return (reader.readString(offset)) as P;
    default:
      throw IsarError('Unknown property with id $propertyId');
  }
}

Id _globalModelGetId(GlobalModel object) {
  return object.id;
}

List<IsarLinkBase<dynamic>> _globalModelGetLinks(GlobalModel object) {
  return [];
}

void _globalModelAttach(
    IsarCollection<dynamic> col, Id id, GlobalModel object) {
  object.id = id;
}

extension GlobalModelQueryWhereSort
    on QueryBuilder<GlobalModel, GlobalModel, QWhere> {
  QueryBuilder<GlobalModel, GlobalModel, QAfterWhere> anyId() {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(const IdWhereClause.any());
    });
  }
}

extension GlobalModelQueryWhere
    on QueryBuilder<GlobalModel, GlobalModel, QWhereClause> {
  QueryBuilder<GlobalModel, GlobalModel, QAfterWhereClause> idEqualTo(Id id) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: id,
        upper: id,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterWhereClause> idNotEqualTo(
      Id id) {
    return QueryBuilder.apply(this, (query) {
      if (query.whereSort == Sort.asc) {
        return query
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            )
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            );
      } else {
        return query
            .addWhereClause(
              IdWhereClause.greaterThan(lower: id, includeLower: false),
            )
            .addWhereClause(
              IdWhereClause.lessThan(upper: id, includeUpper: false),
            );
      }
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterWhereClause> idGreaterThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.greaterThan(lower: id, includeLower: include),
      );
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterWhereClause> idLessThan(Id id,
      {bool include = false}) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(
        IdWhereClause.lessThan(upper: id, includeUpper: include),
      );
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterWhereClause> idBetween(
    Id lowerId,
    Id upperId, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addWhereClause(IdWhereClause.between(
        lower: lowerId,
        includeLower: includeLower,
        upper: upperId,
        includeUpper: includeUpper,
      ));
    });
  }
}

extension GlobalModelQueryFilter
    on QueryBuilder<GlobalModel, GlobalModel, QFilterCondition> {
  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'accessToken',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'accessToken',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'accessToken',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'accessToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'accessToken',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'accessToken',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      accessTokenIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'accessToken',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentBookIdEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'currentBookId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentBookIdGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'currentBookId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentBookIdLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'currentBookId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentBookIdBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'currentBookId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentPlanIdEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'currentPlanId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentPlanIdGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'currentPlanId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentPlanIdLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'currentPlanId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      currentPlanIdBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'currentPlanId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'expiresAt',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'expiresAt',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtEqualTo(DateTime? value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'expiresAt',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtGreaterThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'expiresAt',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtLessThan(
    DateTime? value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'expiresAt',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      expiresAtBetween(
    DateTime? lower,
    DateTime? upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'expiresAt',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTodayEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'finishedRecitesToday',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTodayGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'finishedRecitesToday',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTodayLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'finishedRecitesToday',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTodayBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'finishedRecitesToday',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTotalEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'finishedRecitesTotal',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTotalGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'finishedRecitesTotal',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTotalLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'finishedRecitesTotal',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      finishedRecitesTotalBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'finishedRecitesTotal',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> idEqualTo(
      Id value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> idGreaterThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> idLessThan(
    Id value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'id',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> idBetween(
    Id lower,
    Id upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'id',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> isLoginEqualTo(
      bool value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isLogin',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      isPlanFinishedEqualTo(bool value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'isPlanFinished',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      lastReciteIdEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'lastReciteId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      lastReciteIdGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'lastReciteId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      lastReciteIdLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'lastReciteId',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      lastReciteIdBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'lastReciteId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenIsNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNull(
        property: r'refreshToken',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenIsNotNull() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(const FilterCondition.isNotNull(
        property: r'refreshToken',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenEqualTo(
    String? value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenGreaterThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenLessThan(
    String? value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenBetween(
    String? lower,
    String? upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'refreshToken',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'refreshToken',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenMatches(String pattern, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'refreshToken',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'refreshToken',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      refreshTokenIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'refreshToken',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> restDaysEqualTo(
      int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'restDays',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      restDaysGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'restDays',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      restDaysLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'restDays',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> restDaysBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'restDays',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> speedEqualTo(
      int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      speedGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> speedLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'speed',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> speedBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'speed',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      totalRecitesEqualTo(int value) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'totalRecites',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      totalRecitesGreaterThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'totalRecites',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      totalRecitesLessThan(
    int value, {
    bool include = false,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'totalRecites',
        value: value,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      totalRecitesBetween(
    int lower,
    int upper, {
    bool includeLower = true,
    bool includeUpper = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'totalRecites',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      userIdGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'userId',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      userIdStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdContains(
      String value,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'userId',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> userIdMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'userId',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      userIdIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'userId',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      userIdIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'userId',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> usernameEqualTo(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameGreaterThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        include: include,
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameLessThan(
    String value, {
    bool include = false,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.lessThan(
        include: include,
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> usernameBetween(
    String lower,
    String upper, {
    bool includeLower = true,
    bool includeUpper = true,
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.between(
        property: r'username',
        lower: lower,
        includeLower: includeLower,
        upper: upper,
        includeUpper: includeUpper,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameStartsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.startsWith(
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameEndsWith(
    String value, {
    bool caseSensitive = true,
  }) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.endsWith(
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameContains(String value, {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.contains(
        property: r'username',
        value: value,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition> usernameMatches(
      String pattern,
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.matches(
        property: r'username',
        wildcard: pattern,
        caseSensitive: caseSensitive,
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameIsEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.equalTo(
        property: r'username',
        value: '',
      ));
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterFilterCondition>
      usernameIsNotEmpty() {
    return QueryBuilder.apply(this, (query) {
      return query.addFilterCondition(FilterCondition.greaterThan(
        property: r'username',
        value: '',
      ));
    });
  }
}

extension GlobalModelQueryObject
    on QueryBuilder<GlobalModel, GlobalModel, QFilterCondition> {}

extension GlobalModelQueryLinks
    on QueryBuilder<GlobalModel, GlobalModel, QFilterCondition> {}

extension GlobalModelQuerySortBy
    on QueryBuilder<GlobalModel, GlobalModel, QSortBy> {
  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByAccessToken() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accessToken', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByAccessTokenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accessToken', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByCurrentBookId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentBookId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByCurrentBookIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentBookId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByCurrentPlanId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentPlanId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByCurrentPlanIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentPlanId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByExpiresAt() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expiresAt', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByExpiresAtDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expiresAt', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByFinishedRecitesToday() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesToday', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByFinishedRecitesTodayDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesToday', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByFinishedRecitesTotal() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesTotal', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByFinishedRecitesTotalDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesTotal', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByIsLogin() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLogin', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByIsLoginDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLogin', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByIsPlanFinished() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isPlanFinished', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByIsPlanFinishedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isPlanFinished', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByLastReciteId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastReciteId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByLastReciteIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastReciteId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByRefreshToken() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'refreshToken', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByRefreshTokenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'refreshToken', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByRestDays() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'restDays', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByRestDaysDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'restDays', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortBySpeed() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'speed', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortBySpeedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'speed', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByTotalRecites() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalRecites', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      sortByTotalRecitesDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalRecites', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByUserId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByUserIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByUsername() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'username', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> sortByUsernameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'username', Sort.desc);
    });
  }
}

extension GlobalModelQuerySortThenBy
    on QueryBuilder<GlobalModel, GlobalModel, QSortThenBy> {
  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByAccessToken() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accessToken', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByAccessTokenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'accessToken', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByCurrentBookId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentBookId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByCurrentBookIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentBookId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByCurrentPlanId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentPlanId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByCurrentPlanIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'currentPlanId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByExpiresAt() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expiresAt', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByExpiresAtDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'expiresAt', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByFinishedRecitesToday() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesToday', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByFinishedRecitesTodayDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesToday', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByFinishedRecitesTotal() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesTotal', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByFinishedRecitesTotalDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'finishedRecitesTotal', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenById() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'id', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByIsLogin() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLogin', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByIsLoginDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isLogin', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByIsPlanFinished() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isPlanFinished', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByIsPlanFinishedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'isPlanFinished', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByLastReciteId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastReciteId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByLastReciteIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'lastReciteId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByRefreshToken() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'refreshToken', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByRefreshTokenDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'refreshToken', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByRestDays() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'restDays', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByRestDaysDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'restDays', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenBySpeed() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'speed', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenBySpeedDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'speed', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByTotalRecites() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalRecites', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy>
      thenByTotalRecitesDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'totalRecites', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByUserId() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userId', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByUserIdDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'userId', Sort.desc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByUsername() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'username', Sort.asc);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QAfterSortBy> thenByUsernameDesc() {
    return QueryBuilder.apply(this, (query) {
      return query.addSortBy(r'username', Sort.desc);
    });
  }
}

extension GlobalModelQueryWhereDistinct
    on QueryBuilder<GlobalModel, GlobalModel, QDistinct> {
  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByAccessToken(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'accessToken', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByCurrentBookId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'currentBookId');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByCurrentPlanId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'currentPlanId');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByExpiresAt() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'expiresAt');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct>
      distinctByFinishedRecitesToday() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'finishedRecitesToday');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct>
      distinctByFinishedRecitesTotal() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'finishedRecitesTotal');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByIsLogin() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isLogin');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByIsPlanFinished() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'isPlanFinished');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByLastReciteId() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'lastReciteId');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByRefreshToken(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'refreshToken', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByRestDays() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'restDays');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctBySpeed() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'speed');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByTotalRecites() {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'totalRecites');
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByUserId(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'userId', caseSensitive: caseSensitive);
    });
  }

  QueryBuilder<GlobalModel, GlobalModel, QDistinct> distinctByUsername(
      {bool caseSensitive = true}) {
    return QueryBuilder.apply(this, (query) {
      return query.addDistinctBy(r'username', caseSensitive: caseSensitive);
    });
  }
}

extension GlobalModelQueryProperty
    on QueryBuilder<GlobalModel, GlobalModel, QQueryProperty> {
  QueryBuilder<GlobalModel, int, QQueryOperations> idProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'id');
    });
  }

  QueryBuilder<GlobalModel, String?, QQueryOperations> accessTokenProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'accessToken');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> currentBookIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'currentBookId');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> currentPlanIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'currentPlanId');
    });
  }

  QueryBuilder<GlobalModel, DateTime?, QQueryOperations> expiresAtProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'expiresAt');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations>
      finishedRecitesTodayProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'finishedRecitesToday');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations>
      finishedRecitesTotalProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'finishedRecitesTotal');
    });
  }

  QueryBuilder<GlobalModel, bool, QQueryOperations> isLoginProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isLogin');
    });
  }

  QueryBuilder<GlobalModel, bool, QQueryOperations> isPlanFinishedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'isPlanFinished');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> lastReciteIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'lastReciteId');
    });
  }

  QueryBuilder<GlobalModel, String?, QQueryOperations> refreshTokenProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'refreshToken');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> restDaysProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'restDays');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> speedProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'speed');
    });
  }

  QueryBuilder<GlobalModel, int, QQueryOperations> totalRecitesProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'totalRecites');
    });
  }

  QueryBuilder<GlobalModel, String, QQueryOperations> userIdProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'userId');
    });
  }

  QueryBuilder<GlobalModel, String, QQueryOperations> usernameProperty() {
    return QueryBuilder.apply(this, (query) {
      return query.addPropertyName(r'username');
    });
  }
}
