import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as path;
import 'dart:io';
import 'package:path_provider/path_provider.dart';

import '../api/api.dart';
import '../config.dart';




Future<String?> downloadFile(String url, String fileName) async {
  // var request = await http.Client().get(Uri.parse(url));
  // var bytes = request.bodyBytes;
  // File file = File('${Config.audioPath}/$fileName');
  // await file.writeAsBytes(bytes);

  String filePath = '${Config.audioPath}/$fileName';
  final resp = await Http().request.download(url, filePath);
  if (resp.statusCode == 200) {
    return filePath;
  }
  return null;
}

Future<bool> isFileExist(String filePath) async {
  File file = File(filePath);

  if (file.existsSync()) {
    return true;
  } else {
    return false;
  }
}
