import 'package:flutter/material.dart';

ColorScheme lightScheme = const ColorScheme.light();

ColorScheme darkScheme = const ColorScheme.dark();

ThemeData lightTheme = ThemeData(
  useMaterial3: true,
  brightness: Brightness.light,
  colorScheme: lightScheme,
  // fontFamily: "PingFang",
);

ThemeData darkTheme = ThemeData(
  useMaterial3: true,
  brightness: Brightness.dark,
  colorScheme: darkScheme,
  textTheme: const TextTheme(),
  // fontFamily: "PingFang",
);
