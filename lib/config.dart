import 'package:flutter/foundation.dart';

/// Manage application basic config.
class Config {
  static const String appName = "掌中俱舍";
  static  String audioPath = '';

  static bool isShowNews = false;

  // 鉴权及业务后端
  // mock服务
  // static const String baseUrl = "https://yapi.mojerro.wang/mock/31";
  // 应用入口
  static String baseUrl = kDebugMode
      ? "http://zzjs.api.dev.tulips-tree.top"
      : "https://zzjs.api.tulips-tree.top";
}
