// ignore_for_file: depend_on_referenced_packages

import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:zzjs_client/config.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/theme/theme.dart';
import 'package:zzjs_client/translations/translations.dart';

Future<void> main() async {
  await initServices();

  // TODO：packageInfo在状态管理中重复获取，待解决
  PackageInfo packageInfo = await PackageInfo.fromPlatform();

  String mode;
  switch (true) {
    case kDebugMode:
      mode = 'debug';
      break;
    case kProfileMode:
      mode = 'profile';
      break;
    // TODO: 待确认kProfileMode kReleaseMode是否可以区分开
    // ignore: no_duplicate_case_values
    case kReleaseMode:
      mode = 'release';
      break;
    default:
      mode = 'unknown';
  }

  if (kDebugMode) {
    Get.testMode = true;

    // After flutter_ume 0.3.0
    await SentryFlutter.init(
      (options) => options
        ..dsn = 'https://8a522500443845fe8b7a772b4d2c711b@sentry.mojerro.wang/5'
        // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
        // We recommend adjusting this value in production.
        ..release = "com.zzjs.client@${packageInfo.version}"
        ..environment = mode
        ..tracesSampleRate = .1,
      appRunner: () => runApp(const ZZJSApp()),
    );
  } else {
    await SentryFlutter.init(
      (options) => options
        ..dsn = 'https://8a522500443845fe8b7a772b4d2c711b@sentry.mojerro.wang/5'
        // Set tracesSampleRate to 1.0 to capture 100% of transactions for performance monitoring.
        // We recommend adjusting this value in production.
        ..release =
            "com.zzjs.client@${packageInfo.version}+${packageInfo.buildNumber}"
        ..environment = mode
        ..tracesSampleRate = 1.0,
      appRunner: () => runApp(const ZZJSApp()),
    );
  }
  if (GetPlatform.isAndroid) {
    // 设置状态栏背景及颜色
    SystemUiOverlayStyle systemUiOverlayStyle =
        const SystemUiOverlayStyle(statusBarColor: Colors.transparent);
    SystemChrome.setSystemUIOverlayStyle(systemUiOverlayStyle);
    // 隐藏状态栏，支持多种隐藏动画
    // SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
  }
}

/// 在你运行Flutter应用之前，让你的服务初始化是一个明智之举。
////因为你可以控制执行流程（也许你需要加载一些主题配置，apiKey，由用户自定义的语言等，所以在运行ApiService之前加载SettingService。
///所以GetMaterialApp()不需要重建，可以直接取值。
Future<void> initServices() async {
  WidgetsFlutterBinding.ensureInitialized();
  log('starting services ...');

  ///这里是你放get_storage、hive、shared_pref初始化的地方。
  ///或者moor连接，或者其他什么异步的东西。
  await Get.putAsync(() => GlobalService().init());

  log('All services started...');
}

class ZZJSApp extends StatelessWidget {
  const ZZJSApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    GlobalService.to.getDeviceInfo();

    return GetMaterialApp(
      title: Config.appName,
      debugShowCheckedModeBanner: true,
      defaultTransition: Transition.fade,
      getPages: AppPages.pages,
      initialRoute: GlobalService.to.isLogin.value ? Routes.HOME : Routes.LOGIN,
      // locale: Get.deviceLocale,
      locale: const Locale('zh', 'CN'),
      translations: Translation(),
      theme: lightTheme,
      darkTheme: darkTheme,
      themeMode: ThemeMode.light,
      navigatorObservers: [
        PosthogObserver(),
      ],
      routingCallback: (routing) async {
        switch (routing?.current) {
          case Routes.GATHA_LIST:
            // 如果后退回到偈颂列表页，重新渲染当前列表
            if (routing?.isBack ?? false) {
              await ReciteProgressController.to.resetData();
              ReciteProgressController.to.currentController.reciteGathaList.clear();
              ReciteProgressController.to.currentController.reciteGathaList
                  .addAll(ReciteProgressController.to.gathaListMap[
                      ReciteProgressController.to.currentStatus]!);
              ReciteProgressController.to.currentController.sortGathaList();
            }
            break;
          case Routes.HOME:
            // 如果后退回到首页，重新计算进度数据
            if (routing?.isBack ?? false) {
              await ReciteProgressController.to.resetData();
            }
            break;
          default:
        }
      },
    );
  }
}
