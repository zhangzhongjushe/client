import 'package:flutter/material.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/routes/pages.dart';
// import 'package:get/get.dart';

class NotificationPage extends StatelessWidget {
  const NotificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.NOTIFICATION,
    );

    return Scaffold(
      appBar: AppBar(title: const Text('NotificationPage')),
      body: const Text(""),
    );
  }
}
