import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/routes/pages.dart';

/// 首页
class SettingPage extends StatelessWidget {
  const SettingPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.SETTING,
    );

    return Scaffold(
      appBar: AppBar(
        title: Text('设置'.tr),
        centerTitle: true,
        backgroundColor: const Color(0xFFF3F4FC),
      ),
      body: SafeArea(
        top: false,
        // 兼容异形屏
        child: SingleChildScrollView(
          child: Stack(children: [
            // 背景轮播图
            Container(
              height: 220 - kToolbarHeight - Get.mediaQuery.padding.top,
              color: const Color(0xFFF3F4FC),
            ),
            // 用户信息
            Padding(
              padding: const EdgeInsets.only(
                left: 24,
                top: 24,
              ),
              child: Row(
                children: [
                  CircleAvatar(
                    backgroundColor: const Color(0xFF65BFD0),
                    foregroundImage: AssetImage(
                        "images/placeholder/avatar/avatar${Random().nextInt(7) + 1}.png"),
                  ),
                  const SizedBox(width: 20),
                  Text(
                    GlobalService.to.username.value,
                    style: const TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
            // 选项行
            Container(
              margin: EdgeInsets.only(
                  top: 200 - kToolbarHeight - Get.mediaQuery.padding.top),
              child: Container(
                  width: double.infinity,
                  height: Get.height - 200,
                  decoration: const BoxDecoration(
                    color: Colors.white,
                    borderRadius:
                        BorderRadius.vertical(top: Radius.circular(20)),
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.toNamed(Routes.ABOUT);
                            },
                            child: Container(
                              height: 36,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 24, horizontal: 16),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    "关于我们",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  Icon(Icons.chevron_right),
                                ],
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () async {
                              // TODO: 参考示例项目优化展示
                              Posthog().capture(eventName: '查看许可证书');
                              Get.to(LicensePage(
                                applicationName: "掌中俱舍".tr,
                                applicationLegalese:
                                    "@2023 掌中俱舍 ${GlobalService.to.deviceInfoString}",
                                applicationIcon: const FlutterLogo(),
                                applicationVersion:
                                    GlobalService.to.packageInfoString,
                              ));
                            },
                            child: Container(
                              height: 36,
                              margin: const EdgeInsets.symmetric(
                                  vertical: 24, horizontal: 16),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: const [
                                  Text(
                                    "许可证书",
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  Icon(Icons.chevron_right),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                      // 退出登录
                      Container(
                        margin: const EdgeInsets.only(bottom: 50),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(225, 40),
                              backgroundColor: const Color(0xFFC49C50)),
                          onPressed: () {
                            // 退出登录二次确认
                            Get.dialog(
                              CupertinoAlertDialog(
                                title: Text("退出账号".tr),
                                content: Text("确定退出当前账号".tr),
                                actions: <CupertinoDialogAction>[
                                  CupertinoDialogAction(
                                    isDestructiveAction: true,
                                    onPressed: () {
                                      Get.back();
                                    },
                                    child: Text("取消".tr),
                                  ),
                                  CupertinoDialogAction(
                                    isDefaultAction: true,
                                    onPressed: () async {
                                      await GlobalService.to.logout();
                                    },
                                    child: Text("确定".tr),
                                  ),
                                ],
                              ),
                            );
                          },
                          child: Text(
                            "退出账号".tr,
                            style: const TextStyle(
                              color: Colors.white,
                              fontSize: 16,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )),
            ),
          ]),
        ),
      ),
    );
  }
}
