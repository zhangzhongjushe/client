import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzjs_client/api/user.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/routes/params.dart';

/// 注册页
class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});

  @override
  Widget build(BuildContext context) {
    Rx<GlobalKey<FormState>> formKey = GlobalKey<FormState>().obs;

    // 线上-默认为false
    RxBool isChecked = RxBool(false);

    if (kDebugMode) {
      isChecked.toggle();
    }

    // 线上-删除测试账户密码
    Rx<TextEditingController> usernameController =
        TextEditingController(text: "").obs;
    Rx<TextEditingController> passwordController =
        TextEditingController(text: "").obs;
    Rx<TextEditingController> codeController =
        TextEditingController(text: "").obs;
    usernameController.value.addListener(
      () {
        usernameController.update((_) {});
      },
    );

    passwordController.value.addListener(
      () {
        passwordController.update((_) {});
      },
    );

    onTapRegister() async {
      if (formKey.value.currentState!.validate() && isChecked.value) {
        await registerByPassword(usernameController.value.text,
            passwordController.value.text, codeController.value.text);

        await getUserInfo();
        // TODO: log info
        Get.showSnackbar(GetSnackBar(
          message: "欢迎加入“每日背诵，生生增上”团队！".tr,
          duration: const Duration(seconds: 3),
        ));
        Get.offNamed(Routes.HOME);
      } else if (!isChecked.value) {
        Get.showSnackbar(GetSnackBar(
          message: "请勾选同意“用户协议”和“隐私政策”".tr,
          duration: const Duration(seconds: 3),
        ));
      }
    }

    return GestureDetector(
      onTap: () {
        if (GetPlatform.isMobile) {
          FocusScope.of(context).requestFocus(FocusNode());
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: const Color(0xFFF3F4FC),
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.transparent,
          title: Text("用户注册".tr),
          centerTitle: false,
          // THEME
          titleTextStyle: const TextStyle(
            fontSize: 32,
            color: Colors.black87,
            fontWeight: FontWeight.w600,
          ),
          toolbarHeight: 120,
          flexibleSpace: FlexibleSpaceBar(
              background: Image.asset(
            "images/background/login.jpg",
            fit: BoxFit.cover,
          )),
        ),
        body: Center(
          child: Obx(
            () => Form(
              key: formKey.value,
              autovalidateMode: AutovalidateMode.onUserInteraction,
              child: Column(
                children: [
                  // 用户名
                  Container(
                    height: 48,
                    margin: const EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 24,
                    ),
                    child: TextFormField(
                      controller: usernameController.value,
                      decoration: InputDecoration(
                        hintText: "请输入手机号或邮箱".tr,
                        suffixIcon: const IconButton(
                          onPressed: null,
                          icon: Icon(Icons.error_outline,
                              color: Color(0xFF979797)),
                        ),
                      ),
                      autofocus: true,
                      maxLength: 16,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return '用户名不能为空'.tr;
                        }
                        return null;
                      },
                      onEditingComplete: onTapRegister,
                    ),
                  ),
                  //验证码
                  Container(
                    height: 65,
                    margin: const EdgeInsets.symmetric(
                      vertical: 12,
                      horizontal: 24,
                    ),
                    child: Stack(
                      children: [
                        TextFormField(
                          controller: usernameController.value,
                          decoration: InputDecoration(
                            hintText: "请输入验证码".tr,
                          ),
                          autofocus: true,
                          maxLength: 8,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return '用户名不能为空'.tr;
                            }
                            return null;
                          },
                          onEditingComplete: onTapRegister,
                        ),
                         Positioned(
                          right: 0,
                          top: -2,
                          child:
                          VerificationCodeButton(
                           text: '获取验证码', onPressed: () async {
                             print('123');
                            var dio = Dio();
                            var response = await dio.post(
                                'https://zzjs.api.tulips-tree.top/api/user/verifycode',
                                data: {
                                  'username': '17608471349',
                                }
                            );
                            print('123');
                            var esp = await dio.post<Map>(
                              "https://zzjs.api.tulips-tree.top/api/user/login",
                              data: {
                                "username": 'username',
                                "password": 'yijiaofengxing',
                              },
                            );
                            print('esp:$esp');
                            print('response:$response');
                          },
                          ),
                          // OutlinedButton(
                          //   onPressed: () {
                          //     // 按钮点击事件
                          //   },
                          //   style: OutlinedButton.styleFrom(
                          //       fixedSize: const Size(120, 20),
                          //       shape: RoundedRectangleBorder(
                          //           borderRadius: BorderRadius.circular(20.0)),
                          //       backgroundColor: Colors.white,
                          //       foregroundColor: const Color(0xFFC7A360),
                          //       side: const BorderSide(
                          //           width: 1, color: Color(0xFFC7A360))),
                          //   child: const Text(
                          //     '获取验证码',
                          //     style: TextStyle(fontSize: 14),
                          //   ), // 设置按钮文本和文本颜色
                          // ),
                        ),
                      ],
                    ),
                  ),

                  // 密码输入框
                  Container(
                    height: 48,
                    margin: const EdgeInsets.fromLTRB(24, 36, 24, 16),
                    child: TextFormField(
                      controller: passwordController.value,
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          onPressed: () {
                            // 清空密码
                            passwordController.value.clear();
                          },
                          icon: const Icon(
                            Icons.cancel,
                            color: Color(0xFF979797),
                          ),
                        ),
                        hintText: "请输入密码".tr,
                      ),
                      obscureText: true,
                      maxLength: 32,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return '密码不能为空'.tr;
                        }
                        return null;
                      },
                      onEditingComplete: onTapRegister,
                    ),
                  ),
                  Container(
                    height: 48,
                    margin: const EdgeInsets.fromLTRB(24, 36, 24, 16),
                    child: TextFormField(
                      controller: passwordController.value,
                      decoration: InputDecoration(
                        suffixIcon: IconButton(
                          onPressed: () {
                            // 清空密码
                            passwordController.value.clear();
                          },
                          icon: const Icon(
                            Icons.cancel,
                            color: Color(0xFF979797),
                          ),
                        ),
                        hintText: "确认密码".tr,
                      ),
                      obscureText: true,
                      maxLength: 32,
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return '密码不能为空'.tr;
                        }
                        return null;
                      },
                      onEditingComplete: onTapRegister,
                    ),
                  ),
                  // 忘记密码
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 32),
                    alignment: Alignment.centerRight,
                    child: Text("忘记密码".tr),
                  ),
                  //用户协议确认
                  Container(
                    margin: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 14,
                    ),
                    child: GestureDetector(
                      onTap: () {
                        isChecked.value = !isChecked.value;
                        if (GetPlatform.isMobile) {
                          FocusScope.of(context).requestFocus(FocusNode());
                        }
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Container(
                            margin: const EdgeInsets.only(right: 8),
                            child: isChecked.value
                                ? const Icon(
                                    Icons.check_circle,
                                    color: Color(0xFFC7A360),
                                  )
                                : const Icon(
                                    Icons.radio_button_off,
                                    color: Color(0xFF979797),
                                  ),
                          ),
                          const UserAgreement(),
                        ],
                      ),
                    ),
                  ),
                  Expanded(child: Container()),
                  // 登录按钮
                  Container(
                    margin: const EdgeInsets.only(bottom: 50),
                    width: 256,
                    height: 40,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          backgroundColor: usernameController
                                      .value.text.isNotEmpty &&
                                  passwordController.value.text.isNotEmpty &&
                                  isChecked.value
                              ? const Color(0xFFC49C50)
                              : const Color(0xFFE0D1B7)),
                      onPressed: usernameController.value.text.isNotEmpty &&
                              passwordController.value.text.isNotEmpty &&
                              isChecked.value
                          ? onTapRegister
                          : null,
                      child: Text(
                        "注册并登录".tr,
                        style: const TextStyle(
                          color: Colors.white,
                          fontSize: 16,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

/// 用户协议中“低调”文本的样式。
const TextStyle _lowProfileStyle = TextStyle(
  fontSize: 12.0,
  color: Color(0xFF4A4A4A),
);

/// 用户协议中“高调”文本的样式。
const TextStyle _highProfileStyle = TextStyle(
  fontSize: 12.0,
  color: Color(0xFF00CED2),
);

/// 自定义的用户协议组件。
class UserAgreement extends StatelessWidget {
  const UserAgreement({super.key});

  @override
  Widget build(BuildContext context) {
    // 文本（`Text`）组件，是一系列具有单一样式的文本。
    // 文本.丰富（`Text.rich`）构造函数，则是使用文字跨度（`TextSpan`）组件创建文本。
    return Text.rich(
      // 文字跨度（`TextSpan`）组件，不可变的文本范围。
      TextSpan(
        // 文本（`text`）属性，跨度中包含的文本。
        text: '登录代表你已同意',
        // 样式（`style`）属性，应用于文本和子组件的样式。
        style: _lowProfileStyle,
        children: [
          TextSpan(
            // 识别（`recognizer`）属性，一个手势识别器，它将接收触及此文本范围的事件。
            // 手势（`gestures`）库的点击手势识别器（`TapGestureRecognizer`）类，识别点击手势。
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                // 点击打开“用户协议”
                Get.toNamed(Routes.AGREEMENT,
                    arguments: AgreementParams("用户协议".tr, """
感谢您选择注册并使用“掌中俱舍”，在使用“掌中俱舍”软件及相关服务前，请您认真阅读本协议，并确认承诺同意遵守本协议之全部约定。阅读本协议的过程中，如果您不同意本协议或其中任何条款约定，您应立即停止注册程序。如果您未申请注册流程，或者在本协议生效前已成为我方平台的注册用户，当您通过访问和/或使用我方平台或者接受我方平台所提供的服务或产品时，将视为您表示同意接受本协议的全部内容，否则请您不要访问或使用我方平台。

一、协议条款的确认及接受
1.1 “掌中俱舍”由掌中俱舍团队运营并享有完全的所有权及知识产权等权益，“掌中俱舍”提供的服务将完全按照其发布的条款和操作规则严格执行。
1.2 您确认同意本协议[协议文本包括《用户协议》《隐私政策》及“掌中俱舍”已公示或将来公示的各项规则及提示（包括但不限于我方平台内已经发布及后续发布的全部规则、解读、公告等发布的各类规则、实施细则、产品说明、公告等），所有前述协议、规则及提示乃不可分割的整体，具有同等条约效力，共同构成用户使用“掌中俱舍”及相关服务的整体协议，以下合称“本协议”]所有条款并完成注册程序时，本协议在您与我方间成立并立即发生条约效力，同时您成为“掌中俱舍”正式用户。
1.3 用户应当按照“掌中俱舍”指定方式下载安装本软件产品。谨防在非指定方式下载本软件，以免移动终端设备感染能破坏用户数据和获取用户隐私信息的恶意程序。如果用户从未经“掌中俱舍”团队授权的第三方获取本软件或与本软件名称相同的安装程序，“掌中俱舍”无法保证该软件能够正常使用，并对因此给您造成的损失不予负责。
1.4 用户必须选择与所安装终端设备相匹配的本软件版本，否则，由于软件与设备型号不相匹配所导致的任何软件问题、设备问题或损害，均由用户自行承担。
1.5 为了改善用户体验、完善服务内容，“掌中俱舍”有权不定期地为您提供本软件替换、修改、升级版本，但会对您同步进行消息告知。软件新版本发布后，“掌中俱舍”不保证旧版本软件的继续可用。

二、账户注册与使用
2.1 您确认，在您开始使用/注册程序使用我方平台服务前，您应当具备中华人民共和国法律规定的与您行为相适应的民事行为能力。若您不具备前述与您行为相适应的民事行为能力，则您及您的监护人应依照法律规定承担因此而导致的一切后果。
特别提醒：本软件目前不向未成年人开放使用，管理团队会严格审核每个用户的个人情况，请您知悉。
2.2 当您按照注册页面提示填写信息、阅读并同意本协议且完成全部注册程序后，您可获得我方平台账户并成为我方平台用户。您有权使用您设置或确认的我方会员名、邮箱、手机号码（以下简称“账户名称”）及您设置的密码（账户名称及密码合称“账户”）登录我方平台。
2.3 关于注册后的个人账户，目前仅限于别人使用，不得以任何方式转让他人，如经发现，则会取消该账户资格。
2.4 为使您更好地使用我方平台的各项服务，您需按照我方平台规定完成实名认证。
2.5 如您的账户在任何连续180日内未实际使用，我方有权予以进行账户回收等处理，您的账户将不能再登录我方任一平台，相应服务同时终止。我方在对此类账户进行处理前，将以包括但不限于站内消息、客户端推送信息等方式通知您。
2.6 在使用我方平台服务时，您应当按我方平台页面的提示准确完整地提供您的信息（包括您的姓名、经实名认证后使用的移动电话号码、联系地址等），以便我方在必要时与您联系。您了解并同意，您有义务保证和保持您提供信息的真实性及有效性。
2.7 您所设置的账户名称信息不得违反国家法律法规及我方平台规则关于账户名称信息的管理规定，否则我方可对您的账户进行暂停使用或注销等处理。
2.8 您理解并承诺，您的账户名称、头像和简介等注册信息中不得出现违法和不良信息，没有模仿、冒用、关联组织机构或社会名人等任何侵犯或可能侵犯他人权益的行为，您在账户注册和使用过程中需遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线以及相关法律法规的要求。
2.9 您同意并授权，为了更好的为您提供服务以及确保您的账户安全，我方可以根据您提供的手机号码等信息，向相关人员核实用户身份真实性，以维护良好安全的学习环境。
2.10 如果个人重要信息有变动，您应当配合提供最新、真实、准确、完整的信息。
如我方按您最后一次提供的信息与您联系未果、或您未按照我方的要求及时提供信息、或您提供的信息存在明显不实的，您将承担因此对您自身、他人及我方造成的全部损失与不利后果。
2.11 您的账户由您自行设置、保管。建议您务必保管好您的账户，并确保您在每个上网时段结束时退出登录并以正确步骤离开我方平台。
2.12 账户因您主动泄露或遭受他人攻击、诈骗等行为导致的损失及后果，均需由您自行承担。
2.13 您应妥善保管自己的账户名称、密码，不应将账户转让、出售、出租、出借或分享予他人使用，否则“掌中俱舍”有权根据实际情况暂时封停或永久查封此账户。当您的账户名称或密码遭到未经授权的使用时，您应立即通知“掌中俱舍”团队，否则未经授权的使用行为均视为您本人行为。为保障您的账户安全，您同意当存在多个设备登录您的账户时，“掌中俱舍”可以采取必要的安全措施，包括但不限于暂时冻结您的账户等。
2.14 除我方存在过错外，您应对您账户项下的所有行为结果（包括但不限于在线签署各类协议、发布信息、发布评价及披露信息等）负责。
2.15 如发现任何未经授权使用您账户访问和/或使用我方平台或其他可能导致您账户遭窃、遗失的情况，建议您立即通知我方。您理解我方对您的任何请求采取行动均需要合理时间，除我方存在过错外，我方对在采取行动前已经产生的后果不承担任何责任。

三、服务内容
我方可能为不同的终端设备及使用需求开发不同的应用程序软件版本，您应当根据实际设备及需求状况从正确渠道获取、下载、安装合适的版本。
我方的服务具体内容根据实际情况提供，“掌中俱舍”保留变更、中断或终止部分或全部服务的权利。“掌中俱舍”不承担因业务调整给您造成的损失。除非本协议另有其它明示规定，增加或强化目前我方平台的任何新功能，包括所推出的新产品，均受到本协议之规范。您了解并同意，我方服务仅依其当前所呈现的状况提供，对于任何用户通讯或个人化设定之时效、删除、传递错误、未予储存或其它任何问题，“掌中俱舍”均不承担任何责任。如您不接受服务调整，请停止使用本服务；否则，您的继续使用行为将被视为对调整服务的完全同意且承诺遵守。

四、服务使用规则
4.1 禁止的内容
4.1.1 您理解并保证您在我方平台上传、发布或传输的内容（包括您的账户名称、头像、个人简介等信息）不含有以下内容：
· 反对宪法确定的基本原则的；
· 危害国家安全，泄露国家秘密，颠覆国家政破坏国家统一的；
· 损害国家荣誉和利益的，损害公共利益的；
· 煽动民族仇恨、民族歧视，破坏民族团结的；
· 破坏国家宗教政策，宣扬邪教和封建迷信的；
· 散布谣言，扰乱社会秩序，破坏社会稳定的；
· 散布淫秽、色情、赌博、暴力、凶杀、恐怖或者教唆犯罪的；
· 侮辱或者诽谤他人，侵害他人合法权利的；
· 含有虚假、诈骗、有害、胁迫、侵害他人隐私、骚扰、侵害、中伤、粗俗、猥亵、或其他道德上令人反感的内容；
· 含有中国法律、法规、（部门）规章以及任何具有法律效力之规范所限制或禁止的其他内容的；
· 针对种族、国家、民族、宗教、性别、年龄、地缘、性取向、生理特征的歧视和仇恨言论；
· 含有传教行为、举行宗教活动、成立宗教组织、设立宗教活动场所等内容；
· 含有我方认为不适合在掌中俱舍展示的内容的。
4.1.2 如果您上传、发布或传输的内容含有以上违反法律法规的信息或内容的，或者侵犯任何第三方的合法权益，您将直接承担以上违法或侵权行为导致的一切不利后果。
4.2 用户义务与责任
4.2.1 您不得从事以下行为：
· 未经我方同意，向其他用户或其他第三方转让、出借、许可使用、出售全部或部分服务；
· 妨碍我方提供服务的行为；
· 利用我方服务进行营利及投机行为；
· 犯罪行为及与犯罪有关的行为；
· 使用不恰当语言或行为侮辱、骚扰他人的行为；
· 违反公序良俗的行为；
· 其它我方判断为不适当的行为。
4.2.2 您承诺：
遵守所有关于使用网络服务的法律法规规定、网络协议、程序和惯例；不得利用我方平台制作、复制、查阅和传播下列信息：
· 任何不符合国家法律、法规规定的资料、信息；
· 不利于国家团结和社会安定的内容；
· 任何捏造或者歪曲事实，散布谣言，扰乱社会秩序的信息；
· 任何教唆他人进行违法犯罪行为的内容；
· 任何非法的、骚扰性的、中伤他人的、辱骂性的、恐吓性的、伤害性的、庸俗的、淫秽的信息；
4.2.3 未经允许，不得对我方平台中存储、处理或者传输的数据和应用程序进行删除、修改或者增加；
4.2.4 不得故意制作、传播计算机病毒等破坏性程序；
4.2.5 遵守法律法规、社会主义制度、国家利益、公民合法权益、公共秩序、社会道德风尚和信息真实性等七条底线；
4.2.6 其它法律法规以及行政规章禁止实施的行为。
4.3 服务规范
为方便您使用我方和我方关联的第三方的其他相关服务， 您授权我方将您在账户注册和使用我方平台服务过程中提供、形成的信息传递给我方关联的第三方等其他相关服务提供者，或从我方关联的第三方等其他相关服务提供者获取您在注册、使用相关服务期间提供、形成的信息。
4.3.1 您理解并知晓在使用我方平台服务时，所接触的内容和信息来源广泛，我方无法对该内容和信息的准确性、真实性、可用性、安全性、完整性和正当性负责。您理解并认可您可能会接触到不正确的、令人不快的、不适当的或令人厌恶的内容和信息，您不会以此追究我方的相关责任。我方不对用户在我方平台上传、发布或传输的任何内容和信息背书、推荐或表达观点，也不对任何内容和信息的错误、瑕疵及产生的损失或损害承担任何责任，您对内容和信息的任何使用需自行承担相关的风险。
4.3.2 您同意在使用我方平台服务过程中，遵守以下法律法规：《中国人民共和国网络安全法》《中华人民共和国著作权法》《中华人民共和国计算机信息系统安全保护条例》《信息网络传播权保护条例》等有关计算机及互联网规定的法律、法规。在任何情况下，我方一旦合理地认为您的行为可能违反上述法律、法规，可以在任何时候，不经事先通知终止向您提供服务。

五、第三方链接
我方平台服务可能会包含与其他网站或资源的链接。我方对于前述网站或资源的内容、隐私政策和活动无权控制、审查或修改，您因使用或依赖前述网站或资源所产生的损失或损害，我方不承担任何责任。

六、知识产权
6.1 “掌中俱舍”就本软件给予用户一项个人的、不可转让、不可转授权以及非独占性的许可。
6.2 用户可以为非商业目的在单一台移动终端设备上安装、使用、显示、运行本软件。但用户不得为商业运营目的安装、使用、运行本软件，不可以对本软件或者本软件运行过程中释放到任何终端设备内存中的数据及本软件运行过程中客户端与服务器端的交互数据进行复制、更改、修改、挂接运行或创作任何衍生作品，形式包括但不限于使用插件、外挂或非经授权的第三方工具/服务接入本软件和相关系统。如果需要进行商业性的销售、复制和散发，例如软件预装和捆绑，必须获得掌中俱舍的书面授权和许可。
6.3 用户不得未经“掌中俱舍”许可，将本软件安装在未经掌中俱舍明示许可的其他终端设备上，包括但不限于机顶盒、游戏机、电视机、DVD 机等。
6.4 用户可以为使用本软件及服务的目的复制本软件的一个副本，仅用作备份。备份副本必须包含原软件中含有的所有著作权信息。
6.5 除非另有约定或我方另行声明，我方平台内的所有内容（用户依法享有版权的内容除外）、技术、软件、程序、数据及其他信息（包括但不限于文字、图像、图片、照片、音频、视频、图表、色彩、版面设计、电子文档）的所有知识产权（包括但不限于版权、商标权、专利权、商业秘密等）及相关权利，均归我方或我方关联的第三方所有。未经我方许可，任何人不得擅自使用（包括但不限于复制、传播、展示、镜像、上载、下载、修改、出租），法律另有规定的情形除外。
6.6 我方平台的 Logo、图形及其组合，以及我方平台的其他标识、徵记、产品和服务名称均为我方或我方关联的第三方在中国或其它国家的商标，未经我方书面授权，任何人不得以任何方式展示、使用或作其他处理，也不得向他人表明您有权展示、使用或作其他处理。
6.7 我方对我方专有内容、原创内容和其他通过授权取得的独占或独家内容享有完全知识产权。未经我方许可，任何单位和个人不得私自转载、传播和提供观看服务或者有其他侵犯我方知识产权的行为，否则将承担所有相关的法律责任。
6.8 您了解并同意，经由本平台向您呈现之广告或推广信息等信息所包含之内容，亦受到著作权、商标权、专利权或其他专属权利之法律保护。未经我方明示授权，您不得修改、出租、出借、出售、散布本服务或软件之任何部分或全部，或据以制作衍生著作，或使用擅自修改后的软件等。我方仅授予您个人、不可移转及非专属之使用权，使您得于在手机等终端使用其软件之目的，您不得（且不得允许任何第三人）复制、修改、改编、翻译、创作衍生著作、进行还原工程、反向组译、传播或以其它方式发现原始码，或出售、转让、再授权或提供软件设定担保，或以其它方式移转软件之任何权利。您同意将通过由我方所提供的界面而非任何其它途径使用本服务。

七、违约责任
7.1 违约认定
7.1.1 发生如下情形之一的，视为您违反约定：
· 使用我方平台服务时违反有关法律法规规定的；
· 违反本协议约定（包括不时修订的本协议约定）的。
7.1.2 为适应互联网行业发展和满足多用户对高效优质服务的需求，您理解并同意，我方可在我方平台规则中约定违约认定的程序和标准。如：我方可依据您的用户数据进行合理判定以确认您是否构成违约；您有义务对您的数据异常现象进行充分举证和合理解释，否则将被认定为违约。
7.2 违约处理措施
7.2.1 您在我方平台上发布的内容和信息构成违约的，我方可根据相应规则立即对相应内容和信息进行删除、屏蔽等处理或对您的账户进行暂停使用、查封、注销等处理。
7.2.2 您在我方平台上实施的行为，或虽未在我方平台上实施但对我方平台及用户产生影响的行为构成违约的，我方可依据相应规则对您的账户执行限制参加活动、中止向您提供部分或全部服务等处理措施。如您的行为构成根本违约的，我方可查封您的账户，终止向您提供服务。

八、服务终止
8.1 终止的情形
您有权通过以下任一方式终止本协议：
· 在满足我方平台公示的账户注销等清理条件时，您自行注销您的账户的；
· 变更事项生效前，您停止使用并明示不愿接受变更事项的；
· 您明示不愿继续使用我方平台服务，且符合我方平台终止条件的。
8.2 出现以下情况时，我方可以随时终止本协议：
· 您违反本协议约定，我方依据违约条款终止本协议的；
· 您转让本人账户、盗用他人账户、发布违禁内容和信息、骗取他人财物、采取不正当手段谋利等行为，我方依据平台规则对您的账户予以查封的；
· 除上述情形外，因您多次违反我方平台规则相关规定且情节严重，我方依据平台规则对您的账户予以查封的；
· 您注册数据中内容是虚假或不存在的，我方有权随时终止向您提供服务；如我方有合理理由怀疑您提供的资料错误、不实、过时或不完整的，有权发出询问及/或要求改正的通知，并有权直接做出删除相应资料的处理，直至中止、终止对您提供部分或全部服务。我方对此不承担任何责任，您将承担因此产生的任何直接或间接损失及不利后果。
· 您的账户被我方依据本协议进行注销等清理的；
· 您在我方平台有侵犯他人合法权益或其他严重违法违约行为的；
· 用户协议更新时，您明示不愿接受新的用户协议的；
· 不可抗力导致本协议不能履行的（“不可抗力”是指双方不可预见、或可以预见但不能避免并或克服的客观情况，该事件妨碍、影响或延误任何一方根据合同履行其全部或部分义务。该事件包括但不限于政府行为、自然灾害、战争、网络堵塞或中断、黑客袭击或任何其他类似事件）；
· 我方发现您连续1个月内注册用户数量超过5个的；
· 您注册账户后，连续180天不使用该账户的；
· 您的言论行为违反第4.1条规则或第4.2条规则的；
· 其它根据条约我方应当终止服务或我方认为需终止服务的情况。

九、协议变更
9.1 我方可根据国家法律法规变化及我方平台服务变化的需要，不时修改本协议或我方平台规则，变更后的协议、平台规则（下称“变更事项”）将通过本协议第十二条约定的方式通知您。
9.2 如您对变更事项不同意的，您应当于变更事项确定的生效之日起停止使用我方平台服务；如您在变更事项生效后仍继续使用我方平台服务，则视为您同意已生效的变更事项。除另行明确声明外，任何使“服务”范围扩大或功能增强的新内容均受本协议约束。

十、通知
您同意我方以以下合理的方式向您送达各类通知：
· 公示的文案；
· 站内消息、弹出消息、客户端推送的消息；
· 向您预留于我方平台的联系方式发出的联系方式，如手机短信、函件等。
"""));
              },
            text: '“用户协议”',
            style: _highProfileStyle,
          ),
          const TextSpan(
            text: '和',
            style: _lowProfileStyle,
          ),
          TextSpan(
            recognizer: TapGestureRecognizer()
              ..onTap = () {
                // 点击打开“隐私政策”
                Get.toNamed(Routes.AGREEMENT,
                    arguments: AgreementParams("隐私政策".tr, """ 
本政策适用于“掌中俱舍”产品及服务。
本政策将帮助您了解以下内容：
· 我们如何收集和使用您的个人信息
· 我们如何保护与保存您的个人信息
· 您的权利
· 本政策如何更新
我们深知个人信息对您的重要性，并会尽全力保护您的个人信息安全可靠。我们致力于维持您对我们的信任，恪守以下原则，保护您的个人信息：权责一致原则、目的明确原则、选择同意原则、最少够用原则、确保安全原则、主体参与原则、公开透明原则等。同时，我们承诺，我们将按业界的安全标准，采取相应的安全保护措施来保护您的个人信息。
【特别提示】请您在使用我们的产品及服务前，仔细阅读并了解本《隐私政策》并作出相应选择。一旦您于线上点击同意本政策，或者继续使用我们的产品及服务，即意味着您同意我们按照本隐私政策处理您的相关信息。如您对本隐私政策有任何疑问，您可以通过本隐私政策“如何联系我们”中提过的联系方式与我们联系。
一、我们如何收集并使用您的个人信息
在您使用我们的产品或服务时，您需要/可以选择授权我们收集和使用个人信息的场景如下:
1.为向您提供我们产品/服务的基本功能，您需要授权我们收集、使用必要的信息的情形。如您拒绝提供前述必要信息，您将无法正常使用我们的产品/服务；
2.为向您提供我们产品/服务的附加功能，您可选择授权我们收集、使用信息的情形。如您拒绝提供前述信息，您将无法正常使用相关附加功能或无法达到我们拟达到的功能效果，但并不会影响您正常使用我们产品及/服务的基本功能。
我们会遵循正当、合法、必要的原则，为向您提供下述的各项功能，出于本政策所述的下列目的收集和使用您的个人信息：
(1) 用户注册
当您注册成为“掌中俱舍”的用户时，需要您提供：手机号码及密码用于创建账户。若您通过微信等第三方账号登录，我们会收集您的昵称、头像、所在城市等公开信息，收集该部分信息前，将通过第三方账号的授权功能获得您的授权。请注意，若您拒绝提供上述个人信息，我们将无法为您完成用户注册，您也将无法使用该软件的任何功能。
若您注册用户时，希望向我们上传您的图片，以自定义头像，您可以授权我们调用您所使用的设备的摄像头权限和相册权限（或读取及写入外部存储器权限）。
(2) 参加“掌中俱舍”的排位赛及小班功能，使用组队背单词功能
当您选择参加“掌中俱舍”的排位赛或加入小班时，我们将向其他用户展示您的头像、昵称及单词学习情况，当您选择使用加“掌中俱舍”的组队背单词功能时，为复制外部口令，我们可能读取您的剪切板内容，若您不愿意将上述信息进行展示、读取，请勿使用排位赛、小班及组队背单词功能。
(3) 为您提供用户服务
当您与我们的客服联系时，我们可能会需要您提供必要的信息以核验身份并收集您的联系方式以便在问题得出解决方案后与您及时联络，您与客服的沟通记录及内容也将被保存。
(4) 为保障用户安全及开展用户服务改进计划
为了持续提供安全且不断优化您需要的服务，在您使用我们提供的服务时，我们可能会收集您使用服务的相关信息，这类信息包括：
· 在您使用我们服务或访问我们平台网页时，我们自动接收并记录的您的浏览器和计算机上的信息，包括但不限于您的 IP 地址、浏览器的类型、使用的语言、访问日期和时间、软硬件特征信息及您需求的网页记录等数据；如您下载或使用我们或其关联的第三方客户端软件，或访问移动网页使用我们平台服务时，我们可能会读取与您位置和移动设备相关的信息，包括但不限于设备型号、设备平台、设备厂商、设备品牌、操作系统、分辨率、电信运营商以及网络信息（如IP地址、WIFI信息、基站信息等相关信息）等；
· 日志信息，指您使用我们的服务时，系统可能通过 Cookies 或其他方式自动采集的技术信息，来制定您的国家及使用语言等；
· 在使用我们服务时搜索或浏览的信息，例如您使用的网页搜索词语、访问的社交媒体页面url地址，以及您在使用我们服务时浏览或要求提供的其他信息和内容详情；
· 为了使您持续享受最新的服务，我们可能会使用您应用的版本信息、手机型号用于为您提供应用的升级服务。同时，我们可能收集您已连接的智能设备列表和版本号信息，用于为您提供智能设备的升级功能，以确保您可以使用最新版本的服务（包括固件版本）。
同时为了收集上述基本的个人设备信息，我们将会申请访问您的设备信息的权限，我们收集这些信息是为了向您提供我们基本服务和基础功能，如您拒绝提供上述权限将可能导致您无法使用我们的产品与服务。
(5) 事先征得同意的例外
您充分理解并同意，我们在以下情况下收集、使用您的个人信息无需征得您的授权同意：
· 为应对突发公共卫生事件，或者紧急情况下为保护自然人的生命健康和财产安全所必需；
· 为公共利益实施新闻报道、舆论监督等行为，在合理的范围内处理个人信息；
· 依照《中华人民共和国个人信息保护法》规定在合理的范围内处理个人自行公开或者其他已经合法公开的个人信息；
(6) 如我们停止运营“掌中俱舍”产品或服务，我们将及时停止继续收集您个人信息的活动，将停止运营的通知以逐一送达或公告的形式通知您，并对我们所持有的与已关停业务相关的个人信息进行删除或匿名化处理。

二、我们如何保护与保存您的个人信息
我们已使用符合业界标准的安全防护措施保护您提供的个人信息，防止数据遭到未经授权访问、公开披露、使用、修改、损坏或丢失。我们会采取一切合理可行的措施，保护您的个人信息。您的个人信息将全部被存储于中华人民共和国境内。目前我们不存在向境外提供个人信息的场景。
互联网环境并非百分之百安全，我们将尽力确保或担保您发送给我们的任何信息的安全性。您知悉并理解，您接入我们的服务所用的系统和通讯网络，有可能因我们可控范围外的因素而出现问题。因此，您应当采取积极措施保护个人信息的安全。我们强烈建议您不要将在产品中的个人信息向第三方（包括与本产品链接的其它网站或产品）透露，以免造成信息的泄露；请使用复杂密码且不得将自己的账号密码及相关个人信息透露给他人，协助我们保证您的账号安全。

三、您的权利
按照中国相关的法律、法规、标准，以及其他国家、地区的通行做法，我们保障您对自己的个人信息行使以下权利：
(1) 访问您的个人信息
您有权访问您的个人信息，法律法规规定的例外情况除外。如果您想行使数据访问权，可以通过以下方式自行访问：
· 账户信息——如果您希望访问或编辑您的账户中的个人资料信息和支付信息、更改您的密码、添加安全信息或关闭您的账户等，您可以通过我们平台及/或登录相关客户端执行此类操作。
· 搜索信息——您可以登录账号访问或清除您的搜索历史记录。
如果您无法通过上述方式访问这些个人信息，您可联系邮箱：15110268019@163.com。
我们将在十五个工作日内回复您的访问请求。
对于您在使用我们的产品或服务过程中产生的其他个人信息，只要我们不需要过多投入，我们会向您提供。
(2) 更正您的个人信息
当您发现我们处理的关于您的个人信息有错误时，您有权要求我们作出更正。您可以通过“(1) 访问您的个人信息”中罗列的方式提出更正申请。
如果您无法通过上述方式更正这些个人信息，您可以联系邮箱：15110268019@163.com
我们将在十五个工作日内回复您的更正请求。
(3) 删除您的个人信息
在以下情形中，您可以向我们提出删除个人信息的请求：
· 如果我们处理个人信息的行为违反法律法规；
· 如果我们收集、使用您的个人信息，却未征得您的同意；
· 如果我们处理个人信息的行为违反了与您的约定；
· 如果您不再使用我们的产品或服务，或您注销了账号；
· 如果我们不再为您提供产品或服务。
当您决定删除某项或某几项信息时，您可以联系邮箱：15110268019@163.com
我们将在十五个工作日内回复。
当您从我们的服务中删除信息后，我们可能不会立即从备份系统中删除相应的信息，但会在备份更新时删除这些信息。
(4) 改变您授权同意的范围
每个业务功能需要一些基本的个人信息才能得以完成。对于额外收集的个人信息的收集和使用，您可以通过联系团队，随时给予或撤回您的授权同意。
当您撤回同意后，我们将不再处理相应的个人信息。但您撤同意的决定，不会影响此前基于您的授权而开展的个人信息处理。
(5) 个人信息主体注销账户
您随时可注销此前注册的账户，我们将在收到您的注销请求后15个工作日内完成注销。
请注意，为确保账户安全，我们会对您的身份做必要的验证。在注销账户之后，我们将停止为您提供产品或服务。
(8) 响应您的上述请求
为保障安全，您可能需要提供书面请求，或以其他方式证明您的身份。我们可能会先要求您验证自己的身份，然后再处理您的请求。
我们将在十五个工作日内作出答复。对于那些无端重复、需要过多技术手段（例如，需要开发新系统或从根本上改变现行惯例）、给他人合法权益带来风险或者非常不切实际（例如，涉及备份磁带上存放的信息）的请求，我们可能会予以拒绝。
在以下情形中，按照法律法规的要求，我们将无法响应您的上述请求：
· 与个人信息控制者履行法律法规规定的义务相关的；
· 与国家安全、国防安全直接相关的；
· 与公共安全、公共卫生、重大公共利益直接相关的；
· 与犯罪侦查、起诉、审判和执行判决等直接相关的；
· 个人信息控制者有充分证据表明个人信息主体存在主观恶意或滥用权利的；
· 出于维护个人信息主体或其他个人的生命、财产等重大合法权益但又很难得到本人同意的；
· 响应个人信息主体的请求将导致个人信息主体或其他个人、组织的合法权益受到严重损害的；

四、本政策如何更新
我们可能不时对本隐私政策进行变更。
未经您明确同意，我们不会削减您按照本隐私政策所应享有的权利。我们会在本页面上发布对本政策所做的任何变更。
对于重大变更，我们还会提供更为显著的通知。
本政策所指的重大变更包括但不限于：
· 我们的服务模式发生重大变化。如处理个人信息的目的、处理的个人信息类型、个人信息的使用方式等；
· 我们在所有权结构、组织架构等方面发生重大变化。如业务调整、破产并购等引起的所有者变更等；
· 个人信息共享、转让或公开披露的主要对象发生变化；
· 您参与个人信息处理方面的权利及其行使方式发生重大变化；
· 我们负责处理个人信息安全的责任人员、联络方式及投诉渠道发生变化时；
· 个人信息安全影响评估报告表明存在高风险时。
"""));
              },
            text: '“隐私政策”',
            style: _highProfileStyle,
          ),
        ],
      ),
    );
  }
}

class VerificationCodeButton extends StatelessWidget {
  final String text;
  final int? seconds;
  final VoidCallback onPressed;
  const VerificationCodeButton({
    super.key,
    required this.text, required this.onPressed, this.seconds,
  });

  @override
  Widget build(BuildContext context) {
    final controller = Get.put(CountdownController());

    return Obx(() => OutlinedButton(
          onPressed: controller.disabled.value
              ? null
              : () async {
                  controller.start();
            await getVerifyCode('17608471349');
                  var dio = Dio();
                  // var response = await dio.post(
                  //     'https://zzjs.api.tulips-tree.top/api/user/verifycode',
                  //     data: {
                  //       'username': '17608471349',
                  //     }
                  // );
                  // print('123');
                  var esp = await dio.post<Map>(
                    "https://zzjs.api.tulips-tree.top/api/user/verifycode",
                    data: {
                      'username': '17608471349',
                    },
                    options: Options(headers: {
                      "Content-Type": "application/json",
                      "ApiVer": "1.2.1",
                      "Accept": "application/json",
                    }),
                  );
                  print('esp:$esp');
                  // print('response:$response');
                  // var a = await getUserInfo();
                  // print('a:$a');

                  // await registerByPassword('17608471349', '123456', '123456');
                },
          style: OutlinedButton.styleFrom(
              fixedSize: const Size(120, 20),
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
              backgroundColor: Colors.white,
              foregroundColor: const Color(0xFFC7A360),
              side: const BorderSide(width: 1, color: Color(0xFFC7A360))),
          child:
              Text(controller.disabled.value ? '${controller.seconds}' : text),
        ));
  }
}

class CountdownController extends GetxController {
  final disabled = false.obs;
  final seconds = 0.obs;
  late Timer timer;

  void start({int? seconds}) {
    disabled.value = true;
    this.seconds.value = seconds??60;

    timer = Timer.periodic(const Duration(seconds: 1), (_) {
      if (this.seconds.value > 0) {
        this.seconds.value--;
      } else {
        disabled.value = false;
        timer.cancel();
      }
    });
  }
}
