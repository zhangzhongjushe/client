import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/routes/params.dart';

class AgreementPage extends StatelessWidget {
  const AgreementPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.AGREEMENT,
    );

    AgreementParams data = Get.arguments;
    //拆分文本并去除空字符串
    List<String> txt = data.content.split("\n");
    txt.removeWhere((element) => element.isEmpty);
    return Scaffold(
      appBar: AppBar(
        title: Text(data.title),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(24),
          child: ListView.builder(
              shrinkWrap: true,
              itemCount: txt.length,
              itemBuilder: (BuildContext context, int index) {
                if (RegExp(r"^[一二三四五六七八九十]")
                    .hasMatch(txt[index].substring(0, 1))) {
                  return Text.rich(TextSpan(children: [
                    TextSpan(
                        text: txt[index],
                        style: const TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w700)),
                    const WidgetSpan(
                        child: SizedBox(
                      width: double.infinity,
                      height: 10,
                    ))
                  ]));
                } else {
                  return Text.rich(TextSpan(children: [
                    const WidgetSpan(
                        child: SizedBox(
                      width: 24,
                    )),
                    TextSpan(text: txt[index]),
                    const WidgetSpan(
                        child: SizedBox(
                      width: double.infinity,
                      height: 5,
                    )),
                  ]));
                }
              }),
        ),
      ),
    );
  }
}
