import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:shimmer/shimmer.dart';
import 'package:zzjs_client/api/gatha.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/entities/gatha.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';

/// 偈颂详情页
class GathaDetailPage extends StatefulWidget {
  const GathaDetailPage({super.key});

  static TextStyle txtLeftTopSty =
      const TextStyle(color: Color(0xE6d2d2d2), fontSize: 16, fontWeight: FontWeight.w500, fontStyle: FontStyle.normal);

  static TextStyle txtMidTopSty =
      const TextStyle(color: Color(0xE6d2d2d2), fontSize: 24, fontWeight: FontWeight.w500, fontStyle: FontStyle.italic);
  static TextStyle txtLeftSty = const TextStyle(
    color: Color(0xE6d2d2d2),
    fontSize: 18,
    fontWeight: FontWeight.w500,
    // fontStyle: FontStyle.italic
  );
  static TextStyle txtMidSty = const TextStyle(
    color: Color(0xE6000000),
    fontSize: 24,
    fontWeight: FontWeight.w500,
  );

  @override
  State<GathaDetailPage> createState() => _GathaDetailPageState();
}

class _GathaDetailPageState extends State<GathaDetailPage> {
  final player = AudioPlayer();

  PlayerState? _playerState;

  Duration? _totalDuration;

  Duration? _currentPosition;

  bool _audioLoading = false;

  int _showAudio = -1;

  StreamSubscription? _durationSubscription;

  StreamSubscription? _positionSubscription;

  StreamSubscription? _playerCompleteSubscription;

  StreamSubscription? _playerStateChangeSubscription;

  bool get _isPlaying => _playerState == PlayerState.playing;

  bool get _isPaused => _playerState == PlayerState.paused;

  String get _durationText => _totalDuration?.toString().split('.').first ?? '';

  String get _positionText => _currentPosition?.toString().split('.').first ?? '';

  String get _remainingTime => _currentPosition != null && _totalDuration != null
      ? _formatDuration(_totalDuration! - _currentPosition!)
      : '00:00';

  /// 图表地址
  int _showExp = -1;
  int _showChart = -1;

  @override
  void initState() {
    super.initState();
    // Use initial values from player
    _playerState = player.state;
    player.getDuration().then(
          (value) => setState(() {
            _currentPosition = value;
          }),
        );
    player.getCurrentPosition().then(
          (value) => setState(() {
            _currentPosition = value;
          }),
        );
    _initStreams();
  }

  @override
  void dispose() {
    _durationSubscription?.cancel();
    _positionSubscription?.cancel();
    _playerCompleteSubscription?.cancel();
    _playerStateChangeSubscription?.cancel();
    _stop();
    super.dispose();
  }

  void _initStreams() {
    _durationSubscription = player.onDurationChanged.listen((duration) {
      setState(() => _totalDuration = duration);
    });

    _positionSubscription = player.onPositionChanged.listen(
      (p) => setState(() => _currentPosition = p),
    );

    // _playerCompleteSubscription = player.onPlayerComplete.listen((event) {
    //   setState(() {
    //     _playerState = PlayerState.stopped;
    //     _currentPosition = Duration.zero;
    //   });
    // });

    _playerStateChangeSubscription = player.onPlayerStateChanged.listen((state) {
      setState(() {
        _playerState = state;
      });
    });
  }

  Future<void> _play() async {
    await player.resume();
    setState(() => _playerState = PlayerState.playing);
  }

  Future<void> _pause() async {
    await player.pause();
    setState(() => _playerState = PlayerState.paused);
  }

  Future<void> _stop() async {
    await player.stop();
    setState(() {
      _playerState = PlayerState.stopped;
      _currentPosition = Duration.zero;
    });
  }

  Future<String?> getChart(GathaModel gatha, int index) async {
    GathaDetails? gd = await getGathaDetails(gatha.id);
    if (gd!.graphUrl.isNotEmpty) {
      return gd.graphUrl;
    }
    return null;
  }

  Future<void> getAudioAndPlay(GathaModel gatha, int index) async {
    setState(() {
      _audioLoading = true;
    });
    GathaDetails? gd = await getGathaDetails(gatha.id);

    final String? path = await getAudio(gatha, gd!.gathaVersion, gd.audioUrl);

    if (path != gatha.audioUrl) {
      gatha.audioUrl = path;
      gatha.graphUrl = gd.graphUrl;
      gatha.gathaVersion = gd.gathaVersion;
      await GlobalService.to.isar.writeTxn(() async {
        await GlobalService.to.isar.gathaModels.put(gatha);
      });
    }

    await player.setSource(DeviceFileSource(path!));

    await player.play(DeviceFileSource(path));

    setState(() {
      _audioLoading = false;
      _playerState = PlayerState.playing;
      _showAudio = index;
    });
  }

  void toggleExp(index) {
    setState(() {
      if (_showExp == index) {
        _showExp = -1;
      } else {
        _showExp = index;
      }
      _showChart = -1;
      // if (_showExp != -1) {
      //    // 如果显示文字，则隐藏图片
      // }
    });
  }

  void toggleChart(index) {
    setState(() {
      if (_showChart == index) {
        _showChart = -1;
      } else {
        _showChart = index;
      }
      _showExp = -1; // 如果显示图片，则隐藏文字
      // if (_showChart != -1) {
      //
      // }
    });
  }

  String _formatDuration(Duration duration) {
    int minutes = duration.inMinutes.remainder(60);
    int seconds = duration.inSeconds.remainder(60);
    return '$minutes:${seconds.toString().padLeft(2, '0')}';
  }

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.GATHA_DETAIL,
    );
    var reciteListController = ReciteProgressController.to.currentController;
    // 获取路由传参并初始化状态
    reciteListController.initPageViewController();
    return Scaffold(
      backgroundColor: const Color(0xfff5f3fb),
      appBar: AppBar(
          title: Obx(() => Text(
                reciteListController.chapterTitle.value,
                style: const TextStyle(fontWeight: FontWeight.bold),
              )),
          backgroundColor: const Color(0xfff5f3fb),
          foregroundColor: const Color(0xff15141b)),
      body: Stack(
        children: [
          // 中间背诵内容展示
          SizedBox(
            child: PageView.builder(
              controller: reciteListController.reciteCardController,
              itemCount: reciteListController.reciteGathaList.length,
              itemBuilder: (context, index) {
                GathaModel gatha = reciteListController.getGathaData(index);

                return Container(
                  margin: const EdgeInsets.only(
                    left: 8,
                    right: 8,
                    top: 8,
                    bottom: 120,
                  ),
                  alignment: Alignment.topCenter,
                  //边框设置
                  decoration: BoxDecoration(
                    //背景
                    color: Colors.white,
                    //设置四周圆角 角度
                    borderRadius: const BorderRadius.all(Radius.circular(24.0)),
                    //设置四周边框
                    border: Border.all(width: 1, color: Colors.white),
                    boxShadow: const [
                      BoxShadow(
                        color: Colors.black12, // 阴影的颜色
                        offset: Offset(10, 20), // 阴影与容器的距离
                        blurRadius: 15.0, // 高斯的标准偏差与盒子的形状卷积。
                        spreadRadius: 0.0, // 在应用模糊之前，框应该膨胀的量。
                      ),
                    ],
                  ),
                  child: SingleChildScrollView(
                    child: Column(
                      children: [
                        // 今日背诵进度，不限定今日背诵篇目，完成背诵速度规定数量即可
                        Obx(() => Container(
                              padding: const EdgeInsets.fromLTRB(10, 10, 0, 0),
                              width: double.infinity,
                              child: Text(
                                "今日任务x".trParams({
                                  "finished": GlobalService.to.data.value.finishedRecitesToday.toString(),
                                  "speed": GlobalService.to.data.value.speed.toString(),
                                }),
                                style: GathaDetailPage.txtLeftTopSty,
                                textAlign: TextAlign.left,
                              ),
                            )),
                        // 背诵卡片序号
                        Text(gatha.name, style: GathaDetailPage.txtMidTopSty),
                        // 背诵内容
                        Text(
                          gatha.content.replaceAll("\\r\\n", "\n"),
                          // "迦湿弥罗议理成 我多依彼释对法\n少有贬量为我失 判法正理在牟尼\n大师世眼久已闭 堪为证者多散灭\n不见真理无制人 由鄙寻思乱圣教\n自觉已归胜寂静 持彼教者多随灭\n世无依恬丧众德 无钩制惑随意转\n既知如来正法寿 渐次沦亡如至喉\n是诸烦恼力增时 应求解脱勿放逸",
                          style: GathaDetailPage.txtMidSty,
                        ),
                        _audioLoading
                            ? Shimmer.fromColors(
                                baseColor: Colors.grey.shade300,
                                highlightColor: Colors.grey.shade100,
                                child: Container(
                                  width: MediaQuery.of(context).size.width * 0.8,
                                  height: 50,
                                  margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                  decoration: BoxDecoration(
                                      color: const Color(0XFF5F7292), borderRadius: BorderRadius.circular(4)),
                                ),
                              )
                            : _showAudio == index
                                ? Container(
                                    margin: const EdgeInsets.fromLTRB(20, 20, 20, 0),
                                    decoration: BoxDecoration(
                                        color: const Color(0XFF5F7292), borderRadius: BorderRadius.circular(4)),
                                    child: Row(
                                      children: [
                                        _isPlaying
                                            ? IconButton(
                                                onPressed: _isPlaying ? _pause : null,
                                                icon: const Icon(
                                                  Icons.pause,
                                                  color: Colors.white,
                                                ),
                                              )
                                            : IconButton(
                                                onPressed: _isPlaying ? null : _play,
                                                icon: const Icon(Icons.play_arrow, color: Colors.white)),
                                        SizedBox(
                                          width: MediaQuery.of(context).size.width * 0.53,
                                          child: SliderTheme(
                                            data: const SliderThemeData(
                                                disabledThumbColor: Colors.white10,
                                                thumbColor: Colors.white,
                                                activeTrackColor: Colors.white,
                                                inactiveTrackColor: Colors.white10,
                                                thumbShape: RoundSliderThumbShape(enabledThumbRadius: 8)),
                                            child: Slider(
                                              onChanged: (value) {
                                                final duration = _totalDuration;
                                                if (duration == null) {
                                                  return;
                                                }
                                                final position = value * duration.inMilliseconds;
                                                player.seek(Duration(milliseconds: position.round()));
                                              },
                                              value: (_currentPosition != null &&
                                                      _totalDuration != null &&
                                                      _currentPosition!.inMilliseconds > 0 &&
                                                      _currentPosition!.inMilliseconds < _totalDuration!.inMilliseconds)
                                                  ? _currentPosition!.inMilliseconds / _totalDuration!.inMilliseconds
                                                  : 0.0,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          _currentPosition != null
                                              // ? '$_positionText / $_durationText'
                                              ? _remainingTime
                                              : _totalDuration != null
                                                  ? _durationText
                                                  : '',
                                          style: const TextStyle(fontSize: 16.0, color: Colors.white10),
                                        ),
                                      ],
                                    ),
                                  )
                                : GestureDetector(
                                    onTap: () async {
                                      getAudioAndPlay(gatha, index);
                                    },
                                    child: Container(
                                      padding: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                                      child: const Row(
                                        children: [
                                          Text(
                                            '原声',
                                            style: TextStyle(
                                                fontSize: 16, color: Color(0xFF97A3B7), fontWeight: FontWeight.w600),
                                          ),
                                          Icon(
                                            Icons.volume_mute_rounded,
                                            color: Color(0xFF97A3B7),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),

                        Container(
                          padding: const EdgeInsets.fromLTRB(20, 20, 0, 0),
                          child: Row(
                            children: [
                              OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                    minimumSize: const Size(60, 35), // 设置按钮的最小尺寸
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20), // 设置圆角度
                                    ),
                                    side: const BorderSide(color: Color(0XFFC49C50)), // 设置边框颜色
                                  ),
                                  onPressed: () {
                                    toggleExp(index);
                                    Posthog().capture(
                                      eventName: '点击显示白话解',
                                      properties: {'currentGathaId': reciteListController.getCurrentGathaData().id},
                                    );
                                  },
                                  child: const Text(
                                    '白话解',
                                    style: TextStyle(color: Color(0XFFC49C50)),
                                  )),
                              const SizedBox(
                                width: 10,
                              ),
                              OutlinedButton(
                                  style: OutlinedButton.styleFrom(
                                    minimumSize: const Size(60, 35), // 设置按钮的最小尺寸
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(20), // 设置圆角度
                                    ),
                                    side: const BorderSide(color: Color(0XFFC49C50)), // 设置边框颜色
                                  ),
                                  onPressed: () {
                                    toggleChart(index);
                                    Posthog().capture(
                                      eventName: '点击显示图表',
                                      properties: {'currentGathaId': reciteListController.getCurrentGathaData().id},
                                    );
                                  },
                                  child: const Text(
                                    '图表',
                                    style: TextStyle(color: Color(0XFFC49C50)),
                                  )),
                            ],
                          ),
                        ),
                        if (_showExp == index)
                          Column(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    width: Get.width - 16 - 42 - 3,
                                    padding: const EdgeInsets.symmetric(vertical: 16, horizontal: 20),
                                    child: Text(
                                      gatha.explanation.replaceAll("\\r\\n", "\n\n"),
                                      textAlign: TextAlign.start,
                                      softWrap: true,
                                      style: const TextStyle(fontSize: 14, color: Colors.black54),
                                    ),
                                  ),
                                  const Spacer()
                                ],
                              ),
                            ],
                          ),
                        if (_showChart == index)
                          Center(
                            child: FutureBuilder<String?>(
                              future: getChart(gatha, index),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState == ConnectionState.waiting) {
                                  return const CircularProgressIndicator(
                                    color: Color(0XFFC49C50),
                                  );
                                } else if (snapshot.hasError) {
                                  return Text('Error: ${snapshot.error}');
                                } else if (snapshot.data != null) {
                                  return Image.network(snapshot.data!);
                                } else {
                                  return const Padding(
                                    padding: EdgeInsets.only(top: 50.0),
                                    child: Text('该段落没有图表'),
                                  );
                                }
                              },
                            ),
                          )
                      ],
                    ),
                  ),
                );
              },
            ),
          ),
          // 底部操作栏
          Positioned(
            left: 0,
            right: 0,
            bottom: 40,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // 上一个
                  Obx(() => IconButton(
                        style: IconButton.styleFrom(
                            elevation: 5, shadowColor: Colors.black87, backgroundColor: Colors.white),
                        onPressed: reciteListController.getCurrentGathaData().prev != null
                            ? () {
                                Posthog().capture(
                                  eventName: '点击切换上一个偈颂',
                                  properties: {
                                    'previous': reciteListController.getCurrentGathaData().id,
                                    'current': reciteListController
                                        .getGathaData(reciteListController.currentIndex.value - 1)
                                        .id,
                                  },
                                );
                                reciteListController.toPreviousGatha();
                              }
                            : null,
                        icon: const Icon(
                          Icons.skip_previous_outlined,
                        ),
                      )),
                  // 不熟
                  Obx(() => SizedBox(
                        height: 48,
                        width: 112,
                        child: TextButton(
                            style: TextButton.styleFrom(
                              disabledBackgroundColor: Colors.black26,
                              disabledForegroundColor: Colors.white70,
                              backgroundColor: const Color(0XFF5F7292),
                              foregroundColor: Colors.white,
                            ),
                            onPressed: reciteListController.getCurrentGathaData().progress.value!.status ==
                                    ReciteStatus.forget
                                ? null
                                : () async {
                                    Posthog().capture(
                                      eventName: '点击切换背诵状态',
                                      properties: {
                                        'previous': reciteStatusTitleMap[
                                                reciteListController.getCurrentGathaData().progress.value!.status] ??
                                            '',
                                        'current': reciteStatusTitleMap[ReciteStatus.forget] ?? '',
                                      },
                                    );
                                    await reciteListController.onUpdate(ReciteStatus.forget);
                                  },
                            child: Text("不熟".tr)),
                      )),
                  // 已背熟
                  Obx(() => SizedBox(
                        height: 48,
                        width: 112,
                        child: TextButton(
                            style: TextButton.styleFrom(
                                disabledBackgroundColor: Colors.black26,
                                disabledForegroundColor: Colors.white70,
                                foregroundColor: Colors.white,
                                backgroundColor: const Color(0XFFC49C50)),
                            onPressed: reciteListController.getCurrentGathaData().progress.value!.status ==
                                    ReciteStatus.memorized
                                ? null
                                : () async {
                                    Posthog().capture(
                                      eventName: '点击切换背诵状态',
                                      properties: {
                                        'previous': reciteStatusTitleMap[
                                                reciteListController.getCurrentGathaData().progress.value!.status] ??
                                            '',
                                        'current': reciteStatusTitleMap[ReciteStatus.memorized] ?? '',
                                      },
                                    );
                                    await reciteListController.onUpdate(ReciteStatus.memorized);
                                  },
                            child: Text("已背熟".tr)),
                      )),
                  // 下一个
                  Obx(() => IconButton(
                        style: IconButton.styleFrom(
                            elevation: 5, shadowColor: Colors.black87, backgroundColor: Colors.white),
                        onPressed: reciteListController.getCurrentGathaData().next != null
                            ? () {
                                Posthog().capture(
                                  eventName: '点击切换下一个偈颂',
                                  properties: {
                                    'previous': reciteListController.getCurrentGathaData().id,
                                    'current': reciteListController
                                        .getGathaData(reciteListController.currentIndex.value + 1)
                                        .id,
                                  },
                                );
                                reciteListController.toNextGatha();
                              }
                            : null,
                        icon: const Icon(Icons.skip_next_outlined),
                      )),
                ],
              ),
            ),
          ),
          // 底部操作栏
          kDebugMode
              ? Positioned(
                  left: 0,
                  right: 0,
                  bottom: 200,
                  child: ElevatedButton(
                    onPressed: () {
                      Get.toNamed(Routes.DAILY_FINISHED);
                    },
                    child: const Text("调试模式：跳转到今日进度完成页"),
                  ),
                )
              : const SizedBox(),
        ],
      ),
    );
  }
}
