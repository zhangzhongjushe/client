import 'package:flutter/material.dart';
import 'package:zzjs_client/components/under_line_tabbar_indicator.dart';

class HearingListPage extends StatefulWidget {
  const HearingListPage({super.key});

  @override
  State<HearingListPage> createState() => _HearingListPageState();
}

class _HearingListPageState extends State<HearingListPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('听闻'), actions: [
        IconButton(
          icon: const Icon(
            Icons.search,
            size: 20,
          ),
          onPressed: () {},
        ),
      ]),
      body: _buildListTab(),
    );
  }

  Widget _buildListTab() {
    return Container(
      color: const Color(0xffF1F1F1),
      child: Column(
        children: [
          Expanded(
            child: DefaultTabController(
                length: 3,
                child: Column(
                  children: [
                    Container(
                        width: MediaQuery.of(context).size.width,
                        color: Colors.white,
                        child: const TabBar(
                          labelColor: Colors.black,
                          unselectedLabelColor: Color(0xFF666666),
                          labelStyle: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.w600),
                          unselectedLabelStyle: TextStyle(
                              fontSize: 14, fontWeight: FontWeight.normal),
                          isScrollable: false,
                          indicator: ZZJSUnderlineTabIndicator(
                              indicatorBottom: -10,
                              indicatorWidth: 40,
                              borderSide: BorderSide(
                                  width: 3, color: Color(0xFF1989FA))),
                          tabs: [
                            Text("音频栏目1"),
                            Text("音频栏目2"),
                            Text("音频栏目3"),
                          ],
                        )),
                    Expanded(
                      child: TabBarView(children: [
                        // 下面所对应的页面都是可以自定义要显示的内容的Widget，实现已省略
                        _buildTabWidget(),
                        _buildTabWidget(),
                        _buildTabWidget(),
                      ]),
                    ),
                  ],
                )),
          ),
        ],
      ),
    );
  }

  _buildTabWidget() {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3, // 横向列数
        crossAxisSpacing: 16.0, // 列之间的间距
        mainAxisSpacing: 16.0, // 行之间的间距
      ),
      itemCount: 8,
      itemBuilder: (BuildContext context, int index) {
        return const Center(child: Text('image'));
      },
    );
  }
}
