import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/controllers/demo.dart';
import 'package:zzjs_client/routes/pages.dart';

const double _kItemExtent = 32.0;
const List<String> _fruitNames = <String>[
  'Apple',
  'Mango',
  'Banana',
  'Orange',
  'Pineapple',
  'Strawberry',
];

class DemoPage extends StatelessWidget {
  const DemoPage({super.key});

  // This shows a CupertinoModalPopup with a reasonable fixed height which hosts CupertinoPicker.
  void _showDialog(Widget child, BuildContext context) {
    showCupertinoModalPopup<void>(
        context: context,
        builder: (BuildContext context) => Container(
              height: 216,
              padding: const EdgeInsets.only(top: 6.0),
              // The Bottom margin is provided to align the popup above the system navigation bar.
              margin: EdgeInsets.only(
                bottom: MediaQuery.of(context).viewInsets.bottom,
              ),
              // Provide a background color for the popup.
              color: CupertinoColors.systemBackground.resolveFrom(context),
              // Use a SafeArea widget to avoid system overlaps.
              child: SafeArea(
                top: false,
                child: child,
              ),
            ));
  }

// This shows a CupertinoModalPopup which hosts a CupertinoAlertDialog.
  void _showAlertDialog(BuildContext context) {
    showCupertinoModalPopup<void>(
      context: context,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: const Text('Alert'),
        content: const Text('Proceed with destructive action?'),
        actions: <CupertinoDialogAction>[
          CupertinoDialogAction(
            /// This parameter indicates this action is the default,
            /// and turns the action's text to bold text.
            isDefaultAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('No'),
          ),
          CupertinoDialogAction(
            /// This parameter indicates the action would perform
            /// a destructive action such as deletion, and turns
            /// the action's text color to red.
            isDestructiveAction: true,
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Yes'),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(context) {
    Posthog().screen(
      screenName: Routes.DEMO,
    );
    Get.put(DemoController());

    return Scaffold(
        // Use Obx(()=> to update Text() whenever count is changed.
        appBar: AppBar(
          title: Obx(() => Text("Clicks: ${DemoController.to.current.total}")),
        ),

        // Replace the 8 lines Navigator.push by a simple Get.to(). You don't need context
        body: Center(
            child: Column(
          children: [
            ElevatedButton(
              child: const Text("Go to Other"),
              onPressed: () => Get.to(() => const Other()),
            ),
            ElevatedButton(
              child: const Text("show AlertDialog"),
              onPressed: () async {
                _showAlertDialog(context);
              },
            ),
            ElevatedButton(
              child: const Text("pageView"),
              onPressed: () async {
                Get.to(const PageViewPage());
              },
            ),
            ElevatedButton(
              child: const Text("show CupertinoPicker"),
              onPressed: () async {
                _showDialog(
                    CupertinoPicker(
                      magnification: 1.22,
                      squeeze: 1.2,
                      useMagnifier: true,
                      itemExtent: _kItemExtent,
                      // This is called when selected item is changed.
                      onSelectedItemChanged: (int selectedItem) {},
                      children: List<Widget>.generate(_fruitNames.length,
                          (int index) {
                        return Center(
                          child: Text(
                            _fruitNames[index],
                          ),
                        );
                      }),
                    ),
                    context);
              },
            ),
            ElevatedButton(
              onPressed: () {
                Get.to(const GetXListViewPage2());
              },
              child: const Text("obx listView"),
            )
          ],
        )),
        floatingActionButton: FloatingActionButton(
            onPressed: () => DemoController.to.increment(),
            child: const Icon(Icons.add)));
  }
}

class Other extends StatelessWidget {
  const Other({super.key});

  // You can ask Get to find a Controller that is being used by another page and redirect you to it.

  @override
  Widget build(context) {
    // Access the updated count variable
    return Scaffold(body: Center(child: Text("${DemoController.to.count}")));
  }
}

class PageViewPage extends StatelessWidget {
  const PageViewPage({super.key});

  static const String _title = 'Flutter Code Sample';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title)),
        body: Column(
          children: [
            const SizedBox(height: 600, child: PageViewWidget()),
            ElevatedButton(onPressed: () {}, child: const Text("data")),
          ],
        ),
      ),
    );
  }
}

class PageViewWidget extends StatelessWidget {
  const PageViewWidget({super.key});

  @override
  Widget build(BuildContext context) {
    final PageController controller = PageController(
      initialPage: 1,
      viewportFraction: .9,
      keepPage: true,
    );

    return PageView.builder(
      controller: controller,
      clipBehavior: Clip.none,
      itemBuilder: (context, index) {
        return Container(
          margin: const EdgeInsets.all(30),
          width: 200,
          height: 600,
          color: Colors.amberAccent[100],
          child: Text('$index Page'),
        );
      },
    );
  }
}

class ListDataX2 extends GetxController {
  RxList<int> numbers = List<int>.from([0, 1, 2, 3]).obs;

  void httpCall() async {
    await Future.delayed(
        const Duration(seconds: 1), () => numbers.add(numbers.last + 1));
    //update();
  }

  void reset() {
    numbers = numbers.sublist(0, 3).obs;
    //update();
  }
}

class GetXListViewPage2 extends StatelessWidget {
  const GetXListViewPage2({super.key});

  @override
  Widget build(BuildContext context) {
    ListDataX2 dx = Get.put(ListDataX2());
    // ignore: avoid_print
    print('Page ** rebuilt');
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            Expanded(
              flex: 8,
              child: Obx(
                () => ListView.builder(
                    itemCount: dx.numbers.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text('Number: ${dx.numbers[index]}'),
                      );
                    }),
              ),
            ),
            Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    ElevatedButton(
                      onPressed: dx.httpCall,
                      child: const Text('Http Request'),
                    ),
                    ElevatedButton(
                      onPressed: dx.reset,
                      child: const Text('Reset'),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
