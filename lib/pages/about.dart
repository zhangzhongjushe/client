import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/routes/pages.dart';

/// 关于我们
class AboutPage extends StatelessWidget {
  const AboutPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.ABOUT,
    );

    return Scaffold(
      appBar: AppBar(title: Text('关于我们'.tr)),
      body: const Center(
        child: Text.rich(TextSpan(children: [
          TextSpan(text: "在这里的，是这样的一群人："),
          TextSpan(text: """

相互触碰的彩色
真诚分享的觉受
成就自他的念想
彼此拉拔的同行
携手增上的共识

"""),
          TextSpan(text: "坚持背诵，终能竟功"),
        ])),
      ),
    );
  }
}
