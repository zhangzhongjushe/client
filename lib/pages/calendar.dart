import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/components/calendar/classes/event.dart';
import 'package:zzjs_client/components/calendar/classes/event_list.dart';
import 'package:zzjs_client/components/calendar/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:zzjs_client/controllers/calendar.dart';
import 'package:zzjs_client/routes/pages.dart';

class CalendarPage extends StatelessWidget {
  const CalendarPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.CALENDAR,
    );

    return GetBuilder<CalendarController>(
      init: CalendarController(),
      builder: (controller) {
        DateTime currentDate = DateTime.now();
        Widget eventIcon = Container(
          decoration: BoxDecoration(
            color: const Color(0xffC49C50),
            borderRadius: BorderRadius.circular(1000),
          ),
          child: const Icon(
            Icons.done_rounded,
            color: Colors.white,
            size: 14,
          ),
        );
        EventList<Event> markedDateMap = EventList<Event>(events: {});
        for (var e in controller.taskTracker) {
          markedDateMap.add(
              DateTime.parse(e['updateAt']),
              Event(
                date: DateTime.parse(e['updateAt']),
                title: '',
                icon: eventIcon,
              ));
        }
        return Scaffold(
          appBar: AppBar(
            title: const Text('打卡日历'),
            centerTitle: true,
          ),
          body: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(
                    top: 30.0,
                    bottom: 16.0,
                    left: 16.0,
                    right: 16.0,
                  ),
                  child: Row(
                    children: <Widget>[
                      Text(
                        DateFormat('yyyy.MM')
                            .format(controller.targetDateTime.value),
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 28.0,
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.symmetric(horizontal: 20),
                        width: 26,
                        height: 26,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(1000),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.1),
                                  offset: const Offset(0, 1),
                                  blurRadius: 2.0,
                                  spreadRadius: 1)
                            ]),
                        child: IconButton(
                          onPressed: () {
                            Posthog().capture(
                              eventName: '切换月份',
                              properties: {
                                'previous': DateTime(
                                    controller.targetDateTime.value.year,
                                    controller.targetDateTime.value.month),
                                'current': DateTime(
                                    controller.targetDateTime.value.year,
                                    controller.targetDateTime.value.month - 1),
                              },
                            );
                            controller.getGatha(DateTime(
                                controller.targetDateTime.value.year,
                                controller.targetDateTime.value.month - 1));
                          },
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(24, 24),
                              maximumSize: const Size(24, 24),
                              padding: const EdgeInsets.only(left: 3),
                              shape: const CircleBorder(),
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black),
                          icon: const Icon(
                            Icons.arrow_back_ios,
                            size: 12,
                          ),
                          // padding: EdgeInsets.all(16),
                        ),
                      ),
                      Container(
                        width: 26,
                        height: 26,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(1000),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black.withOpacity(0.1),
                                  offset: const Offset(0, 1),
                                  blurRadius: 2.0,
                                  spreadRadius: 1)
                            ]),
                        child: IconButton(
                          onPressed: () async {
                            Posthog().capture(
                              eventName: '切换月份',
                              properties: {
                                'previous': DateTime(
                                    controller.targetDateTime.value.year,
                                    controller.targetDateTime.value.month),
                                'current': DateTime(
                                    controller.targetDateTime.value.year,
                                    controller.targetDateTime.value.month + 1),
                              },
                            );
                            controller.getGatha(DateTime(
                                controller.targetDateTime.value.year,
                                controller.targetDateTime.value.month + 1));
                          },
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(24, 24),
                              maximumSize: const Size(24, 24),
                              shape: const CircleBorder(),
                              padding: const EdgeInsets.only(left: 2),
                              backgroundColor: Colors.white,
                              foregroundColor: Colors.black),
                          icon: const Icon(
                            Icons.arrow_forward_ios,
                            size: 12,
                          ),
                          // padding: EdgeInsets.all(16),
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16.0),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.all(Radius.circular(12)),
                      boxShadow: [
                        BoxShadow(
                            color: Colors.black.withOpacity(0.1),
                            offset: const Offset(0, 6),
                            blurRadius: 3.0,
                            spreadRadius: 1),
                      ]),
                  child: Listener(
                    onPointerMove: (event) => controller.isMove.value = true,
                    child: CalendarCarousel<Event>(
                      targetDateTime: controller.targetDateTime.value,
                      onDayPressed: (date, events) {
                        Posthog().capture(
                          eventName: '切换日期',
                          properties: {
                            'current': date,
                          },
                        );
                        controller.isMove.value = false;
                        controller.getGatha(date);
                      },
                      markedDateMoreCustomDecoration: const BoxDecoration(
                        color: Color(0x33C49C50),
                      ),
                      markedDateShowIcon: false,
                      markedDateIconBuilder: (event) {
                        return Container(
                            margin: EdgeInsets.zero,
                            padding: EdgeInsets.zero,
                            decoration: BoxDecoration(
                                // color: Color(0x33C49C50),
                                borderRadius: BorderRadius.circular(100)),
                            height: 50,
                            width: 37,
                            child: Stack(
                              clipBehavior: Clip.none,
                              children: [
                                Positioned(
                                  bottom: -5,
                                  right: -3,
                                  child: Container(
                                    padding: const EdgeInsets.all(1),
                                    decoration: BoxDecoration(
                                      color: const Color(0xffC49C50),
                                      borderRadius: BorderRadius.circular(10),
                                      border: Border.all(
                                          color: Colors.white,
                                          width: event.date.year ==
                                                      currentDate.year &&
                                                  event.date.month ==
                                                      currentDate.month &&
                                                  event.date.day ==
                                                      currentDate.day
                                              ? 2
                                              : 0),
                                    ),
                                    child: const Icon(
                                      Icons.done_rounded,
                                      color: Colors.white,
                                      size: 10,
                                    ),
                                  ),
                                )
                              ],
                            ));
                      },
                      markedDateCustomShapeBorder: const CircleBorder(
                          side:
                              BorderSide(width: 20, color: Color(0x33C49C50))),
                      weekdayTextStyle: const TextStyle(color: Colors.black),
                      markedDateIconMaxShown: 1,
                      markedDateIconOffset: 10,
                      dayPadding: 2,
                      selectedDayTextStyle: const TextStyle(
                        color: Colors.white,
                      ),
                      // todayButtonColor: const Color(0xFFC49C50),
                      // todayBorderColor: Colors.transparent,
                      selectedDayBorderColor: const Color(0xFFC49C50),
                      selectedDayButtonColor: const Color(0xFFC49C50),
                      daysHaveCircularBorder: true,
                      showOnlyCurrentMonthDate: false,
                      weekendTextStyle: const TextStyle(
                        color: Colors.black,
                      ),
                      weekFormat: false,
                      markedDatesMap: markedDateMap,
                      height: 270.0,
                      selectedDateTime: controller.targetDateTime.value,
                      customGridViewPhysics:
                          const NeverScrollableScrollPhysics(),
                      markedDateCustomTextStyle: const TextStyle(
                        fontWeight: FontWeight.w800,
                        color: Colors.black,
                      ),
                      showHeader: false,
                      todayTextStyle: TextStyle(
                        color: controller.targetDateTime.value == DateTime.now()
                            ? Colors.white
                            : Colors.black,
                      ),
                      minSelectedDate:
                          currentDate.subtract(const Duration(days: 360)),
                      maxSelectedDate:
                          currentDate.add(const Duration(days: 360)),
                      prevDaysTextStyle: const TextStyle(
                        fontSize: 16,
                        color: Colors.black12,
                      ),
                      nextDaysTextStyle: const TextStyle(color: Colors.black12),
                      inactiveDaysTextStyle:
                          const TextStyle(color: Colors.red, fontSize: 16),
                      onCalendarChanged: (DateTime date) {
                        if (controller.isMove.value) {
                          controller.isMove.value = false;
                          controller.getGatha(date);
                        }
                      },
                      onDayLongPressed: (DateTime date) {},
                    ),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 10),
                  child: Text(
                    DateFormat('M月dd日').format(controller.targetDateTime.value),
                    style: const TextStyle(
                        fontWeight: FontWeight.w700, fontSize: 20),
                  ),
                ),
                Container(
                  width: double.infinity,
                  height: 70,
                  padding: const EdgeInsets.fromLTRB(30, 33, 0, 0),
                  margin: const EdgeInsets.symmetric(
                    horizontal: 20.0,
                  ),
                  decoration: BoxDecoration(
                      image: DecorationImage(
                          fit: BoxFit.contain,
                          image: AssetImage(controller.isFinished.value
                              ? "images/calendar/finished.png"
                              : "images/calendar/unfinished.png"))),
                  child: Text(
                    controller.isFinished.value ? "背诵计划 已完成" : "背诵计划 未完成",
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: controller.isFinished.value
                            ? const Color(0xFFD7BC88)
                            : const Color(0xFF7F8799)),
                  ),
                ),
                Container(
                    width: double.infinity,
                    height: MediaQuery.of(context).size.height / 3,
                    padding: EdgeInsets.only(
                      top: controller.isFinished.value ? 0 : 50,
                    ),
                    child: controller.isFinished.value
                        ? ListView.builder(
                            itemBuilder: (BuildContext c, int index) {
                              return Container(
                                margin: const EdgeInsets.only(
                                    left: 15, right: 15, top: 10),
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 16.0),
                                alignment: const Alignment(0, 0),
                                decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(8.0)),
                                  boxShadow: const [
                                    BoxShadow(
                                      color: Colors.black12,
                                      offset: Offset(0, 0),
                                      blurRadius: 2.0,
                                      spreadRadius: 0.0,
                                    ),
                                  ],
                                  border:
                                      Border.all(width: 1, color: Colors.white),
                                ),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      controller.selectedDayGatha[index]
                                          ['name'],
                                      style: const TextStyle(
                                          color: Color(0xE6d2d2d2),
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                          fontStyle: FontStyle.italic),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                    Expanded(
                                        child: Column(
                                      children: [
                                        Text(
                                          controller.selectedDayGatha[index]
                                                  ['content']
                                              .split("\\r\\n")[0]
                                              .trim(),
                                          style: const TextStyle(
                                            color: Color(0xE6000000),
                                            fontSize: 22,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                        Text(
                                          controller.selectedDayGatha[index]
                                                  ['content']
                                              .split("\\r\\n")[1]
                                              .trim(),
                                          style: const TextStyle(
                                            color: Color(0xE6000000),
                                            fontSize: 22,
                                            fontWeight: FontWeight.w500,
                                          ),
                                          overflow: TextOverflow.ellipsis,
                                        ),
                                      ],
                                    )),
                                    Padding(
                                      padding: const EdgeInsets.only(top: 8),
                                      child: Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: const [
                                          Text('已背熟',
                                              style: TextStyle(
                                                color: Color(0xffC49C50),
                                                fontSize: 12,
                                                fontWeight: FontWeight.w500,
                                              )),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                            itemCount: controller.selectedDayGatha.length,
                          )
                        : Column(
                            children: const [
                              Text(
                                '无背诵记录',
                                style: TextStyle(color: Color(0XFFAEB3C1)),
                              ),
                              SizedBox(
                                height: 16,
                              ),
                              Image(
                                  width: 100,
                                  image: AssetImage(
                                      "images/calendar/unfinished-icon.png")),
                            ],
                          )) //
              ],
            ),
          ),
        );
      },
    );
  }
}
