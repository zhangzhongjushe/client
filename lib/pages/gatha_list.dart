import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/components/gatha_card.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/controllers/recite_sliver_list.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';

/// 偈颂列表页
class GathaListPage extends StatelessWidget {
  const GathaListPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.GATHA_LIST,
    );

    Get.put(ReciteListTabController());
    String title = "书籍名称错误".tr;

    title = ReciteProgressController.to.bookName;

    return Scaffold(
      backgroundColor: const Color(0XffF3F4FC),
      body: NestedScrollView(
        floatHeaderSlivers: true,
        headerSliverBuilder: (context, innerBoxIsScrolled) {
          return <Widget>[
            // 列表头部
            SliverAppBar(
              title: Text(title),
              backgroundColor: const Color(0XFFF3F4FC),
              centerTitle: true,
              floating: true,
              pinned: true,
              forceElevated: innerBoxIsScrolled,
            ),

            // 可拖拽隐藏头部
            SliverOverlapAbsorber(
              handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
              sliver: SliverPersistentHeader(
                delegate: _GathaListHeaderDelegate(),
                floating: true,
                pinned: true,
              ),
            ),
          ];
        },
        body: TabBarView(
          controller: ReciteListTabController.to.tabController,
          children: const [
            TabViewSliverList(ReciteStatus.all),
            TabViewSliverList(ReciteStatus.memorized),
            TabViewSliverList(ReciteStatus.forget),
            TabViewSliverList(ReciteStatus.none),
          ],
        ),
      ),
    );
  }
}

/// 自定义Tab bar选中样式
class CustomTabIndicator extends Decoration {
  /// Create an underline style selected tab indicator.
  ///
  /// The [borderSide] and [insets] arguments must not be null.
  const CustomTabIndicator(
      {this.borderSide = const BorderSide(width: 2.0, color: Colors.white),
      this.insets = EdgeInsets.zero,
      required this.width,
      required this.radius,
      required this.color});

  /// The color and weight of the horizontal line drawn below the selected tab.
  final BorderSide borderSide;

  final double radius;

  final double width;

  final Color color;

  /// Locates the selected tab's underline relative to the tab's boundary.
  ///
  /// The [TabBar.indicatorSize] property can be used to define the
  /// tab indicator's bounds in terms of its (centered) tab widget with
  /// [TabIndicatorSize.label], or the entire tab with [TabIndicatorSize.tab].
  final EdgeInsetsGeometry insets;

  @override
  _UnderlinePainter createBoxPainter([VoidCallback? onChanged]) {
    return _UnderlinePainter(this, onChanged!);
  }
}

class _UnderlinePainter extends BoxPainter {
  _UnderlinePainter(this.decoration, VoidCallback onChanged) : super(onChanged);

  final CustomTabIndicator decoration;

  BorderSide get borderSide => decoration.borderSide;
  EdgeInsetsGeometry get insets => decoration.insets;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size!;
    final RRect rRect =
        RRect.fromRectAndRadius(rect, Radius.circular(decoration.radius));
    canvas.drawRRect(
        rRect,
        Paint()
          ..style = PaintingStyle.fill
          ..color = decoration.color);
  }
}

/// 偈颂列表页头部设置
class _GathaListHeaderDelegate extends SliverPersistentHeaderDelegate {
  final double _minExtent = 20;
  final double _maxExtent = 60;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Container(
      height: 60,
      decoration: const BoxDecoration(
        color: Color(0XffF3F4FC),
        // backgroundBlendMode: BlendMode.luminosity,
      ),
      child: TabBar(
          // isScrollable: true,
          indicator: const CustomTabIndicator(
              width: 10, radius: 8, color: Color(0xFFAA6E1E)),
          indicatorSize: TabBarIndicatorSize.label,
          indicatorWeight: 0,
          labelStyle: const TextStyle(fontSize: 14, color: Colors.white),
          labelColor: Colors.white,
          // labelStyle: const TextStyle(
          //     fontSize: 15,
          //     color: Color(0xFFC49C50),
          //     fontWeight: FontWeight.w700),
          labelPadding: const EdgeInsets.only(left: 8, right: 8),
          unselectedLabelColor: Colors.black87,
          controller: ReciteListTabController.to.tabController,
          // tabs: _tabs,
          tabs: [
            Obx(
              () => Container(
                width: double.infinity,
                // margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    backgroundBlendMode: BlendMode.softLight,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('全部'.tr),
                      Text(
                        ReciteProgressController.to.allGatha.length.toString(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Obx(
              () => Container(
                width: double.infinity,
                // margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    backgroundBlendMode: BlendMode.softLight,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('已背熟'.tr),
                      Text(
                        ReciteProgressController.to.memorizedGatha.length
                            .toString(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Obx(
              () => Container(
                width: double.infinity,
                // margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    backgroundBlendMode: BlendMode.softLight,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('不熟'.tr),
                      Text(
                        ReciteProgressController.to.forgetGatha.length
                            .toString(),
                      )
                    ],
                  ),
                ),
              ),
            ),
            Obx(
              () => Container(
                width: double.infinity,
                // margin: const EdgeInsets.symmetric(vertical: 5),
                decoration: BoxDecoration(
                    backgroundBlendMode: BlendMode.softLight,
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(8)),
                child: SingleChildScrollView(
                  padding: const EdgeInsets.all(12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text('未背'.tr),
                      Text(
                        ReciteProgressController.to.noneGatha.length.toString(),
                      )
                    ],
                  ),
                ),
              ),
            )
          ]),
    );
  }

  //SliverPersistentHeader最大高度
  @override
  double get maxExtent => _maxExtent;

  //SliverPersistentHeader最小高度
  @override
  double get minExtent => _minExtent;

  @override
  bool shouldRebuild(covariant _GathaListHeaderDelegate oldDelegate) {
    return false;
  }
}

/// 偈颂列表TabView
class TabViewSliverList extends StatelessWidget {
  final ReciteStatus status;

  const TabViewSliverList(this.status, {super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Builder(
        // This Builder is needed to provide a BuildContext that is
        // "inside" the NestedScrollView, so that
        // sliverOverlapAbsorberHandleFor() can find the
        // NestedScrollView.
        builder: (BuildContext context) {
          return GetBuilder<ReciteSliverListController>(
            // 将所有计划载入内存，加速读取
            // 根据tab初始化不同内容
            // id 起到隔离controller的作用
            // TODO: 待确认tag的作用，不加也会出现状态冲突的问题
            // tag是指定唯一controller，方便区分不同controller，但是id也能够起到隔离作用，待确认具体实现方案
            id: status.toString(),
            assignId: true,
            tag: status.toString(),
            init: ReciteSliverListController(),
            builder: (controller) {
              // 初次进入页面默认显示全部tab
              // 将偈颂列表添加到控制器
              controller.reciteGathaList
                  .addAll(ReciteProgressController.to.gathaListMap[status]!);

              if (controller.reciteGathaList.isNotEmpty) {
                controller.sortGathaList();
              }

              // 设置当前status对应的列表controller
              ReciteProgressController.to.setController(status, controller);

              // 获取嵌套列表ScrollController
              var pScroll = PrimaryScrollController.of(context);
              controller.reciteListController = pScroll;

              // 监听滚动
              // ignore: invalid_use_of_protected_member
              pScroll.hasListeners
                  ? ""
                  : pScroll.addListener(() {
                      if (pScroll.position.pixels ==
                          pScroll.position.maxScrollExtent) {
                        // TODO：提示 没有更多内容
                      }
                    });

              // 需等页面渲染完成后再跳转
              // FIXME：等待用户手离开屏幕，且切换动画播放完成后跳转，即当时tab位移小数部分为0
              Future.delayed(const Duration(milliseconds: 500), () {
                controller.lastIndex.value < 5 || status != ReciteStatus.all
                    ? pScroll.jumpTo(
                        -1,
                        // curve: Curves.easeInOut,
                        // duration: const Duration(seconds: 1),
                      )
                    : pScroll.animateTo(
                        controller.lastIndex.value * 100 - 300,
                        curve: Curves.easeInOut,
                        duration: const Duration(seconds: 1),
                      );
              });
              return GathaListScrollView(status: status, c: controller);
            },
          );
        },
      ),
    );
  }
}

class GathaListScrollView extends StatelessWidget {
  final ReciteStatus status;
  final ReciteSliverListController c;

  const GathaListScrollView({super.key, required this.status, required this.c});

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return CustomScrollView(
        // The "controller" and "primary" members should be left
        // unset, so that the NestedScrollView can control this
        // inner scroll view.
        // If the "controller" property is set, then this scroll
        // view will not be associated with the NestedScrollView.
        // The PageStorageKey should be unique to this ScrollView;
        // it allows the list to remember its scroll position when
        // the tab view is not on the screen.
        key: PageStorageKey<String>(status.toString()),
        physics: const ScrollPhysics(),
        slivers: <Widget>[
          SliverOverlapInjector(
            // This is the flip side of the SliverOverlapAbsorber
            // above.
            handle: NestedScrollView.sliverOverlapAbsorberHandleFor(context),
          ),
          SliverPadding(
            padding: const EdgeInsets.fromLTRB(4, 4, 4, 0),
            sliver: SliverFixedExtentList(
              itemExtent: 100.0,
              delegate: SliverChildBuilderDelegate(
                childCount: c.reciteGathaList.length,
                addRepaintBoundaries: false,
                (context, index) {
                  if (c.reciteGathaList.isEmpty) {
                    // 加载时的占位符
                    return const SizedBox();
                  }

                  GathaModel gathaItem;
                  try {
                    gathaItem = c.reciteGathaList[index]!;
                  } catch (e) {
                    return null;
                  }
                  if (index == c.lastIndex.value) {
                    // 上次背诵的偈颂突出显示
                    return GathaCard(
                      current: index == c.lastIndex.value,
                      index: index,
                      gathaItem: gathaItem,
                      onSelected: ((_) {
                        c.currentIndex.value = index;
                        Get.toNamed(Routes.GATHA_DETAIL);
                      }),
                    );
                  }

                  bool isLastRecite = index == c.lastIndex.value;
                  return GathaCard(
                    current: isLastRecite,
                    index: index,
                    gathaItem: gathaItem,
                    onSelected: ((_) {
                      print(
                          "ReciteListTabController.to.getCurrentTab(): ${ReciteListTabController.to.getCurrentTab()}");
                      Posthog().capture(
                        eventName: '点击进入背诵详情',
                        properties: {
                          'gathaId': gathaItem.id,
                          'isLastRecite': isLastRecite,
                          // 'currentTab':
                          //     ReciteListTabController.to.getCurrentTab(),
                        },
                      );
                      Get.toNamed(Routes.GATHA_DETAIL);
                    }),
                  );
                },
              ),
            ),
          ),
          const SliverToBoxAdapter(
              child: SizedBox(
            child: Text(
              "已经到底啦",
              style: TextStyle(color: Colors.black26),
              textAlign: TextAlign.center,
            ),
          )),
        ],
      );
    });
  }
}
