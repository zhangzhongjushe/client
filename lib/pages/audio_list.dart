import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:get/get.dart';

import '../controllers/audio_list.dart';

class AudioListPage extends GetView<AudioListController> {
  AudioListPage({super.key});

  AudioListController controller = Get.put<AudioListController>(AudioListController());

  @override
  Widget build(BuildContext context) {
    return Obx(() {
      return DefaultTabController(
        length: controller.titleChannelList.length,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('听闻'),
            actions: [
              IconButton(onPressed: () => {}, icon: const Icon(Icons.search)),
            ],
            bottom:
            TabBar(
              labelColor: Colors.blue,
              unselectedLabelColor: Colors.black87,
              indicatorColor: Colors.blue,
              indicator: const UnderlineTabIndicator(
                  borderSide: BorderSide(width: 2, color: Colors.blue), //高度和颜色
                  insets: EdgeInsets.only(left: 42, right: 42) //左右间距反向设置长度
              ),
              controller: controller.tabController,
              tabs: getTitleList(),
              onTap: (value) => {},
            ),
          ),
          body: TabBarView(
            controller: controller.tabController,
            children: getTabBarView(),
          ),
        ),
      );
    });
  }

  ///channl list
  List<Widget> getTitleList() {
    List<Widget> titleList = [];

    print('why:controller.titleStrList.length:${controller.titleChannelList.length}');
    for (int i = 0; i < controller.titleChannelList.length; i++) {
      titleList.add(Tab(
        text: controller.titleChannelList[i].audioChannelName,
      ));
    }
    return titleList;
  }

  ///tab页下的内容
  List<Widget> getTabBarView() {
    List<Widget> tabBarViewList = [];
    for (int i = 0; i < controller.mListDate.length; i++) {
      print('why:controller.mListDate.length:${controller.mListDate.length}');
      tabBarViewList.add(Center(child:  GridView.builder(
            itemCount: controller.mListDate[i].length,
            padding: const EdgeInsets.all(8),
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              crossAxisSpacing: 8,
              mainAxisSpacing: 8,
              childAspectRatio: 0.73,
            ),
            // gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
            //   maxCrossAxisExtent: 150,
            //   childAspectRatio: 0.6,
            // ),
            itemBuilder: (context, index) {
              return controller.getGridItem(i, index);
            }),
     ));
    }

    return tabBarViewList;
  }
}
