import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:getwidget/components/carousel/gf_carousel.dart';
import 'package:timeago/timeago.dart' as timeago;
import 'package:zzjs_client/api/book.dart';
import 'package:zzjs_client/api/news.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/config.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/controllers/recite_sliver_list.dart';
import 'package:zzjs_client/entities/book.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/global.dart';

/// 首页
class HomePage extends GetView<ReciteProgressController> {
  const HomePage({super.key});

  @override
  Widget build(BuildContext context) {
    Get.put(ReciteProgressController());
    Get.put(ReciteListTabController());
    FocusNode searchFocus = FocusNode();

    return controller.obx(
      (_) => Scaffold(
        backgroundColor: const Color(0xFFF7F4EC),
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          scrolledUnderElevation: 0.0,
          backgroundColor: const Color(0xFFF7F4EC),
          title: Container(
            decoration: const BoxDecoration(
              color: Color(0xFFF7F4EC),
            ),
            child: Row(
              children: [
                Expanded(
                  child: TextField(
                    maxLines: 2,
                    minLines: 1,
                    autofocus: false,
                    focusNode: searchFocus,
                    onTap: () {
                      searchFocus.unfocus();
                      showSearch(
                          context: context, delegate: HomeSearchDelegate());
                    },
                    decoration: InputDecoration(
                      prefixIcon: const Icon(
                        Icons.search,
                        color: Colors.grey,
                      ),
                      filled: true,
                      fillColor: Colors.black.withOpacity(0.05),
                      contentPadding: const EdgeInsets.only(left: 40.0),
                      hintText: '搜索预设文案',
                      hintStyle:
                          TextStyle(color: Colors.black.withOpacity(0.4)),
                      border:
                          const OutlineInputBorder(borderSide: BorderSide.none),
                    ),
                  ),
                ),
                const SizedBox(
                  width: 20,
                ),
                GestureDetector(
                    onTap: () {
                      Get.toNamed(Routes.SETTING);
                    },
                    child: SvgPicture.asset('images/placeholder/setting.svg')),
              ],
            ),
          ),
        ),
        body: SingleChildScrollView(
          child: Obx(() => BodyContent(
                restRecitesToday: GlobalService.to.todayRestGatha,
                speed: GlobalService.to.data.value.speed,
                restDays: GlobalService.to.data.value.restDays,
                totalRecites: GlobalService.to.data.value.totalRecites,
                finishedRecites:
                    GlobalService.to.data.value.finishedRecitesTotal,
              )),
        ),
      ),
      // onLoading: const CircularProgressIndicator(),
    );
  }
}

// 首页Body部分
class BodyContent extends StatelessWidget {
  /// 详情部分
  const BodyContent({
    Key? key,
    required this.restRecitesToday,
    required this.speed,
    required this.restDays,
    required this.totalRecites,
    required this.finishedRecites,
  }) : super(key: key);

  final int restRecitesToday;
  final int speed;
  final int restDays;
  final int totalRecites;
  final int finishedRecites;

  static const List<String> imageList = [
    "images/carousel/banner4@3x.png",
    "images/carousel/banner5@3x.png",
    "images/carousel/banner6@3x.png",
    "images/carousel/banner7@3x.png",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      color: const Color(0xFFF7F4EC),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Column(
        children: [
          // 背景轮播图
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 0),
            child: GFCarousel(
              // enlargeMainPage: true,
              // 图片比例
              aspectRatio: 1125 / 660,
              viewportFraction: 1.0,
              autoPlay: true,
              items: imageList
                  .map((url) => Container(
                        padding: const EdgeInsets.symmetric(vertical: 16),
                        child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(8.0)),
                            child: Image(
                              fit: BoxFit.fill,
                              image: AssetImage(url),
                            )),
                      ))
                  .toList(),
              onPageChanged: (index) {},
            ),
          ),
          // 通知
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SvgPicture.asset(
                "images/placeholder/notice.svg",
              ),
              const SizedBox(
                width: 6,
              ),
              const Text(
                "请继续坚持背诵哦...",
                style: TextStyle(color: Color(0x6616181A)),
              ),
            ],
          ),
          // 进度展示卡片
          Container(
            margin: const EdgeInsets.symmetric(vertical: 30),
            child: TaskCard(
              restRecitesToday,
              speed,
              restDays,
              totalRecites,
              finishedRecites,
            ),
          ),
          // 模块入口
          Container(
            color: const Color(0xFFF7F4EC),
            child: const SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SubModules(
                    route: Routes.CALENDAR,
                    svgPath: 'images/background/calendarModule.svg',
                    title: '打卡日历',
                    buttonText: '立即打卡',
                  ),
                  SubModules(
                    route: Routes.PLAN,
                    svgPath: 'images/background/planModule.svg',
                    title: '计划管理',
                    buttonText: '查看计划',
                  ),
                  SubModules(
                    route: Routes.AUDIO_LIST,
                    svgPath: 'images/background/hearingModule.svg',
                    title: '听闻',
                    buttonText: '查看',
                  ),
                ],
              ),
            ),
          ),
          // 官方贴入口
          Config.isShowNews ?
          FutureBuilder(
              future: getNewsList(1, 10),
              builder: (ctx, snap) {
                if (snap.connectionState == ConnectionState.done) {
                  if (snap.hasError) {
                    // 请求失败，显示错误
                    if (kDebugMode) {
                      print(snap.error);
                    }
                    return Text("Error: ${snap.error}");
                  } else {
                    // 请求成功，显示数据
                    // return Text("Contents: ${snap.data}");
                    return NewsCard(
                      thumb: snap.data![0].thumb,
                      title: snap.data![0].title,
                      typeName: snap.data![0].typeName,
                      timestamp: snap.data![0].date,
                    );
                    // return ListView.builder(
                    //   shrinkWrap: true,
                    //   physics: const NeverScrollableScrollPhysics(),
                    //   itemCount: snap.data?.length,
                    //   itemBuilder: (context, index) {
                    //     // 获取当前的 NewsItems 对象
                    //     NewsItem? item = snap.data?[index];
                    //     // 构建 NewsCard 组件
                    //     return NewsCard(
                    //       thumb: item!.thumb,
                    //       title: item.title,
                    //       typeName: item.typeName,
                    //       timestamp: item.date,
                    //     );
                    //   },
                    // );
                  }
                } else {
                  // 请求未结束，显示loading
                  return const CircularProgressIndicator();
                }
              }) : const SizedBox(),
          // 以下代码只在开发时展示，方便组件调试用，无需删除
          kDebugMode
              ? Container(
                  // padding: const EdgeInsets.all(10),
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    onPressed: () => Get.toNamed(Routes.DEMO),
                    child: Text("Demo".tr),
                  ),
                )
              : Container(),
          kDebugMode ? Text(Config.baseUrl) : Container(),
        ],
      ),
    );
  }
}

/// 模块组件

class SubModules extends StatelessWidget {
  final String route;
  final String svgPath;
  final String title;
  final String buttonText;

  const SubModules(
      {super.key,
      required this.route,
      required this.svgPath,
      required this.title,
      required this.buttonText});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;

    return GestureDetector(
      onTap: () {
        Get.toNamed(route);
      },
      child: Container(
        width: screenWidth * 0.29,
        height: screenWidth * 0.32,
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 12),
        margin: const EdgeInsets.fromLTRB(0, 30, 10, 0),
        decoration: BoxDecoration(
            gradient: const LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [
                Color(0XFFF3E6CB),
                Color(0XFFEEDBB9),
              ],
            ),
            borderRadius: BorderRadius.circular(8)),
        child: Stack(
          clipBehavior: Clip.none,
          alignment: AlignmentDirectional.center,
          children: [
            Positioned(
              top: -40,
              // child: Image(width: 60, image: AssetImage(iconPath)),
              child: SvgPicture.asset(
                svgPath,
                fit: BoxFit.cover,
              ),
            ),
            Positioned(
                top: 40,
                child: Column(
                  children: [
                    Text(
                      title.tr,
                      style: const TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Color(0xFF1F1D1A),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Container(
                      width: screenWidth * 0.2,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 10, vertical: 4),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(16)),
                      child: Text(
                        buttonText,
                        textAlign: TextAlign.center,
                        style: const TextStyle(color: Color(0XFFEA8841)),
                      ),
                    )
                  ],
                ))
          ],
        ),
      ),
    );
  }
}

/// 进度展示卡片
class TaskCard extends StatelessWidget {
  final int restRecitesToday;

  final int daily;

  final int restDays;

  final int finishedRecites;

  final int totalRecites;

  /// 进度展示卡片
  const TaskCard(this.restRecitesToday, this.daily, this.restDays,
      this.totalRecites, this.finishedRecites,
      {super.key});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      clipBehavior: Clip.none,
      child: Container(
        height: 248,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          color: Colors.white,
        ),
        padding: const EdgeInsets.all(24),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            Positioned(
                child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '今日需背x偈'.trParams({
                    "times": restRecitesToday.toString(),
                  }),
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    color: Color(0xE6000000),
                  ),
                ),
                const SizedBox(
                  height: 8,
                ),
                const Text(
                  '勤学如春起之苗，不见其增，日有所长',
                  style: TextStyle(fontSize: 12, color: Color(0XFF978E80)),
                ),

                // 进度条
                Container(
                  margin: const EdgeInsets.only(
                    top: 30,
                    bottom: 8,
                  ),
                  child: Stack(
                    children: [
                      Container(
                        height: 8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(4),
                          color: const Color(0x1AC49C50),
                        ),
                      ),
                      Container(
                        height: 8,
                        width: finishedRecites /
                            totalRecites *
                            Get.mediaQuery.size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4),
                            gradient: const LinearGradient(colors: [
                              Color(0xFFC49C50),
                              Color(0xFFE4CB88),
                            ])),
                      ),
                    ],
                  ),
                ),
                // 进度文字提示
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    RichText(
                        text: TextSpan(children: [
                      const TextSpan(
                        text: '每天',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0x66000000),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextSpan(
                        text: daily.toString(),
                        style: const TextStyle(
                          fontSize: 12,
                          color: Color(0XFFBF9458),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      const TextSpan(
                        text: '偈颂，剩余',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0x66000000),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextSpan(
                        text: restDays.toString(),
                        style: const TextStyle(
                          fontSize: 12,
                          color: Color(0XFFBF9458),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      const TextSpan(
                        text: '天',
                        style: TextStyle(
                          fontSize: 12,
                          color: Color(0x66000000),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                    ])),
                    RichText(
                        text: TextSpan(children: [
                      TextSpan(
                        text: finishedRecites.toString(),
                        style: const TextStyle(
                          fontSize: 12,
                          color: Color(0XFFBF9458),
                          fontWeight: FontWeight.w400,
                        ),
                      ),
                      TextSpan(
                        text: "/$totalRecites",
                        style: const TextStyle(
                          fontSize: 12,
                          color: Color(0x66000000),
                          fontWeight: FontWeight.w400,
                        ),
                      )
                    ])),
                  ],
                ),
                // 开始按钮
                Container(
                  margin: const EdgeInsets.only(top: 32),
                  padding: const EdgeInsets.all(0),
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        fixedSize: const Size(340, 48),
                        backgroundColor: const Color(0xFFC49C50)),
                    onPressed: () => Get.toNamed(Routes.GATHA_LIST),
                    child: Text(
                      "开始背偈颂".tr,
                      style: const TextStyle(
                        color: Colors.white,
                        fontSize: 16,
                      ),
                    ),
                  ),
                ),
              ],
            )),
            const Positioned(
                right: 0,
                top: -40,
                child: Image(
                    width: 90,
                    image: AssetImage('images/placeholder/calendar@3x.png')))
          ],
        ),
      ),
    );
  }
}

class HomeSearchDelegate extends SearchDelegate {
  final reciteSliverListController = Get.put(ReciteSliverListController());
  Future<List<BookItem>>? _searchResults;
  String? _lastQuery;

  @override
  ThemeData appBarTheme(BuildContext context) {
    return Theme.of(context).copyWith(
      primaryColor: const Color(0xF2000000),
      scaffoldBackgroundColor: const Color(0XFFF2F3FB),
      inputDecorationTheme: const InputDecorationTheme(
        border: InputBorder.none,
      ),
    );
  }

  @override
  void showResults(BuildContext context) {
    if (query.isNotEmpty && _lastQuery != query) {
      _lastQuery = query;
      _searchResults = _loadData(query); // 在用户点击搜索按钮时执行搜索
    }
    super.showResults(context);
  }

  @override
  Widget buildResults(BuildContext context) {
    // 根据搜索结果构建页面
    // if (query.isEmpty || _lastQuery == query) {
    //   return Container();
    // }

    return _buildResults();
  }

  Widget _buildResults() {
    return FutureBuilder(
        future: _searchResults,
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            // 显示加载指示器
            return const Center(
                child: CircularProgressIndicator(
              color: Color(0xFFC49C50),
            ));
          } else if (snapshot.hasError) {
            // 显示错误信息
            return Text('Error: ${snapshot.error}');
          } else {
            Get.put(ReciteListTabController());
            return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    if (kDebugMode) {
                      print(snapshot.data[index]);
                      print(int.parse(snapshot.data[index].name));
                    }
                    reciteSliverListController.currentIndex.value =
                        int.parse(snapshot.data[index].name);

                    Get.toNamed(Routes.GATHA_DETAIL);
                  },
                  child: Container(
                    margin: const EdgeInsets.only(left: 15, right: 15, top: 10),
                    //设置 child 居中
                    alignment: const Alignment(0, 0),
                    //边框设置
                    decoration: BoxDecoration(
                      //背景
                      color: Colors.white,
                      //设置四周圆角 角度
                      borderRadius:
                          const BorderRadius.all(Radius.circular(8.0)),
                      //阴影
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black12, // 阴影的颜色
                          offset: Offset(0, 0), // 阴影与容器的距离
                          blurRadius: 2.0, // 高斯的标准偏差与盒子的形状卷积。
                          spreadRadius: 0.0, // 在应用模糊之前，框应该膨胀的量。
                        ),
                      ],
                      //设置四周边框
                      border: Border.all(width: 1, color: Colors.white),
                    ),
                    child: Stack(
                      clipBehavior: Clip.none,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                snapshot.data[index].name,
                                style: const TextStyle(
                                    color: Color(0xE6d2d2d2),
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                    fontStyle: FontStyle.italic),
                                overflow: TextOverflow.ellipsis,
                              ),
                              Expanded(
                                child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      RichText(
                                        text: highlightText(
                                            snapshot.data[index].content
                                                .split("\\r\\n")[0]
                                                .trim(),
                                            query),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                      RichText(
                                        text: highlightText(
                                            snapshot.data[index].content
                                                .split("\\r\\n")[1]
                                                .trim(),
                                            query),
                                        overflow: TextOverflow.ellipsis,
                                      ),
                                    ]),
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                );
              },
            );
          }
        });
  }

  TextSpan highlightText(String text, String query) {
    print(text);
    if (text.isEmpty || query.isEmpty) {
      return TextSpan(text: text);
    }

    final RegExp regex = RegExp(query, caseSensitive: false);
    final Iterable<Match> matches = regex.allMatches(text);

    List<TextSpan> children = [];

    int start = 0;
    for (Match match in matches) {
      final matchStart = match.start;
      final matchEnd = match.end;

      // 添加非匹配部分
      if (matchStart > start) {
        children.add(TextSpan(
            text: text.substring(start, matchStart),
            style: const TextStyle(
              color: Color(0xE6000000),
              fontSize: 22,
              fontWeight: FontWeight.w500,
            )));
      }

      // 添加匹配部分并设置高亮样式
      children.add(TextSpan(
        text: text.substring(matchStart, matchEnd),
        style: const TextStyle(
            fontSize: 22,
            fontWeight: FontWeight.w500,
            color: Colors.blueAccent), // 设置高亮颜色
      ));

      start = matchEnd;
    }

    // 添加剩余的非匹配部分
    if (start < text.length) {
      children.add(TextSpan(
          text: text.substring(start),
          style: const TextStyle(
            color: Color(0xE6000000),
            fontSize: 22,
            fontWeight: FontWeight.w500,
          )));
    }

    return TextSpan(children: children);
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    // 在搜索框右侧显示清除按钮等操作按钮
    return [
      IconButton(
        icon: const Icon(Icons.clear),
        onPressed: () {
          query = "";
          showSuggestions(context);
        },
      ),
      TextButton(
        onPressed: () {
          _loadData;
          close(context, null);
        },
        child: const Text(
          '取消',
          style: TextStyle(color: Colors.blueAccent),
        ),
      )
    ];
  }

  @override
  Widget? buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      icon: const Icon(Icons.search),
      onPressed: () {
        close(context, null);
      },
    );
  }

  Future<List<BookItem>> _loadData(String query) async {
    final globalModel = await GlobalService.to.isar.globalModels.get(1);
    List<BookItem> results =
        await search(globalModel!.currentBookId.toString(), query, 1, 10);
    return results;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return const SizedBox();
  }
}

class NewsCard extends StatelessWidget {
  final String thumb;
  final String title;
  final String typeName;
  final int timestamp;

  const NewsCard(
      {super.key,
      required this.thumb,
      required this.title,
      required this.typeName,
      required this.timestamp});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      margin: const EdgeInsets.symmetric(vertical: 30),
      child: Row(
        children: [
          Image.network(
            thumb,
            width: 160,
          ),
          const SizedBox(
            width: 8,
          ),
          Expanded(
            child: Container(
              // width: MediaQuery.of(context).size.width * 0.5,
              padding: const EdgeInsets.symmetric(vertical: 5),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    title,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: const TextStyle(fontSize: 18),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Text(
                        typeName,
                        style: const TextStyle(color: Colors.black54),
                      ),
                      const SizedBox(
                        width: 20,
                      ),
                      Text(
                        timeago.format(
                            DateTime.fromMillisecondsSinceEpoch(timestamp),
                            locale: 'zh_CN'),
                        style: const TextStyle(color: Colors.black54),
                      )
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
