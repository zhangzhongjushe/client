import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../controllers/audio.dart';

class AudioDetailPage extends GetView<AudioDetailController> {
  AudioDetailPage({super.key});

  AudioDetailController controller = Get.put<AudioDetailController>(
      AudioDetailController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xfff5f3fb),
      appBar: AppBar(
        title: const Text('听闻'),
        centerTitle: true,
      ),
      body: Container(
        margin: const EdgeInsets.only(
          left: 8,
          right: 8,
          top: 8,
          bottom: 8,
        ),
        child: SizedBox(
          height: double.infinity,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(children: [
                Container(
                  width: double.infinity,
                  height: 100,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                ),
                Positioned(
                  top: 10,
                  left: 10,
                  child: Text(controller.entity?.audioName ?? ''),
                ),
                Positioned(
                  left: 10,
                  bottom: 10,
                  child: Obx(() {
                    return Row(
                      children: [
                        controller.isPlaying.value
                            ? IconButton(
                          onPressed: controller.isPlaying.value ? controller.pause : null,
                          icon: const Icon(Icons.pause, color: Colors.blue,),
                        )
                            : IconButton(
                            onPressed: controller.isPlaying.value ? null : controller.play,
                            icon: const Icon(Icons.play_arrow, color: Colors.blue)),
                        SizedBox(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width * 0.53,
                          child: SliderTheme(
                            data: const SliderThemeData(
                                disabledThumbColor: Colors.grey,
                                thumbColor: Colors.white,
                                activeTrackColor: Colors.blue,
                                inactiveTrackColor: Colors.grey,
                                thumbShape: RoundSliderThumbShape(
                                    enabledThumbRadius: 8)),
                            child: Slider(
                              onChanged: (value) {
                                final duration = controller.totalDuration;
                                if (duration == null) {
                                  return;
                                }
                                final position = value * duration.inMilliseconds;
                                controller.audioPlayer.seek(Duration(milliseconds: position.round()));
                              },
                              value: controller.sliderProgress.value,
                            ),
                          ),
                        ),
                        Text(
                          controller.playingTime.value??'--',
                          style: const TextStyle(
                              fontSize: 16.0, color: Colors.black),
                        ),
                        const SizedBox(width: 10,),
                        controller.audioLoading.value == 2?
                        const SizedBox(
                          width: 15,
                          height: 15,
                          child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(Colors.blue),),
                        )
                            :
                            const SizedBox(),
                      ],
                    );
                  }),
                ),
                // Positioned(
                //     right: 20,
                //     bottom: 10,
                //     child: TextButton(
                //       onPressed: () {
                //         controller.playFromStorage();
                //       },
                //       child: Text('播放'),
                //     )),
              ]),
              const SizedBox(
                height: 20,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Text(
                    controller.entity?.audioText ?? '',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
