import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:isar/isar.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/components/chapters_selector.dart';
import 'package:zzjs_client/components/tag_with_data.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_plan.dart';
import 'package:zzjs_client/routes/pages.dart';
import 'package:zzjs_client/schemas/book.dart';

class PlanPage extends GetView<RecitePlanController> {
  /// 计划管理页
  const PlanPage({super.key});

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.PLAN,
    );

    RecitePlanController c =
        Get.put<RecitePlanController>(RecitePlanController());

    return controller.obx(
      (state) => Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Plan Manage'.tr,
              style:
                  const TextStyle(fontWeight: FontWeight.w600, fontSize: 16)),
        ),
        body: Container(
          color: Colors.white,
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: Obx(() => ListView.separated(
              // 添加onGoingPlan用于监控变化
              itemCount: c.allPlans.length + 3 + c.onGoingPlan().id * 0,
              // 分割器构造器
              separatorBuilder: (BuildContext context, int index) {
                return const Divider(color: Colors.white);
              }, // 列表元素构造器
              itemBuilder: (BuildContext context, int index) {
                var onGoingPlan = c.onGoingPlan();
                switch (index) {
                  case 0:
                    return Padding(
                      padding: const EdgeInsets.symmetric(vertical: 20),
                      child: Text("Ongoing Recite Plan".tr,
                          style: const TextStyle(
                              fontWeight: FontWeight.w600, fontSize: 16)),
                    );

                  case 1:
                    return CurrentPlanCard(
                      bookImage: onGoingPlan.bookName,
                      finishedNum: onGoingPlan.finishedCount,
                      name: onGoingPlan.planName,
                      speed: onGoingPlan.speed,
                      total: onGoingPlan.total,
                      onSpeedChange: c.changeReciteSpeed,
                    );
                  case 2:
                    return Text("Existed Recite Range".tr,
                        style: const TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16));
                  default:
                    return RecitePlanCard(
                      bookImage: c.allPlans[index - 3].bookName,
                      finished: c.allPlans[index - 3].isFinished,
                      index: index - 3,
                      name: c.allPlans[index - 3].planName,
                      progress: c.allPlans[index - 3].finishedCount,
                      speed: c.allPlans[index - 3].speed,
                      total: c.allPlans[index - 3].total,
                    );
                }
              })),
        ),
        // 底部新建计划按钮
        persistentFooterButtons: [
          Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 16,
            ),
            width: double.infinity,
            height: 50,
            child: TextButton(
              style: TextButton.styleFrom(
                  foregroundColor: Colors.white,
                  backgroundColor: const Color(0xFFC49C50)),
              onPressed: () async {
                List<ChapterModel> chapters = await GlobalService
                    .to.isar.chapterModels
                    .filter()
                    .book((q) => q.idEqualTo(1))
                    .findAll();
                Map<int, ChapterModel> items = {};

                for (ChapterModel e in chapters) {
                  items[e.id] = e;
                }

                Get.bottomSheet(
                  CreatePlanPicker(items: items),
                  backgroundColor: Colors.white,
                  isScrollControlled: true,
                );
              },
              child: Text('Create New Plan'.tr),
            ),
          ),
        ],
        persistentFooterAlignment: AlignmentDirectional.center,
      ),
      // onLoading: const CircularProgressIndicator(),
    );
  }
}

class BookImage extends StatelessWidget {
  /// 书籍图片展示
  const BookImage({super.key, required this.url});

  final String url;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16),
      width: 80,
      height: 110,
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage(
                  "images/background/book${Random().nextInt(3) + 1}.png"),
              fit: BoxFit.cover),
          color: const Color(0XFFA1CBB7),
          borderRadius: const BorderRadius.all(Radius.circular(8))),
      child: Center(
        child: Text(
          url,
          textAlign: TextAlign.center,
          style: const TextStyle(
            color: Colors.white,
            fontSize: 14,
          ),
        ),
      ),
    );
  }
}

class CurrentPlanCard extends StatelessWidget {
  /// 当前计划卡片
  ///
  /// 计划管理页上方展示当前使用的背诵计划
  const CurrentPlanCard(
      {super.key,
      required this.name,
      required this.total,
      required this.speed,
      required this.finishedNum,
      required this.bookImage,
      required this.onSpeedChange});

  /// 计划名称
  final String name;

  /// 需背诵数量
  final int total;

  /// 背诵速度
  final int speed;

  /// 背诵进度
  final int finishedNum;

  /// 书籍图片
  final String bookImage;

  /// 修改背诵速度方法
  final Future<bool> Function(int) onSpeedChange;

  @override
  Widget build(BuildContext context) {
    /// 剩余背诵天数
    int restDays = ((total - finishedNum) / speed).isNaN
        ? 0
        : ((total - finishedNum) / speed).ceil();

    return Stack(
      clipBehavior: Clip.none,
      children: [
        Positioned(
          child: Container(
            padding: const EdgeInsets.only(left: 100),
            decoration: const BoxDecoration(
                color: Color(0xFFF1F2F5),
                borderRadius: BorderRadius.all(Radius.circular(8))),
            child: Padding(
              padding: const EdgeInsets.fromLTRB(16, 10, 40, 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  // 计划名称
                  Text(
                    name,
                    style: const TextStyle(color: Colors.black54, fontSize: 12),
                  ),
                  // 背诵速度
                  Container(
                    margin: const EdgeInsets.fromLTRB(0, 4, 0, 6),
                    child: Row(
                      children: [
                        Text(
                          "每日背诵x个".trArgs([speed.toString()]),
                          style: const TextStyle(
                              color: Colors.black87,
                              fontSize: 16,
                              fontWeight: FontWeight.w600),
                        ),
                        Container(
                          height: 20,
                          width: 46,
                          margin: const EdgeInsets.only(left: 4),
                          child: TextButton(
                            style: TextButton.styleFrom(
                                padding: const EdgeInsets.only(bottom: 2),
                                minimumSize: const Size(50, 20),
                                fixedSize: const Size(50, 20),
                                backgroundColor: const Color(0XFF5F7292),
                                foregroundColor: Colors.white,
                                textStyle: const TextStyle(fontSize: 12)),
                            onPressed: () {
                              // 修改背诵速度选择器
                              Get.bottomSheet(
                                SpeedPicker(onChange: onSpeedChange),
                                backgroundColor: Colors.white,
                                shape: const RoundedRectangleBorder(
                                  borderRadius: BorderRadius.vertical(
                                      top: Radius.circular(10)),
                                  // side: BorderSide(
                                  //   width: 2,
                                  //   color: Colors.blue,
                                  //   style: BorderStyle.solid,
                                  // ),
                                ),
                              );

                              // FXIME: 选择后触发
                              // onChange(newSpeed);
                            },
                            child: Text("修改".tr),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // 背诵进度数据
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      TagWithData(
                        "共需背",
                        "x个".trArgs([total.toString()]),
                        titleStyle: const TextStyle(
                            color: Colors.black38, fontSize: 12),
                        dataStyle: const TextStyle(
                            color: Colors.black38, fontSize: 12),
                      ),
                      TagWithData("已背", "x个".trArgs([finishedNum.toString()]),
                          titleStyle: const TextStyle(
                              color: Colors.black38, fontSize: 12),
                          dataStyle: const TextStyle(
                              color: Colors.black38, fontSize: 12)),
                      TagWithData("剩余天数", "x天".trArgs([restDays.toString()]),
                          titleStyle: const TextStyle(
                              color: Colors.black38, fontSize: 12),
                          dataStyle: const TextStyle(
                              color: Colors.black38, fontSize: 12)),
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Positioned(bottom: 16, child: BookImage(url: bookImage)),
      ],
    );
  }
}

/// 修改计划背诵速度选择器
class SpeedPicker extends StatelessWidget {
  const SpeedPicker({super.key, required this.onChange});

  final Future<bool> Function(int) onChange;
  static const double _kItemExtent = 32.0;

  @override
  Widget build(BuildContext context) {
    FixedExtentScrollController controller = FixedExtentScrollController(
        initialItem: RecitePlanController.to.onGoingPlan.value.speed - 1);

    RxInt selected = 0.obs;

    return Container(
      height: 347,
      padding: const EdgeInsets.only(
        top: 12,
        bottom: 20,
      ),
      child: Column(
        children: [
          BottomSheetHeader(title: "背诵计划".tr),
          const Divider(),
          SizedBox(
            height: 200,
            child: CupertinoPicker(
              magnification: 1.22,
              squeeze: 1.2,
              useMagnifier: true,
              itemExtent: _kItemExtent,
              scrollController: controller,
              // This is called when selected item is changed.
              onSelectedItemChanged: (int selectedItem) {
                selected = selectedItem.obs;
              },
              children: RecitePlanController.to.generateSpeedPlanList(),
            ),
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
                minimumSize: const Size(256, 44),
                backgroundColor: const Color(0xFFC49C50)),
            onPressed: () async {
              // 保存选项
              Posthog().capture(
                eventName: '确认修改计划',
                properties: {
                  'id': RecitePlanController.to.onGoingPlan.value.id,
                  'previous': RecitePlanController.to.onGoingPlan.value.speed,
                  'current': selected.value + 1
                },
              );
              await onChange(selected.value + 1);
              Get.back();
            },
            child: Text(
              "确认保存".tr,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 14,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

/// 偈颂计划卡片
///
/// 计划管理页面展示每个偈颂计划及范围。
class RecitePlanCard extends StatelessWidget {
  const RecitePlanCard({
    super.key,
    required this.index,
    required this.name,
    required this.finished,
    required this.total,
    required this.speed,
    required this.progress,
    required this.bookImage,
  });

  /// 列表索引
  final int index;

  /// 计划名称
  final String name;

  /// 是否已完成
  final bool finished;

  /// 需背诵数量
  final int total;

  /// 背诵速度
  final int speed;

  /// 背诵进度
  final int progress;

  /// 书籍图片
  final String bookImage;

  @override
  Widget build(BuildContext context) {
    /// 剩余背诵天数
    int speed_ = speed;
    int restDays = ((total - progress) / speed_).isNaN
        ? 0
        : ((total - progress) / speed_).ceil();

    Widget action;

    if (finished) {
      action = Padding(
        padding: const EdgeInsets.fromLTRB(0, 0, 16, 0),
        child: Text(
          "have finished".tr,
          textAlign: TextAlign.right,
          style: const TextStyle(
              color: Color(0XFFC49C50),
              fontWeight: FontWeight.bold,
              fontSize: 12),
        ),
      );
    } else {
      action = SizedBox(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            SizedBox(
              height: 28,
              child: TextButton(
                style: TextButton.styleFrom(
                    padding: const EdgeInsets.only(bottom: 2),
                    textStyle: const TextStyle(fontSize: 12),
                    fixedSize: const Size(60, 28),
                    minimumSize: const Size(60, 24),
                    backgroundColor: const Color(0xFFC49C50)),
                onPressed: () async {
                  await RecitePlanController.to.changeRecitePlan(index);
                },
                child: Text(
                  "Study".tr,
                  style: const TextStyle(
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            const SizedBox(
              height: 8,
            ),
            SizedBox(
              height: 28,
              child: IconButton(
                icon: const Icon(
                  Icons.delete,
                  color: Colors.black38,
                  size: 16,
                ),
                style: IconButton.styleFrom(
                    padding: const EdgeInsets.only(bottom: 2),
                    fixedSize: const Size(60, 28),
                    minimumSize: const Size(60, 24),
                    foregroundColor: Colors.black38,
                    backgroundColor: const Color(0xFFF1F2F5)),
                onPressed: () async {
                  await RecitePlanController.to.deleteRecitePlan(index);
                },
              ),
            ),
          ],
        ),
      );
    }
    double nameWidth = MediaQuery.of(context).size.width * 0.45;
    return Container(
      decoration: finished
          ? const BoxDecoration(
              image: DecorationImage(
              image: AssetImage("images/background/finished.png"),
              fit: BoxFit.contain,
              alignment: Alignment.bottomRight,
            ))
          : const BoxDecoration(),
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height / 6,
      child: Flex(
        direction: Axis.horizontal,
        children: <Widget>[
          Flexible(
            flex: 1,
            child: BookImage(url: bookImage),
          ),
          Flexible(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: finished
                                ? const EdgeInsets.only(top: 8.0)
                                : const EdgeInsets.all(0),
                            child: Text(name,
                                textAlign: TextAlign.left,
                                maxLines: 3,
                                softWrap: true,
                                overflow: TextOverflow.ellipsis,
                                style: const TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 16)),
                          ),
                          Text(
                            "共x偈颂".trArgs([total.toString()]),
                            style: const TextStyle(
                                color: Colors.black38, fontSize: 14),
                          ),
                        ],
                      ),
                    ),
                    Column(
                      children: [
                        Container(
                          // 根据状态显示“学这本、删除” 或 “已完成”
                          child: action,
                        ),
                      ],
                    ),
                  ],
                ),
                progress == total
                    ? const SizedBox()
                    : Stack(
                        children: [
                          Container(
                            height: 8,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: const Color(0x1AC49C50),
                            ),
                          ),
                          Container(
                            height: 8,
                            width: (progress / total).isNaN
                                ? 0
                                : (progress / total) *
                                    Get.mediaQuery.size.width,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                gradient: const LinearGradient(colors: [
                                  Color(0xFFC49C50),
                                  Color(0xFFE4CB88),
                                ])),
                          ),
                        ],
                      ),
                finished
                    ? const SizedBox()
                    : Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                              "每天@speed偈颂，剩余@rest天".trParams({
                                "speed": speed.toString(),
                                "rest": restDays.toString(),
                              }),
                              style: const TextStyle(
                                  color: Colors.black38, fontSize: 12)),
                          Text("$progress/$total"),
                        ],
                      )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class CreatePlanPicker extends StatelessWidget {
  const CreatePlanPicker({super.key, required this.items});

  final Map<int, ChapterModel> items;

  @override
  Widget build(BuildContext context) {
    List<int> selectedGathas = <int>[];
    RxInt total = 0.obs;

    return Padding(
      padding: const EdgeInsets.fromLTRB(16, 100, 16, 0),
      child: Column(
        children: [
          BottomSheetHeader(title: "背诵范围".tr),
          SelectorForm(
            items: items,
            onChange: ((e) {
              selectedGathas = e;
              total.value = 0;
              for (int i in e) {
                total += items[i]!.length;
              }
            }),
          ),
          // 计算偈颂数量
          Obx(() => Text(
                "共x偈颂".trArgs([total.toString()]),
                style: const TextStyle(color: Colors.black54, fontSize: 14),
              )),
          Container(
            height: 50,
            margin: const EdgeInsets.only(bottom: 20),
            child: TextButton(
              style: TextButton.styleFrom(
                  minimumSize: const Size(280, 44),
                  backgroundColor: const Color(0xFFC49C50)),
              onPressed: () async {
                // 创建计划
                if (selectedGathas.isEmpty) {
                  Get.showSnackbar(GetSnackBar(
                    message: "请至少选择一个章节".tr,
                    duration: const Duration(seconds: 3),
                  ));
                  return;
                }
                Posthog().capture(
                  eventName: '新建计划',
                  properties: {
                    'selectedGathas': selectedGathas,
                  },
                );
                RecitePlanController.to.createRecitePlan(1, selectedGathas);
                Get.back();
              },
              child: Text(
                "保存范围".tr,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 14,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}

class BottomSheetHeader extends StatelessWidget {
  const BottomSheetHeader({super.key, required this.title});

  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      width: double.infinity,
      margin: const EdgeInsets.only(bottom: 26),
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: [
          Text(
            title,
            style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),
          ),
          Positioned(
              right: 18,
              child: IconButton(
                  iconSize: 24,
                  onPressed: () {
                    Get.back();
                  },
                  icon: const Icon(Icons.close)))
        ],
      ),
    );
  }
}
