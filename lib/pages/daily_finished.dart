import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:posthog_flutter/posthog_flutter.dart';
import 'package:zzjs_client/components/calendar.dart';
import 'package:zzjs_client/controllers/global.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/routes/pages.dart';

/// 完成每日任务提醒页
class DailyFinishedPage extends StatelessWidget {
  DailyFinishedPage({super.key}) {
    // FIXME: 更换初始化位置
    GlobalService.to.cacuAggMonthData();
  }

  @override
  Widget build(BuildContext context) {
    Posthog().screen(
      screenName: Routes.DAILY_FINISHED,
    );

    /// 当前计划全部完成
    bool planFinished = Get.arguments ?? false;
    String msg = "今日计划已完成";
    if (planFinished) {
      msg = "祝贺你，当前计划已全部完成，可以进行新计划了!";
    }
    return Scaffold(
      appBar: AppBar(
        title: Text(
          ReciteProgressController.to.bookName.tr,
          style: const TextStyle(fontWeight: FontWeight.w600),
        ),
        centerTitle: true,
        backgroundColor: const Color(0xfff3f4fd),
      ),
      body: Container(
        color: const Color(0xfff3f4fd),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            // const SizedBox(
            //   height: 20,
            // ),
            Image.asset(
              "images/placeholder/calendar.png",
              fit: BoxFit.cover,
              width: 120,
            ),
            const SizedBox(
              height: 20,
            ),
            Text(msg.tr),
            const SizedBox(
              height: 20,
            ),
            Obx(() => Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        children: <InlineSpan>[
                          // 展示当前计划总共已背数量
                          const TextSpan(text: "已背"),
                          const WidgetSpan(
                              child: SizedBox(
                            width: 5,
                          )),
                          TextSpan(
                            text: GlobalService.to.monthFinished.toString(),
                            style: const TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic),
                          ),
                          const WidgetSpan(
                              child: SizedBox(
                            width: 5,
                          )),
                          const TextSpan(text: "偈颂"),
                        ],
                      ),
                    ),
                    RichText(
                      text: TextSpan(
                        style: const TextStyle(
                          color: Colors.black,
                          fontSize: 16,
                        ),
                        children: <InlineSpan>[
                          const TextSpan(text: "坚持"),
                          const WidgetSpan(
                              child: SizedBox(
                            width: 5,
                          )),
                          TextSpan(
                            text: GlobalService.to.workingDays.toString(),
                            style: const TextStyle(
                                fontSize: 24,
                                fontWeight: FontWeight.bold,
                                fontStyle: FontStyle.italic),
                          ),
                          const WidgetSpan(
                              child: SizedBox(
                            width: 5,
                          )),
                          const TextSpan(text: "天"),
                        ],
                      ),
                    ),
                  ],
                )),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 16),
              margin: const EdgeInsets.only(top: 20),
              child: Text(
                DateTime.now()
                    .toIso8601String()
                    .substring(0, 10)
                    .replaceAll("-", "."),
                style: const TextStyle(color: Colors.grey),
              ),
            ),
            const CalendarWidget(),
            planFinished
                ? Container()
                : Container(
                    width: double.infinity,
                    padding: const EdgeInsets.symmetric(horizontal: 20.0),
                    child: ElevatedButton(
                      style: ButtonStyle(
                          foregroundColor:
                              MaterialStateProperty.all(Colors.white),
                          textStyle: MaterialStateProperty.all(
                              const TextStyle(color: Colors.white)),
                          backgroundColor: MaterialStateProperty.all(
                              const Color(0xFF5E7292))),
                      onPressed: () {
                        Get.back();
                      },
                      child: Text("加量背诵".tr),
                    ),
                  ),
          ],
        ),
      ),
    );
  }
}
