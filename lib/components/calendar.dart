import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:zzjs_client/components/calendar/flutter_calendar_carousel.dart'
    show CalendarCarousel;
import 'package:zzjs_client/components/calendar/classes/event.dart';
import 'package:zzjs_client/components/calendar/classes/event_list.dart';
import 'package:intl/intl.dart' show DateFormat;
import 'package:zzjs_client/controllers/calendar.dart';

/// 日历组件
///
/// 在打卡日历页和完成今日任务后的提示页，
/// 展示当前月份的打卡状态
class CalendarWidget extends StatelessWidget {
  const CalendarWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return GetBuilder<CalendarController>(
        init: CalendarController(),
        builder: (controller) {
          DateTime currentDate = DateTime.now();
          Widget eventIcon = Container(
            decoration: BoxDecoration(
              color: const Color(0xffC49C50),
              borderRadius: BorderRadius.circular(1000),
            ),
            child: const Icon(
              Icons.done_rounded,
              color: Colors.white,
              size: 14,
            ),
          );
          EventList<Event> markedDateMap = EventList<Event>(events: {});
          for (var e in controller.taskTracker) {
            markedDateMap.add(
                DateTime.parse(e['updateAt']),
                Event(
                  date: DateTime.parse(e['updateAt']),
                  title: '',
                  icon: eventIcon,
                ));
          }
          return SingleChildScrollView(
            child: Container(
                margin:
                    const EdgeInsets.symmetric(horizontal: 16.0, vertical: 20),
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.all(Radius.circular(12)),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black.withOpacity(0.1),
                          offset: const Offset(0, 6),
                          blurRadius: 3.0,
                          spreadRadius: 1),
                    ]),
                child: Listener(
                  onPointerMove:(event)=>controller.isMove.value = true,
                  child: CalendarCarousel<Event>(
                    onDayPressed: (date, events) {
                      controller.isMove.value = false;
                      controller.getGatha(date);
                    },
                    markedDateMoreCustomDecoration: const BoxDecoration(
                      color: Color(0x33C49C50),
                    ),
                    markedDateShowIcon: false,
                    markedDateIconBuilder: (event) {
                      return Container(
                          margin: EdgeInsets.zero,
                          padding: EdgeInsets.zero,
                          decoration: BoxDecoration(
                              // color: Color(0x33C49C50),
                              borderRadius: BorderRadius.circular(100)),
                          height: 50,
                          width: 37,
                          child: Stack(
                            clipBehavior: Clip.none,
                            children: [
                              Positioned(
                                bottom: -5,
                                right: -3,
                                child: Container(
                                  padding: const EdgeInsets.all(1),
                                  decoration: BoxDecoration(
                                    color: const Color(0xffC49C50),
                                    borderRadius: BorderRadius.circular(10),
                                    border: Border.all(
                                        color: Colors.white,
                                        width: event.date.year ==
                                                    currentDate.year &&
                                                event.date.month ==
                                                    currentDate.month &&
                                                event.date.day == currentDate.day
                                            ? 2
                                            : 0),
                                  ),
                                  child: const Icon(
                                    Icons.done_rounded,
                                    color: Colors.white,
                                    size: 10,
                                  ),
                                ),
                              )
                            ],
                          ));
                    },
                    markedDateCustomShapeBorder: const CircleBorder(
                        side: BorderSide(width: 20, color: Color(0x33C49C50))),
                    weekdayTextStyle: const TextStyle(color: Colors.black),
                    markedDateIconMaxShown: 1,
                    markedDateIconOffset: 10,
                    dayPadding: 2,
                    selectedDayTextStyle: const TextStyle(
                      color: Colors.white,
                    ),
                    todayButtonColor: const Color(0xFFC49C50),
                    todayBorderColor: Colors.transparent,
                    selectedDayBorderColor: const Color(0xFFC49C50),
                    selectedDayButtonColor: const Color(0xFFC49C50),
                    daysHaveCircularBorder: true,
                    showOnlyCurrentMonthDate: false,
                    weekendTextStyle: const TextStyle(
                      color: Colors.black,
                    ),
                    weekFormat: false,
                    markedDatesMap: markedDateMap,
                    height: MediaQuery.of(context).size.height/3,
                    targetDateTime: controller.targetDateTime.value,
                    customGridViewPhysics: const NeverScrollableScrollPhysics(),
                    markedDateCustomTextStyle: const TextStyle(
                      fontWeight: FontWeight.w800,
                      color: Colors.black,
                    ),
                    showHeader: false,
                    todayTextStyle:  TextStyle(
                      color: controller.targetDateTime.value==DateTime.now()?Colors.white:Colors.black,
                    ),
                    minSelectedDate:
                        currentDate.subtract(const Duration(days: 360)),
                    maxSelectedDate: currentDate.add(const Duration(days: 360)),

                    prevDaysTextStyle: const TextStyle(
                      fontSize: 16,
                      color: Colors.black12,
                    ),
                    nextDaysTextStyle: const TextStyle(color: Colors.black12),
                    inactiveDaysTextStyle:
                        const TextStyle(color: Colors.red, fontSize: 16),
                    onCalendarChanged: (DateTime date) {
                      if(controller.isMove.value){
                        controller.isMove.value = false;
                        controller.getGatha(date);
                      }
                    },
                    onDayLongPressed: (DateTime date) {},
                  ),
                )),
          );
        });
  }
}
