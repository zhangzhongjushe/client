import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ZZJSUnderlineTabIndicator extends Decoration {
  const ZZJSUnderlineTabIndicator({
    this.borderSide = const BorderSide(width: 2.0, color: Colors.white),
    this.insets = EdgeInsets.zero,
    this.indicatorBottom = 0.0,
    this.indicatorWidth = 28,
    this.isRound = false,
  });
  final BorderSide borderSide;
  final EdgeInsetsGeometry insets;
  final double indicatorBottom; // 自定义指示条距离底部距离
  final double indicatorWidth; // 自定义指示条宽度
  final bool? isRound; // 自定义指示条是否是圆角

  @override
  Decoration? lerpFrom(Decoration? a, double t) {
    if (a is ZZJSUnderlineTabIndicator) {
      return ZZJSUnderlineTabIndicator(
        borderSide: BorderSide.lerp(a.borderSide, borderSide, t),
        insets: EdgeInsetsGeometry.lerp(a.insets, insets, t)!,
      );
    }
    return super.lerpFrom(a, t);
  }

  @override
  Decoration? lerpTo(Decoration? b, double t) {
    if (b is ZZJSUnderlineTabIndicator) {
      return ZZJSUnderlineTabIndicator(
        borderSide: BorderSide.lerp(borderSide, b.borderSide, t),
        insets: EdgeInsetsGeometry.lerp(insets, b.insets, t)!,
      );
    }
    return super.lerpTo(b, t);
  }

  @override
  BoxPainter createBoxPainter([VoidCallback? onChanged]) {
    return _UnderlinePainter(this, onChanged, isRound ?? false);
  }

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    final Rect indicator = insets.resolve(textDirection).deflateRect(rect);

    //    return Rect.fromLTWH(
    //      indicator.left,
    //      indicator.bottom - borderSide.width,
    //      indicator.width,
    //      borderSide.width,
    //    );

    //取中间坐标
    double cw = (indicator.left + indicator.right) / 2;
    // ***************************这里可以自定义指示条的宽度和底部间距***************************
    Rect indictorRect = Rect.fromLTWH(
        cw - indicatorWidth / 2,
        indicator.bottom - borderSide.width - indicatorBottom,
        indicatorWidth,
        borderSide.width);
    return indictorRect;
  }

  @override
  Path getClipPath(Rect rect, TextDirection textDirection) {
    return Path()..addRect(_indicatorRectFor(rect, textDirection));
  }
}

class _UnderlinePainter extends BoxPainter {
  _UnderlinePainter(this.decoration, VoidCallback? onChanged, this.isRound)
      : super(onChanged);

  final ZZJSUnderlineTabIndicator decoration;
  bool isRound = false;

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size!;
    final TextDirection textDirection = configuration.textDirection!;
    final Rect indicator = decoration
        ._indicatorRectFor(rect, textDirection)
        .deflate(decoration.borderSide.width / 2.0);
    //***************************这里可以自定义指示条是否是圆角***************************
    final Paint paint = decoration.borderSide.toPaint()
      ..strokeCap = isRound ? StrokeCap.round : StrokeCap.square;
    canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
  }
}

const double _kTextAndIconTabHeight = 72.0;

enum TabBarIndicatorSize {
  tab,
  label,
}

class TCTab extends StatelessWidget implements PreferredSizeWidget {
  const TCTab({
    Key? key,
    this.text,
    this.icon,
    this.iconMargin = const EdgeInsets.only(bottom: 10.0),
    this.height,
    this.child,
    this.tabBarHeight = 44,
  })  : assert(text != null || child != null || icon != null),
        assert(text == null || child == null),
        super(key: key);

  final String? text;

  final Widget? child;

  final Widget? icon;

  final EdgeInsetsGeometry iconMargin;

  final double? height;

  final double tabBarHeight; // 自定义tab的高度

  Widget _buildLabelText() {
    return child ?? Text(text!, softWrap: false, overflow: TextOverflow.fade);
  }

  @override
  Widget build(BuildContext context) {
    assert(debugCheckHasMaterial(context));

    final double calculatedHeight;
    final Widget label;
    if (icon == null) {
      calculatedHeight = tabBarHeight;
      label = _buildLabelText();
    } else if (text == null && child == null) {
      calculatedHeight = tabBarHeight;
      label = icon!;
    } else {
      calculatedHeight = _kTextAndIconTabHeight;
      label = Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            margin: iconMargin,
            child: icon,
          ),
          _buildLabelText(),
        ],
      );
    }

// ***********************tab的高度在这里定义****************************
    return SizedBox(
      height: height ?? calculatedHeight,
      child: Center(
        widthFactor: 1.0,
        child: label,
      ),
    );
  }

  @override
  void debugFillProperties(DiagnosticPropertiesBuilder properties) {
    super.debugFillProperties(properties);
    properties.add(StringProperty('text', text, defaultValue: null));
    properties
        .add(DiagnosticsProperty<Widget>('icon', icon, defaultValue: null));
  }

  @override
  Size get preferredSize {
    if (height != null) {
      return Size.fromHeight(height!);
    } else if ((text != null || child != null) && icon != null) {
      return const Size.fromHeight(_kTextAndIconTabHeight);
    } else {
      return Size.fromHeight(tabBarHeight);
    }
  }
}
