import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzjs_client/controllers/recite_progress.dart';
import 'package:zzjs_client/schemas/book.dart';

typedef GathaSelectListener = void Function(GathaModel gatha);

/// 偈颂展示卡片
///
/// 每个卡片默认展示两行偈颂，展示在偈颂列表页
class GathaCard extends StatelessWidget {
  /// 当前偈颂卡片展示内容
  final GathaModel gathaItem;

  /// 点击当前偈颂卡片回调函数
  final GathaSelectListener? onSelected;

  /// 当前偈颂所在列表位置索引
  final int index;

  /// 是否是上次背诵内容
  final bool current;

  const GathaCard(
      {super.key,
      required this.gathaItem,
      this.onSelected,
      required this.index,
      required this.current});

  static TextStyle txtLeftSty = const TextStyle(
      color: Color(0xE6d2d2d2),
      fontSize: 20,
      fontWeight: FontWeight.w500,
      fontStyle: FontStyle.italic);

  static TextStyle txtMidSty = const TextStyle(
    color: Color(0xE6000000),
    fontSize: 22,
    fontWeight: FontWeight.w500,
  );

  @override
  Widget build(BuildContext context) {
    String reciteStatus;

    TextStyle statusTextStyle = const TextStyle(
      color: Color(0XFF97A3B7),
      fontSize: 12,
      fontWeight: FontWeight.w500,
    );

    try {
      reciteStatus = reciteStatusTitleMap[gathaItem.progress.value!.status]!;
      switch (gathaItem.progress.value!.status) {
        case ReciteStatus.forget:
          statusTextStyle = const TextStyle(
            color: Color(0xFF0052D9),
            fontSize: 12,
            fontWeight: FontWeight.w500,
          );
          break;
        case ReciteStatus.memorized:
          statusTextStyle = const TextStyle(
            color: Color(0XFFC49C50),
            fontSize: 12,
            fontWeight: FontWeight.w500,
          );
          break;
        case ReciteStatus.none:
          break;
        default:
          throw TypeError();
      }
    } catch (e) {
      reciteStatus = reciteStatusTitleMap[ReciteStatus.none]!;
    }

    return GestureDetector(
      onTap: () {
        if (onSelected != null) {
          ReciteProgressController.to.currentController.currentIndex.value =
              index;
          onSelected!(gathaItem);
        }
      },
      child: Container(
        margin: const EdgeInsets.only(left: 15, right: 15, top: 10),
        //设置 child 居中
        alignment: const Alignment(0, 0),
        //边框设置
        decoration: BoxDecoration(
          //背景
          color: Colors.white,
          //设置四周圆角 角度
          borderRadius: const BorderRadius.all(Radius.circular(8.0)),
          //阴影
          boxShadow: const [
            BoxShadow(
              color: Colors.black12, // 阴影的颜色
              offset: Offset(0, 0), // 阴影与容器的距离
              blurRadius: 2.0, // 高斯的标准偏差与盒子的形状卷积。
              spreadRadius: 0.0, // 在应用模糊之前，框应该膨胀的量。
            ),
          ],
          //设置四周边框
          border: current
              ? Border.all(width: 2, color: const Color(0XFFBCC4D0))
              : Border.all(width: 1, color: Colors.white),
        ),
        child: Stack(
          clipBehavior: Clip.none,
          children: [
            current
                ? Positioned(
                    top: -18,
                    left: -1,
                    child: Container(
                      height: 30,
                      width: 130,
                      decoration: const BoxDecoration(
                          color: Color(0XFF5F7292),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(16),
                            topRight: Radius.circular(16),
                            bottomRight: Radius.circular(16),
                            bottomLeft: Radius.zero,
                          )),
                      child: Center(
                        child: Text(
                          "上次背诵位置".tr,
                          style: const TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ))
                : const SizedBox(),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    gathaItem.name,
                    style: txtLeftSty,
                    overflow: TextOverflow.ellipsis,
                  ),
                  Expanded(
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            gathaItem.content.split("\\r\\n")[0].trim(),
                            style: txtMidSty,
                            overflow: TextOverflow.ellipsis,
                          ),
                          Text(
                            gathaItem.content.split("\\r\\n")[1].trim(),
                            style: txtMidSty,
                            overflow: TextOverflow.ellipsis,
                          )
                        ]),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Text(reciteStatus, style: statusTextStyle),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
