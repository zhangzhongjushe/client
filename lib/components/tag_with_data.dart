import 'package:flutter/material.dart';

class TagWithData extends StatelessWidget {
  /// 上下结构的带title数据展示
  const TagWithData(this.title, this.data,
      {super.key, this.titleStyle, this.dataStyle});

  final String title;
  final String data;
  final TextStyle? titleStyle;
  final TextStyle? dataStyle;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Text(title, style: titleStyle),
        Text(data, style: dataStyle),
      ],
    );
  }
}
