import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:zzjs_client/schemas/book.dart';

class SelectorController extends GetxController {
  final Map<int, ChapterModel> items;
  final void Function(List<int>) onChange;

  SelectorController(this.items, this.onChange);

  static SelectorController get to => Get.find();

  var selected = RxMap<int, ChapterModel>();

  bool isSelected(int id) => selected.containsKey(id);

  List<int> getSelectedKeys() {
    return [
      ...Iterable.generate(selected.length, (i) => selected.keys.elementAt(i))
    ];
  }

  void remove(int id) {
    selected.remove(id);
    onChange(selected.keys.toList());
    update();
  }

  void select(int id) {
    selected[id] = items[id]!;
    onChange(selected.keys.toList());
    update();
  }
}

class SelectorForm extends StatelessWidget {
  const SelectorForm({super.key, required this.items, required this.onChange});

  final Map<int, ChapterModel> items;
  final void Function(List<int>) onChange;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<SelectorController>(
      init: SelectorController(items, onChange),
      initState: (_) {},
      builder: (controller) {
        return Expanded(
          child: controller.items.isEmpty
              ? const Center(child: CircularProgressIndicator())
              : ListView.builder(
                  // separatorBuilder: (context, index) =>
                  //     const SizedBox(height: 1),
                  itemCount: controller.items.length,
                  itemBuilder: (context, index) {
                    int key = items.keys.elementAt(index);
                    var checked = controller.isSelected(key);

                    return Container(
                      height: 60,
                      padding: const EdgeInsets.symmetric(
                          vertical: 5, horizontal: 30),
                      child: TextButton(
                        onPressed: () {
                          checked
                              ? controller.remove(key)
                              : controller.select(key);
                        },
                        style: TextButton.styleFrom(
                            backgroundColor: checked
                                ? Colors.blueGrey[700]
                                : const Color(0XFFF1F2F5)),
                        child: checked
                            ? SizedBox(
                                width: double.infinity,
                                child: Stack(
                                  children: [
                                    const Positioned(
                                      left: 6,
                                      top: 5,
                                      child: Icon(
                                        Icons.check_circle,
                                        color: Color(0xFFEFF1F4),
                                      ),
                                    ),
                                    Center(
                                      child: Text(
                                        controller.items[key]!.name,
                                        style: const TextStyle(
                                            color: Colors.white),
                                      ),
                                    )
                                  ],
                                ),
                              )
                            : Text(
                                controller.items[key]!.name,
                                style: const TextStyle(color: Colors.black),
                              ),
                      ),
                    );
                  },
                ),
        );
      },
    );
  }
}

class SelectorItem extends StatelessWidget {
  const SelectorItem(this.name, {super.key, required this.checked});

  final bool checked;
  final String name;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      width: double.infinity,
      decoration: BoxDecoration(
        color: checked ? const Color(0xFF5F7292) : Colors.white,
        borderRadius: BorderRadius.circular(20),
      ),
      child: Text(
        name,
        style: TextStyle(
            color: checked ? const Color(0xFFF1F2F5) : Colors.black87),
      ),
    );
  }
}
